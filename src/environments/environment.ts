// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  security: {
    key: '2Jy4M`$zb;y_YULQ',
  },
  pageURL: 'http://localhost:4200',
  firebase: {
    /*     apiKey: "AIzaSyAOQgfwmz6rT_1szWVPloamJ0zKGveDDlg",
        authDomain: "agricapitaldev.firebaseapp.com",
        databaseURL: "https://agricapitaldev.firebaseio.com",
        projectId: "agricapitaldev",
        storageBucket: "agricapitaldev.appspot.com",
        messagingSenderId: "218386602288",
        appId: "1:218386602288:web:b19cce4ab252b761ed50f5",
        measurementId: "G-JZGK903Q8Y" */
    apiKey: "AIzaSyAzDDjjS1Ez-1B4f9GRyV8aFutIuvwzwaQ",
    authDomain: "agricapital-3a.firebaseapp.com",
    databaseURL: "https://agricapital-3a.firebaseio.com",
    projectId: "agricapital-3a",
    storageBucket: "agricapital-3a.appspot.com",
    messagingSenderId: "397574632440",
    appId: "1:397574632440:web:0f98e86ae263f86f5f5c0d",
    measurementId: "G-P07J0GNLT7"
  }
};
