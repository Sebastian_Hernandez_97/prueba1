// Modulos requeridos
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileRoutingModule } from './user-profile.routes';
import { SharedModule } from '../../shared.module';

import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
// Componentes del submodulo Auth
registerLocaleData(es);
@NgModule({
  declarations: [],
  imports: [CommonModule, UserProfileRoutingModule, SharedModule],
  exports: [],
  // Solucionar proveer form service
  providers: [{ provide: LOCALE_ID, useValue: 'es-ES' }],
  bootstrap: []
})
export class AuthModule { }
