import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoanProfileAdminComponent } from 'src/app/components/generic/loan-profile-admin/loan-profile-admin.component';
import { CreditHistoryComponent } from 'src/app/components/generic/credit-history/credit-history.component';

const routes: Routes = [
  { path: '', component: LoanProfileAdminComponent },
  { path: ':id', component: LoanProfileAdminComponent },
  { path: ':id/creditHistory/:creditId', component: CreditHistoryComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule { }
