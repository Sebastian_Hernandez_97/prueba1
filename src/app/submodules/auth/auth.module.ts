// Modulos requeridos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth.routes';
import { SharedModule } from '../../shared.module';
// Componentes del submodulo Auth

import { AuthComponent } from './auth.component';
import { LoginComponent } from './components/login/login.component';

// Servicios
import { TemplateService } from './services/template.service';
import { FormService } from 'ngx-3a';
import { RegisterComponent } from './components/register/register.component';
import { TextComponent } from 'src/app/components/fields/text/text.field.component';
import { PasswordComponent } from 'src/app/components/fields/password/password.field.component';
import { FormlyModule } from '@ngx-formly/core';

// Pipes


@NgModule({
  declarations: [AuthComponent, LoginComponent, RegisterComponent],
  imports: [CommonModule, AuthRoutingModule, SharedModule, FormlyModule.forRoot({
    types: [
      {
        name: 'input',
        component: TextComponent
      },
      {
        name: 'password',
        component: PasswordComponent
      }
    ]
  })],
  exports: [SharedModule],
  // Solucionar proveer form service
  providers: [TemplateService, FormService],
  bootstrap: [AuthComponent]
})
export class AuthModule { }
