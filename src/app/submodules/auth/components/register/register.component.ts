import { Component, OnInit, forwardRef, Inject, ViewChild } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { Router, ActivatedRoute } from "@angular/router";
import { UserRole, User, UserStatus } from "src/app/models/user";
import { FormlyFieldConfig } from "@ngx-formly/core";
import { BasicFormComponent, AuthenticationService } from "ngx-3a";
import { MainService } from 'src/app/services/main.service';
import { AlertService } from 'src/app/services/alerts-message.service';

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["../login/login.component.scss"],
})
export class RegisterComponent implements OnInit {
  @ViewChild("form", {
    static: false,
  })
  private form: BasicFormComponent;
  user: any = {};
  fields: FormlyFieldConfig[];
  image = "/assets/login-analista.png";
  loading = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    @Inject(forwardRef(() => UserService)) private userSvc: UserService,
    @Inject(forwardRef(() => AuthenticationService))
    private authSvc: AuthenticationService,
    private mainSvc: MainService,
    private alertSvc: AlertService,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      const invitationID = params.get("invitation");
      if (invitationID) {
        this.userSvc.getInvitation(invitationID).subscribe((invitation) => {
          if (invitation) {
            if (invitation.role != UserRole.FARMER) {
              this.user.name = invitation.name;
              this.user.email = invitation.email;
              this.user.role = invitation.role;
              this.fields = this.userSvc.addUserRegisterFields();
            }
          }
        });
      }
    });
  }

  /**
   * Método que despues de validar los campos se crea el usuario
   */
  onSubmit() {
    this.loading = true;
    this.authSvc.checkUserEmail(this.user.email, (exists, user) => {
      if (!exists) {
        this.authSvc.signUp(this.user.email, this.user.name, this.user.password, (userId, error) => {
          if (error) {
          } else {
            delete this.user["password"];
            this.user.id = userId;
            this.user.status = UserStatus.ACTIVE;
            this.userSvc.saveUser(this.user).subscribe();
            switch (this.user.role) {
              case UserRole.ANALISTA:
                this.router.navigateByUrl("/admin/applications-loans");
                break;
              case UserRole.COMERCIAL:
                this.router.navigateByUrl("/admin/applications-loans");
                break;

              case UserRole.DESEMBOLSO:
                this.router.navigateByUrl("/admin/payout");
                break;

              case UserRole.ADMIN:
                this.router.navigateByUrl("/admin/loans");
                break;
              
              case UserRole.REFERENCIACION:
                this.router.navigateByUrl("/admin/applications-loans");
                break;

              case UserRole.COMITE:
                this.router.navigateByUrl("/admin/applications-loans");
                break;

              case UserRole.TIENDA:
                let messageAlert = {
                  icon: "/assets/Alerta- Conciliacion manual.svg",
                  text: `¡Usuario registrado con éxito!, puedes iniciar sesión en nuestra App con el correo ${this.user.c}`,
                  button1: "Aceptar"
                };
                this.mainSvc.toogleModal.next(
                  this.alertSvc.showAlert(messageAlert)
                );
                break;

              default:
                this.router.navigateByUrl("/404");
                break;
            }
            this.loading = false;
          }
        });
      } else {
        this.loading = false;
        alert("Usuario ya registrado");
      }
    });
  }

  /**
   * validación del formulario
   */
  onClickSubmit() {
    this.form.send(true);
  }
}
