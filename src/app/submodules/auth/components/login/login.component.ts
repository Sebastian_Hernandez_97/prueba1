import { Component, OnInit, Inject, forwardRef, ViewChild } from "@angular/core";
import { TemplateService } from "../../services/template.service";
import { Router } from "@angular/router";
import { FormlyFieldConfig } from "@ngx-formly/core";
import { UserService } from "src/app/services/user.service";
import { BasicFormComponent, AuthenticationService } from "ngx-3a";
import { CustomerIdFormatterService } from "src/app/services/customer-id-formatter.service";
import { take } from "rxjs/operators";
import { UserRole } from "src/app/models/user";

@Component({
  selector: "app-auth-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  @ViewChild("form", {
    static: false,
  })
  form: BasicFormComponent;

  subtitle: string;
  fields: FormlyFieldConfig[];
  userType: string;
  attribute: any = {};
  loading = false;
  image = "/assets/";
  constructor(
    private template: TemplateService,
    private router: Router,
    @Inject(forwardRef(() => UserService)) private userSvc: UserService,
    @Inject(forwardRef(() => AuthenticationService)) private readonly authSvc: AuthenticationService
  ) {}

  ngOnInit() {
    if (this.router.url.includes("user")) {
      this.userType = "user";
      this.subtitle = "Inicia sesión con tu documento de identidad";
      this.image += "agricultor-login.png";
      this.fields = this.template.loginInput("user");
    } else {
      this.userType = "employee";
      this.subtitle = "Inicia sesión con tu E-mail y contraseña";
      this.image += "login-analista.png";
      this.fields = this.template.loginInput("employee");
    }
  }
  onKeyup() {
    if (this.userType === "user") {
      this.fields[0].formControl.setValue(CustomerIdFormatterService.parseid(this.attribute.id));
    }
  }
  /*
   * Está funcion debe abrir el modal de error
   * @param message texto que será mostrado en el momento en que ocurra el error
   */
  openErrorModal(message: string) {
    alert(message);
  }

  onSubmit(model: any) {
    this.loading = true;
    if (this.userType.includes("user")) {
      this.userSvc.signInUser(model.id).subscribe((res) => {
        if (res.length) {
          this.router.navigate(["user", model.id]);
        } else {
          this.openErrorModal("El usuario no existe, intentelo de nuevo");
        }
        this.loading = false;
      });
    } else {
      this.authSvc.signIn(model.email, model.password, (userId, error) => {
        if (!error) {
          this.userSvc
            .getUser(userId)
            .pipe(take(1))
            .subscribe((user: any) => {
              if (user) {
                switch (user.role) {
                  case UserRole.ANALISTA:
                    this.router.navigateByUrl("/admin/applications-loans");
                    break;
                  case UserRole.COMERCIAL:
                    this.router.navigateByUrl("/admin/applications-loans");
                    break;

                  case UserRole.DESEMBOLSO:
                    this.router.navigateByUrl("/admin/payout");
                    break;

                  case UserRole.ADMIN:
                    this.router.navigateByUrl("/admin/loans");
                    break;

                  case UserRole.REFERENCIACION:
                    this.router.navigateByUrl("/admin/applications-loans");
                    break;

                  case UserRole.COMITE:
                    this.router.navigateByUrl("/admin/applications-loans");
                    break;

                  default:
                    this.router.navigateByUrl("/404");
                    break;
                }
                this.loading = false;
              }
            });
        } else {
          this.loading = false;
          this.openErrorModal("El correo o la contraseña es incorrecta, intentelo de nuevo");
        }
      });
    }
    this.attribute = {};
  }

  onClickSubmit() {
    //this.loading = true;
    this.form.send(true);
  }
}
