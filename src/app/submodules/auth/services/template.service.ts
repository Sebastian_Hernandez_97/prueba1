import { Injectable, Inject, forwardRef } from "@angular/core";
import { FormService } from "ngx-3a";
import { FormlyFieldConfig, FormlyField } from "@ngx-formly/core";

@Injectable()
export class TemplateService {
  constructor(
    @Inject(forwardRef(() => FormService)) private formsSvc: FormService
  ) {}
  /**
   * input es el método que define los inputs del Auth
   * @param type Este es el tipo de usuario que va a iniciar sesión
   */
  public loginInput(type: string): FormlyFieldConfig[] {
    let inputs: FormlyFieldConfig[];
    let passwordInput: FormlyFieldConfig;
    if (type == "user") {
      inputs = [
        this.formsSvc.textInput("id", "Tu documento de identidad", "", true)
      ];
    } else {
      passwordInput = this.formsSvc.passwordInput("Contraseña", "", true);
      passwordInput.templateOptions.pattern = /[a-zA-Z0-9]{6,}/;
      passwordInput.validation.messages.pattern = error =>
        "Contraseña invalida. Mínimo 8 carácteres 1 mayúscula y 1 minuscula.";
      inputs = [this.formsSvc.emailInput("E-mail", "", true), passwordInput];
    }
    for (let i = inputs.length - 1; i >= 0; --i) {
      inputs[i].templateOptions["width"] = "45.5vw";
    }
    return inputs;
  }
}
