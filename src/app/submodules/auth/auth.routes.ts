import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

const routes: Routes = [
	{ path: 'user', component: LoginComponent },
	{ path: 'admin', component: LoginComponent },
	{ path: 'signup/:invitation', component: RegisterComponent },
	{ path: '**', redirectTo: 'admin' },
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AuthRoutingModule {}
