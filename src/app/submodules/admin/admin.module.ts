// Modulos requeridos
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileRoutingModule } from './admin.routes';
import { SharedModule } from '../../shared.module';
import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
// Componentes del submodulo Auth

import { AdminComponent } from './admin.component';
import { NavAdminComponent } from './components/nav-admin/nav-admin.component';
import { UsersAdminComponent } from './components/users-admin/users-admin.component';
import { PayOutAdminComponent } from './components/pay-out-admin/pay-out-admin.component';
import { CustomersAdminComponent } from './components/customers-admin/customers-admin.component';
import { InvoicesAdminComponent } from './components/invoices-admin/invoices-admin.component';
import { LoansAdminComponent } from './components/loans-admin/loans-admin.component';
import { ProfileFarmerComponent } from './components/profile-farmer/profile-farmer.component';
import { StatementsCarteraComponent } from './components/statements-cartera/statements-cartera.component';
import { LoanApplicationsComponent } from './components/loan-applications/loan-applications.component';


registerLocaleData(es);
@NgModule({
  declarations: [
    AdminComponent,
    UsersAdminComponent,
    NavAdminComponent,
    PayOutAdminComponent,
    LoansAdminComponent,
    CustomersAdminComponent,
    InvoicesAdminComponent,
    ProfileFarmerComponent,
    StatementsCarteraComponent,
    LoanApplicationsComponent,
  
  ],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    SharedModule,
      
  ],
  exports: [],
  // Solucionar proveer form service
  providers: [{ provide: LOCALE_ID, useValue: 'es-ES' }],
  bootstrap: [AdminComponent]
})
export class AdminModule { }
