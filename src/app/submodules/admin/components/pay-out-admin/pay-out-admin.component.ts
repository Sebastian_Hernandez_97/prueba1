import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { DatatableOptions, DashboardDatatableComponent } from "ngx-3a";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { MainService } from "src/app/services/main.service";
import { LoansService } from "src/app/services/loans.service";
import { Loan, EstadoRef } from "src/app/models/loans";
import { Estado } from "src/app/models/loans";
import { LoansApplicationsService } from "src/app/services/loan-applications.service";
import { BankAccountType } from "src/app/models/bank";
import * as moment from "moment";
import { InvoiceService } from "src/app/services/invoice.service";
import { FilterType } from "src/app/components/modals/apply-filters/apply-filters.component";
import { CommentsService } from "src/app/services/comments.service";

@Component({
  selector: "app-pay-out-admin",
  templateUrl: "./pay-out-admin.component.html",
  styleUrls: ["./pay-out-admin.component.scss"],
})
export class PayOutAdminComponent implements OnInit {
  @ViewChild("datatable", { static: false }) datatable: DashboardDatatableComponent;

  @ViewChild("fileDownload", { static: false })
  fileDownloadTemplate: TemplateRef<any>;

  @ViewChild("denyLoan", { static: false }) denyLoanTemplate: TemplateRef<any>;
  @ViewChild("approveLoan", { static: false }) approveLoanTemplate: TemplateRef<any>;
  @ViewChild("returnLoan", { static: false }) returnLoanTemplate: TemplateRef<any>;

  @ViewChild("date", { static: true })
  dateTemplate: TemplateRef<any>;

  @ViewChild("money", { static: true })
  moneyTemplate: TemplateRef<any>;

  @ViewChild("actions",{static:false}) actionsTemplate: TemplateRef<any>;
  /**
   * Opciones para el Datatable de 3A
   */
  options: DatatableOptions;
  showMessageEmptyData: boolean;
  showLoading = true;
  /**
   * Arreglo que contiene la información de los desembolsos
   */
  outlays: Array<object> = [];
  outlaysFilters: Array<object>;
  filters: FilterType = {
    filterOptions: [],
  };
  applicationsLoansFilters: Array<any> = [];
  applicactionsLoans: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    private userSvc: UserService,
    private router: Router,
    private mainSvc: MainService,
    private loansSvc: LoansService,
    private loansApplicationsSvc: LoansApplicationsService,
    private invoiceSvc: InvoiceService,
    private commentsSvc: CommentsService
  ) {}

  ngOnInit() {
    this.mapToOutlays();
    this.loansApplicationsSvc.getApplications().subscribe((data) => {
      this.applicactionsLoans = data;
      this.loadData(data);
    });
  }

  mapToOutlays() {
    this.loansSvc.getLoans().subscribe((res: any) => {
      if (res) {
        for (let i = res.length - 1; i >= 0; --i) {
          /**
           * Se mapean los outlay para organizar al formato que debe ser mostrado
           */
          this.outlays.push({
            name: res[i].farmer.PrimerNombre,
            id: res[i].farmer.cc,
            date: res[i].outlayDate.toDate(),
            amount: res[i].amount,
            bank: res[i].bankInformation.bank.name,
            account: res[i].bankInformation.accountNumber,
            accountType: res[i].bankInformation.accountType === BankAccountType.CHECKING ? "Corriente" : "Ahorros",
            ref: res[i].id,
          });
        }
        this.loadData(this.outlays);
      }
    });
  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData(data, columns) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    options.csvExportOptions = {
      // Genera el formato del excel
      filename: "Desembolsos" + moment().format("DD/MM/YYYY"),
      buttonEnabled: true,
      headers: [
        {
          key: "id",
          label: "Cédula",
        },
        {
          key: "name",
          label: "Nombre",
        },
        {
          key: "bank",
          label: "Banco",
        },
        {
          key: "account",
          label: "Cuenta",
        },
        {
          key: "accountType",
          label: "Tipo",
        },
        {
          key: "ref",
          label: "Referencia",
        },
        {
          key: "amount",
          label: "Valor",
        },
      ],
    };
    this.options = options;
    this.showLoading = false;
    if (data.length > 0) {
      this.showMessageEmptyData = false;
    } else {
      this.showMessageEmptyData = true;
    }
  }

  /**
   * Consulta en la base de datos la información de los desembolsos y los guarda en una variable para ser mostrados
   */

  loadData(data) {
    // Desembolso puede ver cuando estan aprobados por analisis o comité y con referenciación exitosa
    this.applicationsLoansFilters = this.applicactionsLoans;
    let data_final = [];
    let rows = [];
    data_final = data.filter((data2) => data2.Estado === 2 && data2.EstadoRef === 1);
    rows = [
      {
        name: "Nombre",
        prop: "PrimerNombre",
      },
      {
        name: "Apellido",
        prop: "PrimerApellido",
      },
      {
        name: "Cédula",
        prop: "cc",
      },
      {
        name: "Monto solicitado",
        prop: "MontoSolicitado",
        cellTemplate: this.moneyTemplate,
      },
      {
          name: "Acciones",
          cellTemplate: this.actionsTemplate,
      }
    ];
    this.loadTableData(data_final, rows);
  }

  openModal() {
    this.mainSvc.toogleFullModal.next(this.userSvc.addOrganizaitonClientEvent());
  }

  generateCSV() {
    this.datatable.generateCompleteCSV();
  }

  ModalDeny(data, event) {
    event.stopPropagation();
  }

  ModalApprove(data, event) {
    event.stopPropagation();
    this.mainSvc.toogleFullModal.next(this.loansApplicationsSvc.addbankInfoEvent(data));
    this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state: Estado.APROBADO }, () => {}));
  }

  ModalReturn(data, event) {
    // Para analisis y referenciación
    this.mainSvc.toogleFullModal.next(this.loansApplicationsSvc.ReturnElectionEvent(data));
    event.stopPropagation();
  }

  /**
   *Metodo que dispara la accion para que se muestre la modal que nos ayufa a genera la conciliación
   */
  displayFilters() {
    this.mainSvc.toogleModal.next(
      this.invoiceSvc.applyFilters(this.filters, (resp) => {
        this.filters = resp;
        this.filterToTable(resp);
      })
    );
  }

  /**
   * Este metodo filtra las facturas con las respectivas condiciones
   * @param filter parametro con filtros
   */
  filterToTable(filter) {
    let filterOld: object;
    if (filter != filterOld) {
      this.outlaysFilters = this.outlays;
      if (filter.document) {
        this.outlaysFilters = this.outlaysFilters.filter((outlay: any) => outlay.id === filter.document.toString());
      }

      if (filter.minDate) {
        this.outlaysFilters = this.outlaysFilters.filter((outlay: any) => moment(outlay.date).valueOf() >= moment(filter.minDate).valueOf());
      }

      if (filter.maxDate) {
        this.outlaysFilters = this.outlaysFilters.filter((outlay: any) => moment(outlay.date).valueOf() <= moment(filter.maxDate).valueOf());
      }

      if (filter.minAmount) {
        this.outlaysFilters = this.outlaysFilters.filter((outlay: any) => Number(outlay.amount) >= filter.minAmount);
      }
      if (filter.maxAmount) {
        this.outlaysFilters = this.outlaysFilters.filter((outlay: any) => Number(outlay.amount) <= filter.maxAmount);
      }
    }
    if (this.outlaysFilters) {
      this.loadData(this.outlaysFilters);
    }
    filterOld = filter;
  }
}
