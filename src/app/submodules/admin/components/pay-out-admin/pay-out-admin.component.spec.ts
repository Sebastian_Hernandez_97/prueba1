import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayOutAdminComponent } from './pay-out-admin.component';

describe('PayOutAdminComponent', () => {
  let component: PayOutAdminComponent;
  let fixture: ComponentFixture<PayOutAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayOutAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayOutAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
