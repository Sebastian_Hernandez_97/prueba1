import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementsCarteraComponent } from './statements-cartera.component';

describe('StatementsCarteraComponent', () => {
  let component: StatementsCarteraComponent;
  let fixture: ComponentFixture<StatementsCarteraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatementsCarteraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementsCarteraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
