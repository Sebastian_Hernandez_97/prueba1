import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { DatatableOptions } from "ngx-3a";
import { UserService } from "src/app/services/user.service";
import { DatabaseQueryRelation } from "3a-common/dist/database";
import { UserRole } from "src/app/models/user";
import { switchMap, map } from "rxjs/operators";
import { LoansService } from "src/app/services/loans.service";
import * as moment from "moment";
import { AngularFireFunctions } from '@angular/fire/functions';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: "app-statements-cartera",
  templateUrl: "./statements-cartera.component.html",
  styleUrls: ["./statements-cartera.component.scss"]
})
export class StatementsCarteraComponent implements OnInit {
  @ViewChild('money', { static: false }) moneyTemplate: TemplateRef<any>;
  /**
   * Opciones para el Datatable de 3A
   */
  options: DatatableOptions;
  showSearchDateAnimation: boolean;
  arrayStatements = [];
  arrayFinal = [];
  constructor(private userSvc: UserService, private loanSvc: LoansService) { }

  ngOnInit() {
  //  this.getStatements();
  }
  showSearchDate() {
    this.showSearchDateAnimation = !this.showSearchDateAnimation;
  }

  getStatements() {
   
  }

  calculateMora() {

    const dummyRows = [
      {
        name: 'Nombre',
        prop: 'userName'
      },
      {
        name: 'Cédula',
        prop: 'userCC'
      },
      {
        name: 'Monto',
        prop: 'userAmount',
        cellTemplate: this.moneyTemplate
      },
      {
        name: 'Monto de la mora',
        prop: 'amountMora',
        cellTemplate: this.moneyTemplate
      },
      {
        name: 'Nivel de la mora',
        prop: 'levelMora',
        cellClass: (data) => {
          return (data.value === 'T1') ? 'level-mora.t1' : '';
        }
      },
      {
        name: 'Etado de la mora',
        prop: 'stateOfMora',
      }
    ];

    this.loadTableData(this.arrayFinal, dummyRows);
  }

  loadTableData(data, columns) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    this.options = options;
  }

}
