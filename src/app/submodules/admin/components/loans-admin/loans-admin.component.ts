import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { MainService } from "src/app/services/main.service";
import { LoansService } from "src/app/services/loans.service";
import { DatatableOptions } from "ngx-3a";
import { Loan } from "src/app/models/loans";

@Component({
  selector: "app-loans-admin",
  templateUrl: "./loans-admin.component.html",
  styleUrls: ["./loans-admin.component.scss"],
})
export class LoansAdminComponent implements OnInit {
  @ViewChild("money", { static: false }) moneyTemplate: TemplateRef<any>;
  @ViewChild("months", { static: false }) monthsTemplate: TemplateRef<any>;
  @ViewChild("percent", { static: false }) percentTemplate: TemplateRef<any>;
  /**
   * Opciones para el Datatable de 3A
   */
  options: DatatableOptions;
  showMessageEmptyData: boolean;
  showLoading = true;
  constructor(
    private route: ActivatedRoute,
    private userSvc: UserService,
    private loansSvc: LoansService,
    private router: Router,
    private mainSvc: MainService
  ) {}

  ngOnInit() {
    this.loansSvc.getLoans().subscribe((data) => {
      const dummyRows = [
        {
          name: "Agricultor",
          prop: "farmer.PrimerNombre",
        },
        {
          name: "Periodicidad de Pago",
          prop: "paymentPeriodicity",
          cellTemplate: this.monthsTemplate,
        },
        {
          name: "Tasa de interés",
          prop: "interestRate",
          cellTemplate: this.percentTemplate,
        },
        {
          name: "Valor",
          prop: "amount",
          cellTemplate: this.moneyTemplate,
        },
      ];
      this.loadTableData(data, dummyRows);
    });
  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData(data, columns) {

    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    this.options = options;
    this.showLoading = false;
    if (data.length > 0) {
      this.showMessageEmptyData = false;
    } else {
      this.showMessageEmptyData = true;
    }
  }

  /**
   * Método para agregar un crédito en la aplicación
   */
  openModal() {
    this.mainSvc.toogleFullModal.next(this.loansSvc.addLoanEvent());
  }

  /**
   * Método para ver la descripción de un credito
   * @param loan cédito que se desea abrir
   */
  openLoan(loan: Loan) {
    this.router.navigateByUrl("/admin/loans/" + loan.id + "/" + loan.farmer.cc);
  }
}
