import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoansAdminComponent } from './loans-admin.component';

describe('LoansAdminComponent', () => {
  let component: LoansAdminComponent;
  let fixture: ComponentFixture<LoansAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoansAdminComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoansAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
