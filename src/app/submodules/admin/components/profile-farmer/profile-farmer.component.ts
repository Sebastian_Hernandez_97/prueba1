import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { map, finalize, take, pluck } from "rxjs/operators";
import { UserService } from "src/app/services/user.service";
import * as moment from "moment";
import { RxFormGroup, RxFormBuilder, async } from "@rxweb/reactive-form-validators";
import { PersonalDataModel, financialDataModel, ReferecesDataModel, RefereceModel, ReferecePersonalModel, UserRole } from "src/app/models/user";
import { FormGroup } from "@angular/forms";
import { LoansService } from "src/app/services/loans.service";
import { Loan, Estado } from "src/app/models/loans";
import { AngularFireStorage } from "@angular/fire/storage";
import { Location } from "@angular/common";
import { AlertService } from "src/app/services/alerts-message.service";
import { MainService } from "src/app/services/main.service";
import { LoansApplicationsService } from "src/app/services/loan-applications.service";
import { AddFileAttachmentService } from "src/app/services/add-file-attachment.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { CommentsService } from "src/app/services/comments.service";

@Component({
  selector: "app-profile-farmer",
  templateUrl: "./profile-farmer.component.html",
  styleUrls: ["./profile-farmer.component.scss"],
})
export class ProfileFarmerComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private mainSvc: MainService,
    private formBuilder: RxFormBuilder,
    private loansSrv: LoansService,
    private userSvc: UserService,
    private storage: AngularFireStorage,
    private location: Location,
    private alertSvc: AlertService,
    private loansApplicationsSvc: LoansApplicationsService,
    private addFileAttachmentSvc: AddFileAttachmentService,
    private afAuth: AngularFireAuth,
    private commentsSvc: CommentsService
  ) {}

  formFinancialData: Array<object> = [];
  formPersonalData: Array<object> = [];
  formReferencesData: Array<object> = [];
  personalDataFormGroup: FormGroup;
  financilaDataFormGroup: FormGroup;
  referencesDataFormGroup: FormGroup;
  departamentsFinancialData = [];
  citiesFinancialData = [];
  departamentsPersonalData = [];
  citiesPersonalData = [];
  sendCorrespondence = [];
  files: Array<any> = [];
  user: any;
  percentajeLoaderProfilePhoto: number = 472;
  strokeColor: string = "#1479fb";
  soporteIngresos: boolean = true;
  firma: string;
  userLogged: any;

  ngOnInit() {
    this.departamentsFinancialData = this.loansSrv.DEPARTMENTS;
    this.departamentsPersonalData = this.loansSrv.DEPARTMENTS;

    this.route.paramMap.subscribe(async (params) => {
      const userId = params.get("userId");
      if (userId) {
        this.userSvc
          .getUser(userId)
          .pipe(take(1))
          .subscribe((user) => {
            if (user) {
              this.authUserLogged();
              this.user = user;
              this.buildArrayCitiesPersonalData();
              this.buildArrayCitiesFinancialData();
              this.getForms();
            } else {
              this.loansApplicationsSvc
                .getApplication(userId)
                .pipe(take(1))
                .subscribe((user) => {
                  if (user) {
                    this.authUserLogged();
                    this.user = user;
                    this.buildArrayCitiesPersonalData();
                    this.buildArrayCitiesFinancialData();
                    this.getForms();
                  }
                });
            }
          });
        this.initForms();
      }
    });
  }

  /**
   * Metodo que verifica el usuario logueado
   */
  authUserLogged() {
    this.afAuth.authState.pipe(map((user) => user.uid)).subscribe(async (id) => {
      return this.userSvc.getUser(id).subscribe((currentUser) => {
        this.userLogged = currentUser;
        if (this.userLogged.role != UserRole.ADMIN && (this.userLogged.role != UserRole.COMERCIAL || this.user["Estado"] != 0)) {
          this.referencesDataFormGroup.disable();
          this.personalDataFormGroup.disable();
          this.financilaDataFormGroup.disable();
        }
      });
    });
  }

  /**
   * Metodo que incial los furmularios
   */
  initForms() {
    let personalData = new PersonalDataModel();
    let financialdata = new financialDataModel();
    let referencesData = new ReferecesDataModel();
    referencesData.familiar_reference_1 = new RefereceModel();
    referencesData.familiar_reference_2 = new RefereceModel();
    referencesData.personal_reference_1 = new ReferecePersonalModel();
    referencesData.personal_reference_2 = new ReferecePersonalModel();
    this.personalDataFormGroup = this.formBuilder.formGroup(personalData);
    this.financilaDataFormGroup = this.formBuilder.formGroup(financialdata);
    this.referencesDataFormGroup = this.formBuilder.formGroup(referencesData);
    //this.authUserLogged();
  }

  /**
   * Meotdo para construir el array de ciudades para el departamento seleccionado en los datos financieros
   */
  buildArrayCitiesFinancialData() {
    if (this.user && this.user["DepartamentoActividad"]) {
      this.financilaDataFormGroup.controls.MunicipioActividad.setValue(this.user["MunicipioActividad"]);
      delete this.user["MunicipioActividad"];
    } else {
      this.citiesFinancialData = this.departamentsFinancialData.find((cities) => cities.departamento === "Antioquia");
    }
    this.financilaDataFormGroup.controls.DepartamentoActividad.valueChanges.subscribe((res) => {
      this.citiesFinancialData = this.departamentsFinancialData.find((cities) => cities.departamento === res);
    });
  }

  /**
   * Meotdo para construir el array de ciudades para el departamento seleccionado en los datos personales
   */
  buildArrayCitiesPersonalData() {
    if (this.user && this.user["DepartamentoRes"]) {
      this.personalDataFormGroup.controls.MunicipioRes.setValue(this.user["MunicipioRes"]);
      delete this.user["MunicipioRes"];
    } else {
      this.citiesPersonalData = this.departamentsPersonalData.find((cities) => cities.departamento === "Antioquia");
    }
    this.personalDataFormGroup.controls.DepartamentoRes.valueChanges.subscribe((res) => {
      this.citiesPersonalData = this.departamentsPersonalData.find((cities) => cities.departamento === res);
    });
  }

  sendCorrespondense(option) {
    const i = this.sendCorrespondence.findIndex((el) => el.id === option.id);
    if (i >= 0) {
      this.sendCorrespondence.splice(i, 1);
    } else {
      this.sendCorrespondence.push(option);
    }
  }

  /**
   * Metodo que llama el servicio para traer los formularos, dependiendos del id los lleva a otro metodos
   */
  getForms() {
    this.userSvc
      .getForms()
      .pipe(
        map((form: any) => {
          return { id: form[0].id, forms: form[0].forms };
        })
      )
      .subscribe((form) => {
        switch (form.id) {
          case "financialdata":
            this.mapToFinancialData(form);
            break;
          case "personalData":
            this.mapToPersonalData(form);
            break;

          case "references":
            this.mapToReferences(form);
            break;
          default:
            break;
        }

        if (this.formFinancialData.length && this.formPersonalData.length && this.formReferencesData.length) this.mapToInfoEmpty();
      });
  }

  /**
   * Metodo que permite mapear la informacion financiera del agricultor
   * @param form parametro que recibe el formulario
   */
  mapToFinancialData(form) {
    const forms = form.forms;
    forms.forEach((form, index) => {
      form.fields.forEach((element, index) => {
        if (this.user.hasOwnProperty(element.key)) {
          let temp = {
            ...element,
            valueUser: element.type === "date" ? moment(this.user[element.key]).format("l") : this.user[element.key],
          };
          this.financilaDataFormGroup.controls[element.key].setValue(temp.valueUser);
          this.formFinancialData.push(temp);
          delete this.user[element.key];
        } else {
          let temp = {
            ...element,
            valueUser: "",
          };
          this.formFinancialData.push(temp);
        }
      });
    });
  }

  /**
   * Metodo que permite mapear la informacion personal del agricultor
   * @param form parametro que recibe el formulario
   */
  mapToPersonalData(form) {
    const forms = form.forms;
    forms.forEach((form, index) => {
      form.fields.forEach((element, index) => {
        if (this.user.hasOwnProperty(element.key)) {
          let temp = {
            ...element,
            valueUser: element.type === "date" ? moment(this.user[element.key].seconds * 1000).format("l") : this.user[element.key],
          };
          this.personalDataFormGroup.controls[element.key].setValue(temp.valueUser);
          this.formPersonalData.push(temp);
          delete this.user[element.key];
        } else {
          let temp = {
            ...element,
            valueUser: "",
          };
          this.formPersonalData.push(temp);
        }
      });
    });
  }

  /**
   * Metodo que permite mapear las referencias del agricultor
   * @param form parametro que recibe el formulario
   */
  mapToReferences(form) {
    const forms = form.forms;
    for (let index = 0; index < forms.length; index++) {
      const element = forms[index];
      if (this.user.hasOwnProperty(element.id)) {
        let temporal = this.user[element.id];
        element.fields.forEach((field) => {
          for (const key in temporal) {
            if (key === field.key) {
              field.valueUser = temporal[key] ? temporal[key] : "";
              this.referencesDataFormGroup.controls[element.id]["controls"][field.key].setValue(field.valueUser);
            }
          }
        });
        this.formReferencesData.push(element);
        delete this.user[element.key];
      } else {
        element.fields.forEach((field) => {
          field.valueUser = "";
        });
        this.formReferencesData.push(element);
      }
    }
  }

  /**
   * Metodo que mapea el restante del objeto user despues de pasar por las funciones principales
   */
  mapToInfoEmpty() {
    console.log("LOGGG", this.user);
    for (const key in this.user) {
      let element = this.user[key];
      if (key === "farmerphotos") {
        for (let index = 0; index < this.user.farmerphotos.length; index++) {
          const file = this.user.farmerphotos[index];
          this.loadToFiles({ element: file, key: Object.keys(file)[0] });
        }
      }
      if (key === "Firma") {
        this.files.push({
          fileOriginal: element,
          fileEdit: element,
          fileName: key,
          type: "firma",
        });
      }
    }
  }
  /**
   * Este metodo mapea la informacion dandole un tipo al archivo para luego ser presentado
   * en la vista
   * @param element parametro que trae el archivo adjunto
   */
  loadToFiles(element) {
    const file = element.element[element.key];
    const fileTemp = element.element[element.key];
    let match = /\?alt/.exec(fileTemp);
    this.files.push({
      fileEdit: match.index ? fileTemp.slice(0, match.index) : fileTemp,
      fileOriginal: file,
      fileName: element.key,
    });
    this.files.forEach((element, index) => {
      if (element.fileEdit.match(/.(jpeg|jpg|gif|png|webp)/g) != null) {
        this.files[index].type = "image";
      } else if (element.fileEdit.match(/.pdf/g) != null) {
        this.files[index].type = "pdf";
      } else if (element.fileEdit.match(/.(docx|doc)/g) != null) {
        this.files[index].type = "docx";
      } else if (element.fileEdit.match(/.xlsx/g) != null) {
        this.files[index].type = "xlsx";
      }
    });
  }

  /**
   * Metodo para guardar o actualizar los datos del agricultor
   */
  saveToFarmer() {
    this.personalDataFormGroup.value.FechaExpedicion = moment(this.personalDataFormGroup.value.FechaExpedicion).valueOf();
    this.personalDataFormGroup.value.FechaNacimiento = moment(this.personalDataFormGroup.value.FechaNacimiento).valueOf();
    let farmerTemp = { ...this.user, ...this.personalDataFormGroup.value, ...this.financilaDataFormGroup.value, ...this.referencesDataFormGroup.value };

    if (this.user.applicationsLoan === true) {
      this.loansApplicationsSvc.saveApplication(farmerTemp).subscribe((resp) => {
        if (resp) {
          let messageAlert = {
            icon: "/assets/Alerta- Conciliacion manual.svg",
            text: "¡Agricultor actualizado con éxito!",
            button1: "Seguir editando",
            button2: "Volver al historial",
          };
          this.showConfirmationAlert(messageAlert);
        }
      });
    } else {
      this.userSvc.saveUser(farmerTemp).subscribe((farmer) => {
        this.loansSrv
          .getUserLoans(farmer.id)
          .pipe(take(1))
          .subscribe((loans) => {
            loans.forEach((element: Loan) => {
              this.loansSrv.saveLoan({ ...element, farmer: farmer });
              if (element) {
                let messageAlert = {
                  icon: "/assets/Alerta- Conciliacion manual.svg",
                  text: "¡Agricultor actualizado con éxito!",
                  button1: "Seguir editando",
                  button2: "Volver al historial",
                };
                this.showConfirmationAlert(messageAlert);
              }
            });
          });
      });
    }
  }

  /**
   * Metodo para guardar o actualizar los datos del agricultor sin alertas
   */
  saveToFarmerNoAlert() {
    this.personalDataFormGroup.value.FechaExpedicion = moment(this.personalDataFormGroup.value.FechaExpedicion).valueOf();
    this.personalDataFormGroup.value.FechaNacimiento = moment(this.personalDataFormGroup.value.FechaNacimiento).valueOf();
    let farmerTemp = { ...this.user, ...this.personalDataFormGroup.value, ...this.financilaDataFormGroup.value, ...this.referencesDataFormGroup.value };

    if (this.user.applicationsLoan === true) {
      this.loansApplicationsSvc.saveApplication(farmerTemp).subscribe((resp) => {
        if (resp) {
        }
      });
    } else {
      this.userSvc.saveUser(farmerTemp).subscribe((farmer) => {
        this.loansSrv
          .getUserLoans(farmer.id)
          .pipe(take(1))
          .subscribe((loans) => {
            loans.forEach((element: Loan) => {
              this.loansSrv.saveLoan({ ...element, farmer: farmer });
            });
          });
      });
    }
  }

  /**
   * Este metodo se utiliza para mostrar en la vista una mejor visualizacion del nombre del documento
   * @param name nombre del documento
   */
  toCapitalizeDocumentName(name) {
    return name.replace(/([A-Z-0-9])/g, " $1").replace(/^./, (str) => str.toUpperCase());
  }

  /**
   * Metodo para abrir la modal de confirmacion
   */

  showConfirmationAlert(messageAlert) {
    this.mainSvc.toogleModal.next(
      this.alertSvc.showAlert(messageAlert, (resp) => {
        if (resp.action) {
          this.location.back();
        }
      })
    );
  }
  /**
   * Metodo que se encargar de guardar la foto de perfil del agricultor
   * @param event evento que trae la foto de perfil para actualizar
   */
  photoProfileUpload(event) {
    const file = event.target.files[0];
    const filePath = `Prospeccion/${this.user["id"]}/photoProfile`;
    const fileRef = this.storage.ref(filePath);
    this.strokeColor = "#1479fb";
    const task = this.storage.upload(filePath, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            this.user["photoProfile"] = url;
            if (url) {
              setTimeout(() => {
                this.percentajeLoaderProfilePhoto = 472;
              }, 2500);
            }
            this.saveToFarmer();
          });
        })
      )
      .subscribe();

    task.percentageChanges().subscribe((percentaje) => {
      if (Math.round(percentaje) < 90) {
        this.percentajeLoaderProfilePhoto = 30;
      } else if (Math.round(percentaje) === 100) {
        this.percentajeLoaderProfilePhoto = 0;
        setTimeout(() => {
          this.strokeColor = "#fff";
        }, 500);
      }
    });
  }

  /**
   * Metodo para actualizar o guardar un nuevo archivo
   * @param atributes atributos para guardar o actualizar los archivos
   */
  addFileAttachment(atributes?) {
    if (!atributes) {
      atributes = {
        nameFile: "",
        url: "",
      };
    }
    atributes.user = this.user.id;
    this.mainSvc.toogleModal.next(
      this.addFileAttachmentSvc.showModalAddFile(atributes, (resp) => {
        if (resp) {
          let findIdx = this.user.farmerphotos.findIndex((el) => Object.keys(el)[0] === resp.nameFile);
          if (findIdx > -1) {
            this.user.farmerphotos[findIdx] = { [this.toDeleteSpaces(resp.nameFile)]: resp.url };
          } else {
            this.user.farmerphotos.push({ [this.toDeleteSpaces(resp.nameFile)]: resp.url });
          }
          this.saveToFarmerNoAlert();
          this.files = [];
          this.mapToInfoEmpty();
        }
      })
    );
  }

  /**
   * Meotodo para descargar el archivo seleccionado
   * @param file objecto con el nombre y url del archivo
   */
  donwloadFile(file) {
    this.getBase64ImageFromUrl(file.fileOriginal).then((result: string) => {
      var a = document.createElement("a");
      a.href = result;
      a.download = file.fileName;
      a.click();
    });
  }

  /**
   * Metodo que convierte la imagen a base64 para descargar en local
   * @param imageUrl url de la imagen
   */
  async getBase64ImageFromUrl(imageUrl) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();

    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.addEventListener(
        "load",
        function () {
          resolve(reader.result);
        },
        false
      );

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    });
  }

  /**
   * Meotdo el cual recibe un string y se le borran los espacios en blanco que tenga
   * @param name nombre al cual se le van a borrar los espacios en blanco
   */
  toDeleteSpaces(name) {
    return name.replace(/\s+/g, "");
  }

  sendApplicationLoan() {
    this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data: this.user, state: Estado.ANALISIS }, () => {
      this.authUserLogged();
    }));
    
  }
  /**
   * metodo para volver atras
   */
  back() {
    this.location.back();
  }
}
