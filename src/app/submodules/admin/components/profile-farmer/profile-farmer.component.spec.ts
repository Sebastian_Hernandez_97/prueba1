import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileFarmerComponent } from './profile-farmer.component';

describe('ProfileFarmerComponent', () => {
  let component: ProfileFarmerComponent;
  let fixture: ComponentFixture<ProfileFarmerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFarmerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFarmerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
