import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesAdminComponent } from './invoices-admin.component';

describe('InvoicesAdminComponent', () => {
  let component: InvoicesAdminComponent;
  let fixture: ComponentFixture<InvoicesAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
