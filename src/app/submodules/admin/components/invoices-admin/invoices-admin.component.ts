import { Component, OnInit, ViewChild, TemplateRef, OnDestroy, ElementRef } from "@angular/core";
import { DatatableOptions, DashboardDatatableComponent } from "ngx-3a";
import { ActivatedRoute, Router } from "@angular/router";
import { MainService } from "src/app/services/main.service";
import { QuotesService } from "src/app/services/quotes.service";
import { UserService } from "src/app/services/user.service";
import { ConciliationService } from "src/app/services/conciliation.service";
import { LoansService } from "src/app/services/loans.service";
import { InvoicesPdfService } from "src/app/services/invoces-pdf.service";
import { filter, map } from "rxjs/operators";
import { Subscription } from "rxjs";
import { InvoiceService } from "src/app/services/invoice.service";
import * as moment from "moment";
import { FilterType } from "src/app/components/modals/apply-filters/apply-filters.component";

@Component({
  selector: "app-invoices-admin",
  templateUrl: "./invoices-admin.component.html",
  styleUrls: ["./invoices-admin.component.scss"],
})
export class InvoicesAdminComponent implements OnInit, OnDestroy {
  @ViewChild("datatable", { static: false }) datatable: DashboardDatatableComponent;

  @ViewChild("fileDownload", { static: false })
  fileDownloadTemplate: TemplateRef<any>;

  @ViewChild("date", { static: true })
  dateTemplate: TemplateRef<any>;

  @ViewChild("mora", { static: true })
  moraTemplate: TemplateRef<any>;

  @ViewChild("money", { static: true })
  moneyTemplate: TemplateRef<any>;

  @ViewChild("buttonDownloadInvoices", { static: true })
  buttonDownloadInvoices: ElementRef;

  @ViewChild("buttonDownloadInvoices", { static: true })
  buttonDownloadDataCredit: ElementRef;

  positionFiltersDownload: string;
  idButtonTemp: string = "";
  showFiltersDownloadsAnimated: boolean = false;
  options: DatatableOptions;
  loans: any;
  invoices: Array<any> = [];
  invoicesToConciliation: Array<any> = [];
  loansSubscribe$: Subscription;
  invoicesFilters: Array<any> = [];
  showMessageEmptyData: boolean;
  showLoading = true;
  filters: FilterType = {
    filterOptions: [
      { label: "Sin mora", value: "0" },
      { label: "Mora T1", value: "1" },
      { label: "Mora T2", value: "2" },
      { label: "Mora T3", value: "3" },
      { label: "Mora T4", value: "4" },
    ],
  };

  constructor(
    private conciliationSvc: ConciliationService,
    private mainSvc: MainService,
    private loansSvc: LoansService,
    private invoicePdfSvc: InvoicesPdfService,
    private quoteSvc: QuotesService,
    private invoiceSvc: InvoiceService
  ) {}

  ngOnInit() {
    this.getAllQuotes();
  }
  ngOnDestroy() {
    this.loansSubscribe$.unsubscribe();
  }

  /**
   * Metodo que trae todas las cuotas de los creidtos
   */
  getAllQuotes() {
    this.loansSubscribe$ = this.loansSvc.getLoans().subscribe((loans: any) => {
      if (loans.length) {
        for (let index = 0; index < loans.length; index++) {
          const element = loans[index];
          this.quoteSvc.getLoanQuotes(element.id).subscribe((quotes: any) => {
            this.mapQuotesASingleArray(quotes, element);
          });
        }
      } else {
        this.showLoading = false;
        this.showMessageEmptyData = true;
      }
    });
  }

  /**
   * Metodo que filtra las cuotas y saca solo las que ya han sido facturadas
   * @param quotes parametro que trae las coutas por cada loan
   */
  mapQuotesASingleArray(quotes, loan) {
    for (let index = 0; index < quotes.length; index++) {
      const element = quotes[index];
      if (element.invoice) {
        this.invoicesToConciliation.push({ quote: element, ...loan });
        this.invoices.push({
          primerNombre: loan.farmer.PrimerNombre,
          primerApellido: loan.farmer.PrimerApellido,
          cc: loan.farmer.cc,
          date: moment(element.invoice.date).format("l"),
          billingCycle: loan.billingCycle,
          total: element.invoice.total.toFixed(2),
          mora: loan.mora ? loan.mora : "",
          numberInvoice: element.invoice.numberInvoice,
          expirationDate: moment(element.invoice.expirationDate).format("l"),
          interest: element.invoice.interest.toFixed(2),
          others: element.invoice.others.toFixed(2),
          paymentCapital: element.invoice.paymentCapital.toFixed(2),
          amount: loan.amount,
        });
      }
    }
    if (this.invoices.length) {
      this.loadData(this.invoices);
    } else {
      this.showLoading = false;
      this.showMessageEmptyData = true;
    }
  }

  /**
   * Carga los datos que se nececitaran para mostrar en la tabla
   */

  loadData(data, dateExc?) {
    const rows = [
      {
        name: "Nombre",
        prop: "primerNombre",
      },
      {
        name: "Cédula",
        prop: "cc",
      },
      {
        name: "Fecha",
        prop: "date",
        cellTemplate: this.dateTemplate,
        flexGrow: 3,
      },
      {
        name: "Ciclo",
        prop: "billingCycle",
      },
      {
        name: "Valor factura",
        prop: "total",
        cellTemplate: this.moneyTemplate,
      },
      {
        name: "Nivel de la mora",
        prop: "r",
        cellTemplate: this.moraTemplate,
      },
      {
        name: "Descargar",
        prop: "amount",
        cellTemplate: this.fileDownloadTemplate,
      },
    ];
    this.loadTableData(data, rows, dateExc);
  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData(data, columns, dataExc?) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    options.excelExportOptions = {
      filename: "facturas" + moment().valueOf(),
      data: dataExc ? dataExc : [],
      headers: [
        {
          key: "numberInvoice",
          label: "Número de factura",
        },
        {
          key: "cc",
          label: "Cédula",
        },
        {
          key: "primerNombre",
          label: "Nombre",
        },
        {
          key: "primerApellido",
          label: "Apellido",
        },
        {
          key: "date",
          label: "Fecha",
        },
        {
          key: "expirationDate",
          label: "Fecha expiración",
        },
        {
          key: "interest",
          label: "Interes",
        },
        {
          key: "mora",
          label: "Mora",
        },
        {
          key: "others",
          label: "Valor otros",
        },
        {
          key: "paymentCapital",
          label: "Pago a capital",
        },
        {
          key: "total",
          label: "Total",
        },
      ],
    };
    this.options = options;
    this.showLoading = false;
    if (data.length > 0) {
      this.showMessageEmptyData = false;
    } else {
      this.showMessageEmptyData = true;
    }
  }

  /**
   * Metodo que controla visualmente el filtro por meses
   * @param $event Objeto del evento click para descargar datacredito o facturas
   */
  showFiltersDownloads($event) {
    if (this.idButtonTemp === $event.target.id) {
      this.showFiltersDownloadsAnimated = false;
      this.idButtonTemp = "";
    } else {
      this.showFiltersDownloadsAnimated = true;
      this.idButtonTemp = $event.target.id;
    }
    switch ($event.target.id) {
      case "buttonInvoices":
        this.positionFiltersDownload = `${$event.target.offsetLeft - 167}px`;
        //this.downloadInvoices()
        break;
      case "buttonDataCredit":
        this.positionFiltersDownload = `${$event.target.offsetLeft - 137}px`;
      default:
        break;
    }
  }

  /**
   * Metodo para cerrar el filtro por meses
   */
  closeFiltersDownloads() {
    this.showFiltersDownloadsAnimated = false;
    this.idButtonTemp = "";
  }

  /**
   * Metodo que descarga las facturas  o el reporte de datacredito
   * @param date parametro que trae la fecha seleccioanda para la descarga
   */
  dateSelectFiltersDownload(date) {
    let invoicesToDownloadExc = this.invoices;
    switch (this.idButtonTemp) {
      case "buttonInvoices":
        invoicesToDownloadExc = invoicesToDownloadExc.filter((invoice) => moment(invoice.date).valueOf() >= moment(date).startOf("month").valueOf());
        invoicesToDownloadExc = invoicesToDownloadExc.filter((invoice) => moment(invoice.date).valueOf() <= moment(date).endOf("months").valueOf());
        this.loadData(this.invoices, invoicesToDownloadExc);
        this.closeFiltersDownloads();
        setTimeout(() => {
          this.downloadInvoices();
        }, 200);
        break;
      case "buttonDataCredit":

      default:
        break;
    }
  }

  /**
   * Metodo para descargar las facturas filtradas por mes
   */
  downloadInvoices() {
    this.datatable.generateCompleteExcel();
  }

  /**
   *Metodo que dispara la accion para que se muestre la modal que nos ayuda a genera la conciliación
   */
  generateConciliation() {
    this.mainSvc.toogleModal.next(this.conciliationSvc.addOrganizaitonClientEvent(this.invoicesToConciliation));
  }

  /**
   *Metodo que dispara la accion para que se muestre la modal de los filtros
   */
  displayFilters() {
    this.mainSvc.toogleModal.next(
      this.invoiceSvc.applyFilters(this.filters, (resp) => {
        this.filters = resp;
        this.filterToTable(resp);
      })
    );
  }

  /**
   * Este metodo filtra las facturas con las respectivas condiciones
   * @param filter parametro con filtros
   */
  filterToTable(filter) {
    let filterOld: object;
    if (filter != filterOld) {
      this.invoicesFilters = this.invoices;
      if (filter.document) {
        this.invoicesFilters = this.invoicesFilters.filter((invoice) => invoice.cc === filter.document.toString());
      }

      if (filter.minDate) {
        this.invoicesFilters = this.invoicesFilters.filter((invoice) => moment(invoice.date).valueOf() >= moment(filter.minDate).valueOf());
      }

      if (filter.maxDate) {
        this.invoicesFilters = this.invoicesFilters.filter((invoice) => moment(invoice.date).valueOf() <= moment(filter.maxDate).valueOf());
      }

      if (filter.minAmount) {
        this.invoicesFilters = this.invoicesFilters.filter((invoice) => Number(invoice.amount) >= filter.minAmount);
      }
      if (filter.maxAmount) {
        this.invoicesFilters = this.invoicesFilters.filter((invoice) => Number(invoice.amount) <= filter.maxAmount);
      }
    }

    if (this.invoicesFilters) {
      this.loadData(this.invoicesFilters);
    }
    filterOld = filter;
  }

  /**
   * Metodo que se ejecuta cuando el usuario da click en alguna de las facturas de la tabla
   * @param event Evento que trae los datos de la factura
   */
  openInvoice(event) {
    this.invoicePdfSvc.generatePdf(event);
  }
}
