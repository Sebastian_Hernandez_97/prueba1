import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DatatableOptions, DashboardDatatableComponent } from "ngx-3a";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { MainService } from "src/app/services/main.service";
import { DatabaseQueryRelation } from "3a-common/dist/database";
import { UserRole, User } from "src/app/models/user";
import * as moment from "moment";

@Component({
  selector: "app-customers-admin",
  templateUrl: "./customers-admin.component.html",
  styleUrls: ["./customers-admin.component.scss"],
})
export class CustomersAdminComponent implements OnInit {
  @ViewChild("role", { static: false }) roleTemplate: TemplateRef<any>;
  @ViewChild("status", { static: false }) statusTemplate: TemplateRef<any>;
  @ViewChild("datatable", { static: false }) datatable: DashboardDatatableComponent;
  /**
   * Opciones para el Datatable de 3A
   */
  options: DatatableOptions;
  showMessageEmptyData: boolean;
  showLoading = true;
  constructor(private route: ActivatedRoute, private userSvc: UserService, private router: Router, private mainSvc: MainService) {}

  ngOnInit() {
    this.userSvc.getUsers([{ key: "role", relation: DatabaseQueryRelation.Equal, value: UserRole.FARMER }]).subscribe((data) => {
      const dummyRows = [
        {
          name: "Nombre",
          prop: "PrimerNombre",
        },
        {
          name: "Cédula",
          prop: "cc",
        },
        {
          name: "Dirección",
          prop: "DireccionRes",
        },
      ];
      this.loadTableData(data, dummyRows);
    });
  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData(data, columns) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    options.csvExportOptions = {
      filename: "Clientes" + moment().format("DD/MM/YYYY"),
      buttonEnabled: true,
      headers: [
        {
          key: "cc",
          label: "Cédula",
        },
        {
          key: "PrimerNombre",
          label: "Nombre",
        },
        {
          key: "PrimerApellido",
          label: "Apellido",
        },
        {
          key: "cellphone",
          label: "Celular",
        },
      ],
    };
    this.options = options;
    this.showLoading = false;
    if (data.length > 0) {
      this.showMessageEmptyData = false;
    } else {
      this.showMessageEmptyData = true;
    }
  }

  generateCSV() {
    this.datatable.generateCompleteCSV();
  }
}
