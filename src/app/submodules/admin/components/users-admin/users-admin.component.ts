import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DatatableOptions } from 'ngx-3a';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { MainService } from 'src/app/services/main.service';
import { User, UserRole } from 'src/app/models/user';
import { DatabaseQueryRelation } from '3a-common/dist/database';

@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.scss']
})
export class UsersAdminComponent implements OnInit {
  @ViewChild('role', { static: false }) roleTemplate: TemplateRef<any>;
  @ViewChild('status', { static: false }) statusTemplate: TemplateRef<any>;
  showMessageEmptyData: boolean;
  showLoading = true;

  /**
   * Opciones para el Datatable de 3A
   */
  options: DatatableOptions;


  constructor(
    private route: ActivatedRoute,
    private userSvc: UserService,
    private router: Router,
    private mainSvc: MainService
  ) { }

  ngOnInit() {


    this.userSvc.getUsers([{ key: 'role', relation: DatabaseQueryRelation.NotEqual, value: UserRole.FARMER }]).subscribe(data => {
      const dummyRows = [
        {
          name: 'Nombre',
          prop: 'name'
        },
        {
          name: 'E-mail',
          prop: 'email'
        },
        {
          name: 'Estado',
          prop: 'status',
          cellTemplate: this.statusTemplate
        }
      ];
      this.loadTableData(data, dummyRows);
    });

  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData(data, columns) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    this.options = options;
    this.showLoading = false;
    if (data.length > 0) {
      this.showMessageEmptyData = false;
    } else {
      this.showMessageEmptyData = true;
    }
  }
  /**
   * Método para agregar usuarios a la aplicación
   */
  openModal() {
    this.mainSvc.toogleModal.next(this.userSvc.addOrganizaitonClientEvent());
  }

  /**
   * Método encargado de activar o desactivar un usuario
   * @param data usuario que se modificara
   */
  checkValue(data: User) {
    data.status = data.status === 0 ? 1 : 0;
    const a = data;
    // tslint:disable-next-line:no-string-literal
    delete a['_c_o_l_u_m_n__f_i_l_t_e_r'];
    this.userSvc.saveUser(data).subscribe();
  }
}
