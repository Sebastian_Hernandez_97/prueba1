import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { map } from "rxjs/operators";
import { UserRole } from "src/app/models/user";
import { Subscription } from "rxjs";

@Component({
  selector: "app-nav-admin",
  templateUrl: "./nav-admin.component.html",
  styleUrls: ["./nav-admin.component.scss"],
})
export class NavAdminComponent implements OnInit, OnDestroy {
  currentTab = "loans";
  currentMenu = [];
  observableAth$: Subscription;
  constructor(private router: Router, private afAuth: AngularFireAuth, private userSvc: UserService) {}

  ngOnInit() {
    this.buildCurrentMenu();
    this.router.events.subscribe((event: any) => {
      if (event.url !== undefined) {
      }
    });
  }

  buildCurrentMenu() {
  this.userSvc.authUserLogged().then((currentUser) => {
        switch (currentUser.role) {
          case UserRole.ANALISTA:
            this.currentMenu = [{ title: "Análisis", url: "applications-loans", icon: "/assets/admin-desembolso.svg" }];
            break;
          case UserRole.COMERCIAL:
            this.currentMenu = [{ title: "Solicitudes", url: "applications-loans", icon: "/assets/admin-desembolso.svg" }];
            break;

          case UserRole.DESEMBOLSO:
            this.currentMenu = [{ title: "Desembolsos", url: "payout", icon: "/assets/admin-desembolso.svg" }];
            break;
          case UserRole.REFERENCIACION:
            this.currentMenu = [{ title: "Referenciación", url: "applications-loans", icon: "/assets/admin-desembolso.svg" }];
            break;

          case UserRole.COMITE:
            this.currentMenu = [{ title: "Comite", url: "applications-loans", icon: "/assets/admin-desembolso.svg" }];
            break;

          case UserRole.ADMIN:
            this.currentMenu = [
              { title: "Solicitudes", url: "applications-loans", icon: "/assets/admin-desembolso.svg" },
              { title: "Créditos", url: "loans", icon: "/assets/admin-desembolso.svg" },
              { title: "Clientes", url: "customers", icon: "/assets/admin-usuarios.svg" },
              { title: "Facturas", url: "invoices", icon: "/assets/admin-factura.svg" },
              { title: "Desembolsos", url: "payout", icon: "/assets/admin-desembolso.svg" },
              { title: "Usuarios", url: "users", icon: "/assets/admin-usuario.svg" },
            ];
            break;
          default:
            break;
        }
      });
  }

  navigate(url: string) {
    this.currentTab = url;
    this.router.navigateByUrl("/admin/" + url);
  }

  logOut(): void {
    this.router.navigateByUrl("/auth/admin");
    this.userSvc.logOut();
  }

  ngOnDestroy(): void {
  //  this.observableAth$.unsubscribe();
  }
}
