import { Component, OnInit, ViewChild, TemplateRef, ElementRef, AfterViewInit } from "@angular/core";
import { DatatableOptions, AuthenticationService } from "ngx-3a";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { LoansService } from "src/app/services/loans.service";
import { MainService } from "src/app/services/main.service";
import { Loan, EstadoRef } from "src/app/models/loans";
import { Estado } from "src/app/models/loans";
import { LoansApplicationsService } from "src/app/services/loan-applications.service";
import { CommentsService } from "src/app/services/comments.service";
import { InvoiceService } from "src/app/services/invoice.service";
import { FilterType } from "src/app/components/modals/apply-filters/apply-filters.component";
import * as moment from "moment";
import { AngularFireAuth } from "@angular/fire/auth";
import { UserRole } from "src/app/models/user";
import { state } from "@angular/animations";
import { fromEvent } from 'rxjs';

@Component({
  selector: "app-loan-applications",
  templateUrl: "./loan-applications.component.html",
  styleUrls: ["./loan-applications.component.scss"],
})
export class LoanApplicationsComponent implements OnInit {
  @ViewChild("createLoan", { static: false }) createLoanTemplate: TemplateRef<any>;
  @ViewChild("money", { static: false }) moneyTemplate: TemplateRef<any>;
  @ViewChild("createComment", { static: false }) createCommentTemplate: TemplateRef<any>;
  @ViewChild("denyLoan", { static: false }) denyLoanTemplate: TemplateRef<any>;
  @ViewChild("actions", { static: false }) actionsTemplate: TemplateRef<any>;
  @ViewChild("returnLoan", { static: false }) returnLoanTemplate: TemplateRef<any>;
  @ViewChild("SendComite", { static: false }) returnComite: TemplateRef<any>;
  @ViewChild("editLoan", { static: false }) editLoanTemplate: TemplateRef<any>;
  @ViewChild("stateLoan", { static: false }) stateLoanTemplate: TemplateRef<any>;
  @ViewChild("stateLoanRef", { static: false }) stateLoanRefTemplate: TemplateRef<any>;
  @ViewChild("date", { static: true }) dateTemplate: TemplateRef<any>;



  filters: FilterType = {
    filterOptions: [],
  };
  options: DatatableOptions;
  applicationsLoansFilters: Array<any> = [];
  applicactionsLoans: Array<any> = [];
  showMessageEmptyData: boolean;
  showLoading = true;
  currentUser:any;
  constructor(
    private route: ActivatedRoute,
    private userSvc: UserService,
    private loansSvc: LoansService,
    private router: Router,
    private mainSvc: MainService,
    private loansApplicationsSvc: LoansApplicationsService,
    private commentsSvc: CommentsService,
    private invoiceSvc: InvoiceService,
    private authSvc: AuthenticationService,
    private afAuth: AngularFireAuth
  ) {}

  ngOnInit() {
    this.loansApplicationsSvc.getApplications().subscribe((data) => {
      this.applicactionsLoans = data;
      this.loadData(data);
    });
  }


  /**
   * Este metodo nos carga la tabla dependiendo del rol de usuario
   * @param data solicitudes en la bd
   * */
  loadData(data) {
    this.applicationsLoansFilters = this.applicactionsLoans;
    this.userSvc.authUserLogged().then((currentUser) => {
      if (currentUser) {
        this.currentUser = currentUser;
        let dummyRows = [];
        let data_final = [];
        switch (currentUser.role) {
          case 0: //Analisis, solo ve los de analisis
            data_final = data.filter((data2) => data2.Estado === 1);

            dummyRows = [
              {
                name: "Nombre",
                prop: "PrimerNombre",
              },
              {
                name: "Apellido",
                prop: "PrimerApellido",
              },
              {
                name: "Cédula",
                prop: "cc",
              },
              {
                name: "Monto solicitado",
                prop: "MontoSolicitado",
                cellTemplate: this.moneyTemplate,
              },
                  {
                name: "Fecha",
                prop: "store.dateCreate",
                cellTemplate: this.dateTemplate,
              },
              {
                name: "Acciones",
                cellTemplate: this.actionsTemplate,
              },
              {
                name: "Estado",
                prop: "Estado",
                cellTemplate: this.stateLoanTemplate,
              },
            ];

            break;
          case 2: // Comercial, solo ve los devueltos y los iniciales
            data_final = data.filter((data2) => data2.Estado === 0 || data2.Estado === 4);

            dummyRows = [
              {
                name: "Nombre",
                prop: "PrimerNombre",
              },
              {
                name: "Apellido",
                prop: "PrimerApellido",
              },
              {
                name: "Cédula",
                prop: "cc",
              },
              {
                name: "Monto solicitado",
                prop: "MontoSolicitado",
                cellTemplate: this.moneyTemplate,
              },
                  {
                name: "Fecha",
                prop: "store.dateCreate",
                cellTemplate: this.dateTemplate,
              },
              {
                name: "Acciones",
                cellTemplate: this.actionsTemplate,
              },
              {
                name: "Estado",
                prop: "Estado",
                cellTemplate: this.stateLoanTemplate,
              },
            ];
            break;
          case 5: // Admin, puede ver a todos
            data_final = data.filter((data2) => data2.Estado >= 0);
            dummyRows = [
              {
                name: "Nombre",
                prop: "PrimerNombre",
              },
              {
                name: "Apellido",
                prop: "PrimerApellido",
              },
              {
                name: "Cédula",
                prop: "cc",
              },
              {
                name: "Monto solicitado",
                prop: "MontoSolicitado",
                cellTemplate: this.moneyTemplate,
              },
                  {
                name: "Fecha",
                prop: "store.dateCreate",
                cellTemplate: this.dateTemplate,
              },
              {
                name: "Acciones",
                cellTemplate: this.actionsTemplate,
              },
              {
                name: "Estado",
                prop: "Estado",
                cellTemplate: this.stateLoanTemplate,
              },
              {
                name: "Referenciación",
                prop: "EstadoRef",
                cellTemplate: this.stateLoanRefTemplate,
              },
            ];
            break;

          case 7: //Comite, solo ve lo que el analista mande a comite
            data_final = data.filter((data2) => data2.Estado === 5);
            dummyRows = [
              {
                name: "Nombre",
                prop: "PrimerNombre",
              },
              {
                name: "Apellido",
                prop: "PrimerApellido",
              },
              {
                name: "Cédula",
                prop: "cc",
              },
              {
                name: "Monto solicitado",
                prop: "MontoSolicitado",
                cellTemplate: this.moneyTemplate,
              },
                  {
                name: "Fecha",
                prop: "store.dateCreate",
                cellTemplate: this.dateTemplate,
              },
              {
                name: "Acciones",
                cellTemplate: this.actionsTemplate,
              },
              {
                name: "Estado",
                prop: "Estado",
                cellTemplate: this.stateLoanTemplate,
              },
            ];
            break;

          case 4: // Referenciacion, puede ver lo que ve el analista y comité que no sean negados
            data_final = data.filter((data2) => data2.Estado === 1 || data2.Estado === 5 || data2.Estado === 2);
            dummyRows = [
              {
                name: "Nombre",
                prop: "PrimerNombre",
              },
              {
                name: "Apellido",
                prop: "PrimerApellido",
              },
              {
                name: "Cédula",
                prop: "cc",
              },
              {
                name: "Monto solicitado",
                prop: "MontoSolicitado",
                cellTemplate: this.moneyTemplate,
              },
                  {
                name: "Fecha",
                prop: "store.dateCreate",
                cellTemplate: this.dateTemplate,
              },
              {
                name: "Acciones",
                cellTemplate: this.actionsTemplate,
              },
              
              {
                name: "Estado",
                prop: "Estado",
                cellTemplate: this.stateLoanTemplate,
              },
              {
                name: "EstadoRef",
                prop: "EstadoRef",
                cellTemplate: this.stateLoanRefTemplate,
              },
            ];
            break;
          
            

          default:
            dummyRows = [
              {
                name: "Nombre",
                prop: "PrimerNombre",
              },
              {
                name: "Apellido",
                prop: "PrimerApellido",
              },
              {
                name: "Cédula",
                prop: "cc",
              },
            ];
            break;
        }
        this.loadTableData(data_final, dummyRows);
      }
    });
  }
  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData(data, columns) {
    data = data.sort((a, b) => moment(a.store.dateCreate.toDate()).valueOf() - moment(b.store.dateCreate.toDate()).valueOf() , 0);
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    this.options = options;
    this.showLoading = false;
    if (data.length > 0) {
      this.showMessageEmptyData = false;
    } else {
      this.showMessageEmptyData = true;
    }
  }

  /**
   * Método para agregar un crédito en la aplicación
   */
  openModal(user, event) {
    event.stopPropagation();
    this.mainSvc.toogleFullModal.next(this.loansSvc.addLoanEvent(user));
  }

  ModalDeny(data, event) {
    event.stopPropagation();
    this.userSvc.authUserLogged().then((currentUser) => {
      if (currentUser) {
        if (currentUser.role === 0 || currentUser.role === 5 || currentUser.role === 7) {
          // Si es analisis, cambia estado a Negado
          this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state: Estado.NEGADO }, () => {}));
        } else if (currentUser.role === 4) {
          // Si es referenciación cambia estado de referenciacion a No Exitosa
          event.stopPropagation();
          this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state1: EstadoRef.No_Exitosa }, () => {}));
        }
      }
    });
  }

  ModalApprove(data, event) {
    event.stopPropagation();
    this.userSvc.authUserLogged().then((currentUser) => {
      if (currentUser) {
        if (currentUser.role === 0 || currentUser.role === 5 || currentUser.role === 7) {
          // Si es analisis, cambia estado a aprobado
          console.log(data);
          this.mainSvc.toogleFullModal.next(this.loansApplicationsSvc.addLoanConditionEvent(data));
          this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state: Estado.APROBADO }, () => {}));
        } else if (currentUser.role === 4) {
          // Si es referenciación cambia estado de referenciacion a Exitosa
          this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state1: EstadoRef.Exitosa }, () => {}));
        }
      }
    });
  }

  ModalReturn(data, event) {
    event.stopPropagation();
    // Para analisis y referenciación
    this.userSvc.authUserLogged().then((currentUser) => {
      if (currentUser) {
        if (currentUser.role === 0 || currentUser.role === 5 || currentUser.role === 7) {
          // Si es analisis, cambia estado a devuelto
          this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state: Estado.DEVUELTO }, () => {}));
        } else if (currentUser.role === 4) {
          // Si es referenciación cambia estado de referenciacion a No contactado
          this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state1: EstadoRef.No_Contactado }, () => {}));
        }
      }
    });
  }

  ModalComite(data, event) {
    event.stopPropagation();
    this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data, state: Estado.COMITE }, () => {}));
  }

  /*
   * Metodo para abrir modal para agregar comentarios
   */
  openModalComments(data, event) {
    event.stopPropagation();
    this.mainSvc.toogleModal.next(this.commentsSvc.showModalComments({ data }, () => {}));
  }
  /**
   * Método para ver la informacion de un agricultor
   * @param user agricultor que desea editar
   */
  navigateToProfileFarmer(user) {
    this.router.navigateByUrl(`/admin/profile-farmer/${user.id}`);
    /* let State = {Estado: Estado.ANALISIS};
    let data2 = Object.assign(user,State);
    this.loansApplicationsSvc.saveUser(data2) */
  }

  /**
   * Este metodo nos muestra en la tabla el estado actual
   * @param state estado de la solicitud
   * @param state1 estado de la solicitud de referenciacion
   */
  showState(state) {
    switch (state) {
      case 0:
        return "INICIAL";
        break;
      case 1:
        return "ANÁLISIS";
        break;
      case 2:
        return "APROBADO";
        break;
      case 3:
        return "NEGADO";
        break;
      case 4:
        return "DEVUELTO";
        break;

      case 5:
        return "A COMITÉ";
        break;

      default:
        break;
    }
  }
  showstateref(state1) {
    switch (state1) {
      case 0:
        return "EN ESPERA DE CONTACTO";
        break;
      case 1:
        return "EXITOSO";
        break;
      case 2:
        return "NO EXITOSO";
        break;
      case 3:
        return "NO CONTACTADO";
        break;
      default:
        break;
    }
  }

  /**
   *Metodo que dispara la accion para que se muestre la modal de los filtros
   */
  displayFilters() {
    this.mainSvc.toogleModal.next(
      this.invoiceSvc.applyFilters(this.filters, (resp) => {
        this.filters = resp;
        this.filterToTable(resp);
      })
    );
  }

  /**
   * Este metodo filtra las facturas con las respectivas condiciones
   * @param filter parametro con filtros
   */
  filterToTable(filter) {
    let filterOld: object;
    if (filter != filterOld) {
      this.applicationsLoansFilters = this.applicactionsLoans;
      if (filter.document) {
        this.applicationsLoansFilters = this.applicationsLoansFilters.filter((applicationLoan) => applicationLoan.cc === filter.document.toString());
      }
      if (filter.minDate) {
        this.applicationsLoansFilters = this.applicationsLoansFilters.filter(
          (applicationLoan) => moment(applicationLoan.date).valueOf() >= moment(filter.minDate).valueOf()
        );
      }

      if (filter.maxDate) {
        this.applicationsLoansFilters = this.applicationsLoansFilters.filter(
          (applicationLoan) => moment(applicationLoan.date).valueOf() <= moment(filter.maxDate).valueOf()
        );
      }

      if (filter.minAmount) {
        this.applicationsLoansFilters = this.applicationsLoansFilters.filter((applicationLoan) => Number(applicationLoan.MontoSolicitado) >= filter.minAmount);
      }
      if (filter.maxAmount) {
        this.applicationsLoansFilters = this.applicationsLoansFilters.filter((applicationLoan) => Number(applicationLoan.MontoSolicitado) <= filter.maxAmount);
      }
    }
    if (this.applicationsLoansFilters) {
      this.loadData(this.applicationsLoansFilters);
    }
    filterOld = filter;
  }
}
