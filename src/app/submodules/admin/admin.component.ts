import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute, RouterOutlet } from '@angular/router';
import { DatatableOptions } from 'ngx-3a';
import { User } from 'src/app/models/user';
import { Observable, pipe } from 'rxjs';
import { animationRoutes } from 'src/app/animations/animation-routes';
import { LoansService } from 'src/app/services/loans.service';
import { take } from 'rxjs/operators';
import { Loan } from 'src/app/models/loans';
import { QuotesService } from 'src/app/services/quotes.service';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  animations: [
    animationRoutes
  ]
})
export class AdminComponent implements OnInit {


  /**
   * El usuario
   */
  user: User = { name: '', PrimerNombre: '', PrimerApellido: '' };

  /**
   * El rol del usuario
   */
  role: string;

  /**
   * Opciones para el Datatable de 3A
   */
  options: DatatableOptions;

  constructor(
    private route: ActivatedRoute,
    private userSvc: UserService,
    private router: Router,
    private loansSvc: LoansService,
    private quotesService: QuotesService,
  ) { }
  ngOnInit() {
    this.generateQuoteCapitalMonth()
  }

  /**
   * Este metodo se inicia cuando carga la app por primera vez para que recorra todos los creditos
   * y aquel que sea creado en la app de promocion y prospeccion se le añada su respectiva cuota a capital
   */
  generateQuoteCapitalMonth() {
    this.loansSvc.getLoans().pipe(take(1)).subscribe((loans: Array<Loan>) => {
      for (let index = 0; index < loans.length; index++) {
        const element = loans[index];
        if (!element.quoteCapitalMonth) {
          let quoteCapital = Number(this.quotesService.quoteCapitalMonth(element).toFixed(2))
          let newLoan = { ...element, quoteCapitalMonth: quoteCapital }
          this.loansSvc.saveLoan(newLoan)
        }
      }
    });
  }

  /** 
   * Animacion para la transicion entre rutas
   * @param outlet Es la referencia del nodo en la vista del outlet
  */

  prepareOutletAnimation(outlet: RouterOutlet): number {
    return outlet.activatedRouteData.position === undefined ? -1 : outlet.activatedRouteData.position
  }


}
