import { AdminComponent } from "./admin.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { UsersAdminComponent } from "./components/users-admin/users-admin.component";
import { PayOutAdminComponent } from "./components/pay-out-admin/pay-out-admin.component";
import { CustomersAdminComponent } from "./components/customers-admin/customers-admin.component";
import { LoansAdminComponent } from "./components/loans-admin/loans-admin.component";
import { InvoicesAdminComponent } from "./components/invoices-admin/invoices-admin.component";
import { LoanProfileAdminComponent } from "src/app/components/generic/loan-profile-admin/loan-profile-admin.component";
import { CreditHistoryComponent } from "src/app/components/generic/credit-history/credit-history.component";
import { ProfileFarmerComponent } from "./components/profile-farmer/profile-farmer.component";
import { StatementsCarteraComponent } from "./components/statements-cartera/statements-cartera.component";
import { LoanApplicationsComponent } from "./components/loan-applications/loan-applications.component";
import { UserRole } from "src/app/models/user";
import { AuthGuard, RoleGuard } from "src/app/services/guards.service";


const routes: Routes = [
  {
    path: "",
    component: AdminComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: "applications-loans",
        component: LoanApplicationsComponent,
        data: { position: 2, roles: [UserRole.ANALISTA, UserRole.COMERCIAL, UserRole.ADMIN, UserRole.REFERENCIACION, UserRole.COMITE, UserRole.DESEMBOLSO] },
        canActivate: [RoleGuard],
      },
      {
        path: "payout",
        component: PayOutAdminComponent,
        data: { position: 6, roles: [UserRole.DESEMBOLSO, UserRole.ADMIN] },
        canActivate: [RoleGuard],
      },
      { path: "loans", component: LoansAdminComponent, data: { position: 3 } },
      { path: "loans/:id/:userId", component: LoanProfileAdminComponent },
      { path: "loans/:id/:userId/creditHistory/:creditId", component: CreditHistoryComponent },
      { path: "profile-farmer/:userId", component: ProfileFarmerComponent },
      { path: "customers", component: CustomersAdminComponent, data: { position: 4 } },
      { path: "invoices", component: InvoicesAdminComponent, data: { position: 5 } },
      // { path: "extracts", component: StatementsCarteraComponent, data: { position: 5 } },

      { path: "users", component: UsersAdminComponent, data: { position: 7, roles: [UserRole.ADMIN] }, canActivate: [RoleGuard] },

      { path: "", redirectTo: "loans" },
      { path: "**", redirectTo: "" },
    ],
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserProfileRoutingModule {}
