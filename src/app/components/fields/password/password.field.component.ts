import { Component } from "@angular/core";
import { InputComponent } from "ngx-3a";

@Component({
  selector: "app-password-field",
  templateUrl: "./password.field.component.html",
  styleUrls: ["./password.field.component.scss"]
})
export class PasswordComponent extends InputComponent {}
