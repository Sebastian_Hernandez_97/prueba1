import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotPage404Component } from './not-page404.component';

describe('NotPage404Component', () => {
  let component: NotPage404Component;
  let fixture: ComponentFixture<NotPage404Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotPage404Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotPage404Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
