import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-page404',
  templateUrl: './not-page404.component.html',
  styleUrls: ['./not-page404.component.scss']
})
export class NotPage404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
