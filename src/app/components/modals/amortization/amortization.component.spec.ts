import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AmortizationModalComponent } from './amortization.component';


describe('AddUserComponent', () => {
  let component: AmortizationModalComponent;
  let fixture: ComponentFixture<AmortizationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AmortizationModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmortizationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
