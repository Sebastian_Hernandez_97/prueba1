import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { ModalBaseComponent } from '../modal.base.component';
import { MainService } from 'src/app/services/main.service';
import { UserService } from 'src/app/services/user.service';
import { DatatableOptions } from 'ngx-3a';
import { Outlay } from 'src/app/models/outlay';
import { Quote } from 'src/app/models/quote';

@Component({
  selector: 'app-amortization',
  templateUrl: './amortization.component.html',
  styleUrls: ['./amortization.component.scss']
})
export class AmortizationModalComponent extends ModalBaseComponent implements OnInit {
  @ViewChild('money', { static: true }) moneyTemplate: TemplateRef<any>;
  @ViewChild('date', { static: true }) dateTemplate: TemplateRef<any>;
  @ViewChild('total', { static: true }) totalTemplate: TemplateRef<any>;

  options: DatatableOptions;
  alert: string;
  payload: { quotes: Quote[] };

  @Input() set _payload(payload: { quotes: Quote[] }) {
    if (payload) {
      this.payload = payload;
      this.loadData();
    }
  }

  constructor(private userSvc: UserService, mainSvc: MainService) {
    super(mainSvc);
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    const options = new DatatableOptions();
    options.columns = [
      {
        name: 'cuota',
        prop: 'index',
        flexGrow: 1
      },
      {
        name: 'Fecha',
        prop: 'date',
        cellTemplate: this.dateTemplate,
        flexGrow: 2
      },
      {
        name: 'Abono capital',
        prop: 'paymentCapital',
        cellTemplate: this.moneyTemplate,
        flexGrow: 1
      }, {
        name: 'Abono intereses',
        prop: 'taxAmount',
        cellTemplate: this.moneyTemplate,
        flexGrow: 1
      },
      {
        name: 'Total',
        prop: 'total',
        cellTemplate: this.moneyTemplate,
        flexGrow: 1
      },
    ];
    options.rows = this.payload.quotes ? this.payload.quotes.map((e, i) => ({ ...e, index: i + 1 })) : [];
    this.options = options;
  }



}
