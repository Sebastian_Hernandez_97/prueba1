import { Component, OnInit, Input, TemplateRef, ViewChild, AfterViewInit } from "@angular/core";
import { MainService } from "src/app/services/main.service";
import { AngularFireStorage } from "@angular/fire/storage";
import { ModalBaseComponent } from "../modal.base.component";
import { UserService } from 'src/app/services/user.service';
import * as moment from 'moment';
import { LoansApplicationsService } from 'src/app/services/loan-applications.service';

interface CommentObj {
  user?:string,
  date?:any,
  text?: string
}

@Component({
  selector: "app-return",
  templateUrl: "./return-election.component.html",
  styleUrls: ["./return-election.component.scss"],
})
export class ReturnElectionModalComponent extends ModalBaseComponent implements OnInit, AfterViewInit {

  @ViewChild("comments", { static: false }) commentsRef: any;

  @Input() set _payload(payload: any) {
    if (payload) {
      this.payload = payload.data;
      if(!payload.data.comments){
        this.payload.comments = [];
      }
      if(payload.state){
       this.state = payload.state
        switch (payload.state) {
          case 0:
            this.textComment = "INICIAL";
            break;
          case 1:
            this.textComment = "ANÁLISIS";
            break;
          case 2:
            this.textComment = "APROBADO";
            break;
          case 3:
            this.textComment = "NEGADO";
            break;
          case 4:
            this.textComment = "DEVUELTO";
            break;
    
          default:
            break;
        }
      }
      if(payload.state1){
        this.state1 = payload.state1
        switch (payload.state1){
          case 1: 
            this.textComment = "Referenciación Exitosa";
            break;
          case 2: 
            this.textComment = "Referenciación No Exitosa";
            break;
          case 3: 
            this.textComment = "No se pudo contactar al cliente";
            break;
          default:
            break;
        }
      }    
      console.log("LA MODAL", payload);
    }
  }

  constructor(mainSvc: MainService,
    private userSvc:UserService,
    private applicationsSvc:LoansApplicationsService
    ) {
    super(mainSvc);
  }

  payload: any;
  commentObj :CommentObj;
  userLogged:any;
  textComment:string = "";
  loading= false;
  showMessageError = false;
  addCommentAuto = false;
  state:number;
  state1:number;

  ngOnInit() {
    this.userSvc.authUserLogged().then(resp => { this.userLogged = resp})
  }

  ngAfterViewInit() {
    if(this.payload.comments.length){
      this.commentsRef.nativeElement.scrollTop = this.commentsRef.nativeElement.scrollHeight -  this.commentsRef.nativeElement.clientHeight;
    }
  }

  addComment(){
    if(this.textComment.length > 0 || this.textComment === undefined){
      this.showMessageError = false;
      this.state ? this.payload.Estado = this.state : this.payload
      this.state1 ? this.payload.EstadoRef = this.state1 : this.payload
      this.commentObj = {
        user: this.userLogged.name,
        date: moment().valueOf(),
        text: this.textComment
      }
      this.payload.comments.push(this.commentObj);
      
      this.applicationsSvc.saveApplication(this.payload);
      this.textComment = "";
      setTimeout(() => {
        this.commentsRef.nativeElement.scrollTop = this.commentsRef.nativeElement.scrollHeight -  this.commentsRef.nativeElement.clientHeight;
      }, 200);
      console.log("final", this.payload);
    }else{
      this.showMessageError = true;
    }
  }

  close() {
    this.confirm();
    this.cancel();
  }
}
