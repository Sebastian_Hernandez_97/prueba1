import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnElectionComponent } from './return-election.component';

describe('CommentsComponent', () => {
  let component: ReturnElectionComponent;
  let fixture: ComponentFixture<ReturnElectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnElectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnElectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});