import { Component, OnInit, Input } from '@angular/core';
import { ModalBaseComponent } from '../modal.base.component';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { MainService } from 'src/app/services/main.service';
import { UserService } from 'src/app/services/user.service';
import { UserStatus, UserRole } from 'src/app/models/user';

export interface UserAdd {
	name?: DataAction;
	email?: DataAction;
	role?: number;
	status?: UserStatus;
}

export interface DataAction {
	value?: string;
	error?: boolean;
}

@Component({
	selector: 'app-add-user',
	templateUrl: './add-user.component.html',
	styleUrls: ['./add-user.component.scss'],
})
export class AddUserModalComponent extends ModalBaseComponent implements OnInit {
	fields: FormlyFieldConfig[];
	alert: string;
	userRole = [
		{title:"Analista", value: 0},
		{title:"Comercial", value: 2},
		{title:"Desembolso", value: 3},
		{title:"Referenciación", value: 4},
		{title:"Tienda", value: 6}

	];
	users: UserAdd[] = [{ email: {}, name: {}, role: 0, status: 0 }];
	payload: {};

	@Input() set _payload(payload: {}) {
		if (payload) {
			this.payload = payload;
		}
	}

	constructor(private userSvc: UserService, mainSvc: MainService) {
		super(mainSvc);
	}

	ngOnInit(): void {}
	/**
	 * Método que agrega un nuevo usuario a la lista de agregación
	 * @param index  indice  usuario
	 * @param add si se agrega un nuevo usuario a la lista
	 */
	userAction(index: number, add: boolean) {
		if (add) {
			this.users.push({ email: {}, name: {}, role: 0, status: 0 });
		} else {
			this.users.splice(index, 1);
		}
	}

	/**
	 * Método que valida eel texto ingresado en el campo de nombres
	 * @param nombre nombre de un usuario
	 * @param index indice del usuario
	 */
	nameChanged(name: string, index: number) {
		if (name !== '' && name !== ' ') {
			this.users[index].name.error = false;
		}
	}
	/**
	 * Método que valida el texto ingresado en el campo de emails
	 * @param emailData email de un usuario
	 * @param index indice del usuario
	 */
	emailChanged(emailData: string, index: number) {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailData)) {
			this.users[index].email.error = false;
		} else {
			this.users[index].email.error = true;
		}
	}

	/**
	 * Método que valida que la infromacion sea correcta para que sea creeado el usuario
	 */
	manageData() {
		let flag = true;
		this.users.forEach(element => {
			if (element) {
				if (element.email.error || element.name.error) {
					flag = false;
				}

				if (element.email.error === undefined || element.name.error === undefined) {
					flag = false;
				}
			}
		});

		if (flag) {
			this.confirm(this.users.map(e => ({ name: e.name.value, email: e.email.value, role: Number(e.role), status: e.status })));
		} else {
			this.users = this.users.map(e => ({
				name: {
					value: e.name.value,
					error: e.name.error !== undefined ? e.name.error : true,
				},
				email: {
					value: e.email.value,
					error: e.email.error !== undefined ? e.email.error : true,
				},
				role: e.role,
			}));
		}
	}
}
