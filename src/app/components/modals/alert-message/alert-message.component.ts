import { Component, OnInit, Input } from '@angular/core';
import { ModalBaseComponent } from '../modal.base.component';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss']
})
export class AlertMessageComponent extends ModalBaseComponent implements  OnInit {

  constructor(
    mainSvc: MainService
  ) {
    super(mainSvc);
  }
  payload:any;
  @Input() set _payload(payload: {}) {
		if (Object.keys(payload).length) {
      this.payload = payload;
		}
	}

  ngOnInit() {

  }

  close(){
    this.cancel()
  }

  confirmAction(){
    this.payload.action = true;
    this.confirm(this.payload)
  }
}
