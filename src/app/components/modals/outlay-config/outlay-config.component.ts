import { Component, OnInit, Input } from '@angular/core';
import { ModalBaseComponent } from '../modal.base.component';
import { MainService } from 'src/app/services/main.service';
import { Loan } from 'src/app/models/loans';
import { filter } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-outlay-config',
  templateUrl: './outlay-config.component.html',
  styleUrls: ['./outlay-config.component.scss']
})
export class ModalOutlayConfigComponent extends ModalBaseComponent implements OnInit {

  payload: { loan: Loan };

  currentOutlay: any;
  @Input() set _payload(payload: { loan: Loan }) {
    if (payload) {
      this.payload = payload;
      this.setCurrent();
    }
  }

  constructor(mainSvc: MainService) {
    super(mainSvc);
  }

  ngOnInit() {

  }

  setCurrent() {
    const array = this.payload.loan.outlay.map((v, i) => ({ ...v, index: i })).filter((value) => {
      const d = moment((value.date as any).toDate());
      return d.isAfter(moment().subtract(1, 'd'));
    });
    if (array.length > 0) {
      this.currentOutlay = array[0];
    }


  }

}
