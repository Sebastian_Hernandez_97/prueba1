import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalOutlayConfigComponent } from './outlay-config.component';

describe('OutlayConfigComponent', () => {
  let component: ModalOutlayConfigComponent;
  let fixture: ComponentFixture<ModalOutlayConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalOutlayConfigComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalOutlayConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
