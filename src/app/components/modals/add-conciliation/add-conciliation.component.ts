import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { ModalBaseComponent } from '../modal.base.component';
import { MainService } from 'src/app/services/main.service';
import { DashboardDatatableComponent, DatatableOptions } from 'ngx-3a';
import { ConciliationService } from 'src/app/services/conciliation.service';
import * as moment from 'moment';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { debounceTime, pluck, take } from 'rxjs/operators';
import { Database } from '3a-common';
import { LoansService } from 'src/app/services/loans.service';
import { UserService } from 'src/app/services/user.service';
import { Loan } from 'src/app/models/loans';
import { QuotesService } from 'src/app/services/quotes.service';

export interface CSVRecord {
  amount: any;
  date: any;
  cc: any;
}

@Component({
  selector: 'app-add-conciliation',
  templateUrl: './add-conciliation.component.html',
  styleUrls: ['./add-conciliation.component.scss']
})
export class AddConciliationModalComponent extends ModalBaseComponent implements OnInit {

  @ViewChild('datatable1', { static: false }) datatable1: DashboardDatatableComponent;
  @ViewChild('datatable2', { static: false }) datatable2: DashboardDatatableComponent;


  @ViewChild('openManualConciliationButton', { static: true })
  openManualConciliationButton: TemplateRef<any>;

  @ViewChild('generateManualConciliationButton', { static: true })
  generateManualConciliationButton: TemplateRef<any>;


  @ViewChild('money', { static: true })
  moneyTemplate: TemplateRef<any>;

  @ViewChild('date', { static: true })
  dateTemplate: TemplateRef<any>;



  payload: {};
  invoices = [];
  invoicesToConciliationManual = [];
  quotesAllCreditsForUser = [];
  conciliations = [];
  noConciliations = [];
  loading = false;
  records: any[] = [];
  currentStep = 0;
  optionsTable1: DatatableOptions;
  optionsTable2: DatatableOptions;
  countConciliation: number = 0;
  searchFormGroup: FormGroup;
  recepitSelectToConciliationManual: Loan;
  errorMessage: string;
  dataToConfirmConcilitation: any;

  @Input() set _payload(payload: {}) {
    if (payload) {
      this.payload = payload;
    }
  }

  constructor(
    mainSvc: MainService,
    private conciliationsSvc: ConciliationService,
    private db: Database.DatabaseService,
    private loansSvc: LoansService,
    private userSvc: UserService,
    private formBuilder: FormBuilder,
    private quoteSvc: QuotesService
  ) {
    super(mainSvc);
  }

  ngOnInit(): void {
    this.invoices = this.payload['invoices'];
    this.serachFarmer()
  }

  /**
   * Metodo para buscar las facturas de un usuario ingresando la cedula
   */
  serachFarmer() {
    this.searchFormGroup = this.formBuilder.group({
      cc: ['', RxwebValidators.digit()],
    });

    this.searchFormGroup.controls.cc.valueChanges.pipe(debounceTime(500)).subscribe(resp => {
      this.invoicesToConciliationManual = [];
      this.quotesAllCreditsForUser = []
      this.loadData2()
      this.userSvc.getUserByDocument(resp).pipe(take(1)).subscribe((user: any) => {
        if (user) {
          this.errorMessage = '';
          this.loansSvc.getUserLoans(user.id).subscribe(loans => {
            loans.forEach((loan: any) => {
              this.quoteSvc.getLoanQuotes(loan.id).pipe(take(1)).subscribe(quotes => {
                quotes.forEach(quote => this.quotesAllCreditsForUser.push(quote))
                this.mapQuotesASingleArray(this.quotesAllCreditsForUser, loan)
              })
            })
          })
        } else {
          this.errorMessage = "Usuario no encontrado, por favor intentelo de nuevo con otra cedula";
        }
      })
    })
  }

  /** 
   * Metodo que filtra las cuotas y saca solo las que ya han sido facturadas
   * @param quotes parametro que trae las coutas por cada credito
   * @param loan parametro que trae la informacion del credito
   */
  mapQuotesASingleArray(quotes, loan) {
    for (let index = 0; index < quotes.length; index++) {
      const element = quotes[index];
      if (element.invoice) {
        this.invoicesToConciliationManual.push({ quote: element, ...loan })
      }
    }
    if (this.invoicesToConciliationManual.length) {
      this.loadData2()
    }
  }

  /**
   * Este metodo verifica que solo venga un archivo y sea de tipo csv
   * @param file parametro que recibe el archivo csv que ingresa el analista
   */
  fileConciliation(file) {
   // console.log("En la modal", file);
    if (!file) {
     // console.log("Solo se admite un archivo")
    } else {
      if (file.type === "text/csv") {
        this.generateConciliation(file)
      } else {
       // console.log("El archivo seleccionado no es admitido")
      }
    }
  }

  /**
   * Este metodo procesa la informacion del archivo que ingreso el analista
   * @param file recibe el archivo ya verificado
   */
  generateConciliation(file) {
    let text = [];
    let reader = new FileReader();
    reader.readAsText(file);

    reader.onload = () => {
      let csvData = reader.result;
      let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);
      let headersRow = this.getHeaderArray(csvRecordsArray);
      this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
      if (this.records) {
        this.loading = true
        this.proccesConciliation(this.records)
      }
    };
    reader.onerror = function () {
      console.log('error is occured while reading file!');
    };
  }

  /**
   * Este metodo convierte en array la informacion que trae el archivo csv
   * @param csvRecordsArray parmametro que tiene el contenido del cuerpo del archivo
   * @param headerLength parametro que tiene la cabecera del archivo
   */
  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let csvArr = [];
    for (let i = 1; i < csvRecordsArray.length; i++) {
      let curruntRecord = (<string>csvRecordsArray[i]).split(',');
      if (curruntRecord.length == headerLength) {
        let csvRecord: CSVRecord = {
          amount: curruntRecord[0].trim(),
          date: curruntRecord[1].trim(),
          cc: curruntRecord[2].trim(),
        }
        csvArr.push(csvRecord);
      }
    }
    return csvArr;
  }

  /**
   * Este metodo convierte la cabecera del archivo en un array
   * @param csvRecordsArr parametro que recibe la cabecera del archivo
   */
  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  /**
   * Este metodo compara las facturas con el archivo de conciliacion, las que concuerdad las concilia automaticamente
   * las que no las deja en un array diferente para hacer el proceso manual.
   * @param records parametro que trae el archivo csv ya procesado
   */
  proccesConciliation(records) {
    let invoices = this.invoices

    records.forEach(record => {
      invoices.forEach((invoice, index) => {
        if (!invoice.quote.invoice.receipt &&
          record.cc === invoice.farmer.cc &&
          record.amount === invoice.quote.invoice.total.toFixed(2).toString()) {

          let receiptTemp = {
            transaction: Date.now().toString(),
            date: moment(this.formatDateConciliation(record.date)).valueOf(),
            amount: record.amount,
            farmerCC: record.cc,
            concept: ""
          }
          let invoiceTemp = { ...invoice.quote.invoice, receipt: receiptTemp };
          let quoteTemp = { ...invoice.quote, invoice: invoiceTemp }
          this.countConciliation++
          this.conciliationsSvc.addReceiptOfConciliationInTheInvoice(invoice, quoteTemp);
          // invoices.splice(index, 1)
        } else if (!invoice.quote.invoice.receipt &&
          record.cc === invoice.farmer.cc &&
          record.amount != invoice.quote.invoice.total.toFixed(2).toString()) {
          this.noConciliations.push({
            transaction: Date.now().toString(),
            date: this.formatDateConciliation(record.date),
            amount: record.amount,
            farmerCC: record.cc,
            concept: ""
          })
          // invoices.splice(index, 1)
        }
      });
    });

    this.loading = false
    this.currentStep = 1
    this.loadData1()
  }

  /**
   * Este metodo le da un formato a la fecha que viene desde al archivo en desorden
   * @param date parametro que trae la fecha
   */
  formatDateConciliation(date) {
    let arrayDate = date.split('')
    return `${arrayDate[4]}${arrayDate[5]}/${arrayDate[6]}${arrayDate[7]}/${arrayDate[0]}${arrayDate[1]}${arrayDate[2]}${arrayDate[3]}`
  }

  /**
   * Metodo que asigna una transaccion a una variable y me permite avanzar al siguiente paso
   * @param row parametro que trae la transaccion seleccionada
   */
  openManualConciliation(row) {
    this.currentStep = 2
    this.recepitSelectToConciliationManual = row
  }

  /**
  * Este metodo pasa al siguiente paso para confirmar la conciliacion
  * @param row parametro que contiene la informacion completa para realizar la conciliacion manual
  */
  confirmConciliation(row) {
    this.currentStep = 3
    this.dataToConfirmConcilitation = row
  }

  /**
   * Este metodo agrega un objeto a la informacion el cual trae el recibo que se genera para la conciliacion
   */
  generateManualConciliation() {
    let loan = this.dataToConfirmConcilitation;
    let invoiceTemp = { ...loan.quote.invoice, receipt: this.recepitSelectToConciliationManual };
    let quoteTemp = { ...loan.quote, invoice: invoiceTemp }
    this.countConciliation++
    this.conciliationsSvc.addReceiptOfConciliationInTheInvoice(loan, quoteTemp);
    this.cancel()
  }

  /**
   * Carga los datos que se nececitaran para mostrar en la tabla
   */
  loadData1() {
    const rows = [
      {
        name: 'Transacción',
        prop: 'transaction'
      },
      {
        name: 'Fecha',
        prop: 'date'
      },
      {
        name: 'Monto',
        prop: 'amount',
        cellTemplate: this.moneyTemplate,
      },
      {
        name: 'Concepto',
        prop: 'concept',
      },
      {
        name: 'Realizar conciliación',
        prop: 'amount',
        flexGrow: 3,
        cellTemplate: this.openManualConciliationButton
      },
    ];
    this.loadTableData1(this.noConciliations, rows);
  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData1(data, columns) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    this.optionsTable1 = options;
  }

  /**
   * Carga los datos que se nececitaran para mostrar en la tabla
   */
  loadData2() {
    const rows = [
      {
        name: 'Nombre',
        prop: 'farmer.PrimerNombre'
      },
      {
        name: 'Cédula',
        prop: 'farmer.cc'
      },
      {
        name: 'Fecha',
        prop: 'quote.date',
        cellTemplate: this.dateTemplate
      },
      {
        name: 'Préstamo',
        prop: 'quote.invoice.total',
        cellTemplate: this.moneyTemplate,
      },

      {
        name: 'Realizar conciliación',
        prop: 'amount',
        flexGrow: 3,
        cellTemplate: this.generateManualConciliationButton
      },
    ];
    this.loadTableData2(this.invoicesToConciliationManual, rows);
  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData2(data, columns) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    this.optionsTable2 = options;
  }

  back() {
    if (this.currentStep === 0) {
      this.cancel()
    } else {
      this.currentStep--
    }
  }

}
