import { Component, OnInit, Input } from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { ModalBaseComponent } from '../modal.base.component';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-add-file-attachment',
  templateUrl: './add-file-attachment.component.html',
  styleUrls: ['./add-file-attachment.component.scss']
})
export class AddFileAttachmentComponent extends ModalBaseComponent  implements OnInit {

  constructor(
    mainSvc: MainService,
    private storage: AngularFireStorage,
  ) {
    super(mainSvc);
  }
  payload:any;
  @Input() set _payload(payload: any) {
		if (payload) {
      this.payload = payload;
      if(this.payload.fileName){
        this.nameFile =  this.payload.fileName;
        this.inputDisabled = true;
      }
    }
	}
  nameFile:string ="";
  inputDisabled:boolean = false;
  url:string;
  file:any;
  imgMessage:string;
  showErrorInput:boolean = false;
  showErrorImg:boolean = false;
  filePathToDelete: string;
  loading: boolean =false;
  ngOnInit() {

  }

  close(){
    if(this.url){
      const reftoDelete = this.storage.ref(this.filePathToDelete);
      reftoDelete.delete().subscribe()
    }
    this.cancel()
  }

  /**
   * este metodo verifica si el el archivo tiene la extencion requerida 
   * si no la tiene muestra el mensaje de error
   * @param file archivo para guardar
   */
  uploadFileAttachment(file){
    if (file.name.match(/.(jpeg|jpg|png|pdf|xlsx|docx|doc)/g)) {
      this.file = file
     this.showErrorImg = false;
    } else {
     this.showErrorImg = true;
    }
  }

  /**
   * Este metodo crea la referencia y guarda en el storage el archivo
   * @param file archivo para guardar
   */
  uploadFileAttachmentFb(file){
     const filePath = `Prospeccion/${this.payload.user}/${file.name}`;
     this.filePathToDelete = filePath;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.loading = true;
    task.snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            this.url = url;
            this.payload.url = this.url
            if(this.url){
              this.savedFile()
              this.loading = false;
            }
          });
        })
      )
      .subscribe();  
  }

  /**
   * Este metodo confirma los datos para guardar y llama el metodo uploadFileAttachmentFb
   */
  confirmAction(){
    if(this.nameFile.length === 0 || !this.file){
      this.nameFile.length === 0 ? this.showErrorInput = true : this.showErrorInput = false;
      !this.file ? this.showErrorImg = true : this.showErrorImg = false;
    }else{
      this.showErrorInput = false;
      this.payload.nameFile = this.nameFile
      this.uploadFileAttachmentFb(this.file)
    }
  }

  /**
   * Este metodo cierra la modal y confirmar el guardado, enviando un callback al componente 
   * de donde se genero la modal
   */
  savedFile(){
   this.confirm(this.payload)
  }
}
