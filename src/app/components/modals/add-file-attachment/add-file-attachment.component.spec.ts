import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFileAttachmentComponent } from './add-file-attachment.component';

describe('AddFileAttachmentComponent', () => {
  let component: AddFileAttachmentComponent;
  let fixture: ComponentFixture<AddFileAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFileAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFileAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
