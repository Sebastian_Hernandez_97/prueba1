import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { ModalBaseComponent } from '../modal.base.component';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { LoansService } from 'src/app/services/loans.service';
import { Loan, Product, AnalistLoanModel } from 'src/app/models/loans';
import { BasicFormComponent } from 'ngx-3a';
import { Outlay } from 'src/app/models/outlay';
import { Quote } from 'src/app/models/quote';
import * as moment from 'moment-timezone';
import { UserRole, User, UserModel } from 'src/app/models/user';
import { QuotesService } from 'src/app/services/quotes.service';
import { UserService } from 'src/app/services/user.service';
import { RxFormGroup, RxFormBuilder, FormBuilderConfiguration } from '@rxweb/reactive-form-validators';
import { FormGroup, FormControl, AsyncValidatorFn } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Database } from '3a-common';
import { BankInformationModel } from 'src/app/models/bank';
import { AppConstantsService } from 'src/app/services/app-constants.service';
import { map } from 'rxjs/operators';
import { CurrencyPipe } from '@angular/common';




@Component({
  selector: 'app-add-loan-conditions',
  templateUrl: './add-loan-conditions.component.html',
  styleUrls: ['./add-loan-conditions.component.scss']
})
export class AddLoanConditionsModalComponent extends ModalBaseComponent implements OnInit {

  loanFormGroup: RxFormGroup;
  fillData = true;
  error = false;
  currentStep = 0;
  payload: any;
  loan = new AnalistLoanModel()
  newOutlay: Outlay = { delivered: false };
  outlays: Outlay[] = [];
  quotes: Quote[] = [];
  quotesSaveDB: Quote[] = []
  outlayed = 0;
  lastProduct: Product;
  lastPaymentPeriodicity: number;
  additionalService = {
    concept: 'ensurance',
    value: 0,
    periodicity: 0
  };
  analist: User
  departaments = [];
  cities = [];
  banks = [];
  products = [];
  paymentPeriodicity = [];
  typesInterest = [];
  paymentMethods = [];
  typeQuotes = [];
  capitalSubscriptions = [];
  agreements = [];
  customMonths = [];
  customMonthsPaySelected = [];
  othersServices = [];
  othersServicesSelected = [];


  @Input() set _payload(payload:any) {
    console.log(payload)
    this.payload = payload.data;
    /*if (payload.user) {
      this.payload = payload.user;
    }*/
  }
  
  constructor(
    mainSvc: MainService,
    private loansSrv: LoansService,
    private quotesService: QuotesService,
    private userSvc: UserService,
    private formBuilder: RxFormBuilder,
    private db: Database.DatabaseService,
    private constants: AppConstantsService
  ) {
    super(mainSvc);
  }

  ngOnInit() {
    this.initFormBuilder()
    this.userSvc.getCurrentUser().subscribe(user => {
      this.analist = user
    })
  }

  initFormBuilder() {
    this.departaments = this.loansSrv.DEPARTMENTS
    let user = new UserModel()
    //let bank = new BankInformationModel()
    this.loan.farmer = user
    console.log(this.payload)
    this.loanFormGroup = <RxFormGroup>this.formBuilder.formGroup(this.loan)
    this.loanFormGroup.valueChanges.subscribe(res => {
      console.log(res);
      console.log( this.loanFormGroup);
    })
   
    this.loanFormGroup.controls.farmer['controls'].PrimerNombre.setValue(this.payload.PrimerNombre)
    this.loanFormGroup.controls.farmer['controls'].PrimerApellido.setValue(this.payload.PrimerApellido)
    this.loanFormGroup.controls.farmer['controls'].SegundoNombre.setValue(this.payload.SegundoNombre ? this.payload.SegundoNombre: "" )
    this.loanFormGroup.controls.farmer['controls'].SegundoApellido.setValue(this.payload.SegundoApellido ? this.payload.SegundoApellido: "")
    this.loanFormGroup.controls.farmer['controls'].cc.setValue(this.payload.cc)
    this.loanFormGroup.controls.farmer['controls'].cellphone.setValue(this.payload.cellphone)
    this.loanFormGroup.controls.farmer['controls'].DireccionRes.setValue(this.payload.DireccionRes ? this.payload.DireccionRes: "")
    this.loanFormGroup.controls.amount.setValue(this.payload.MontoSolicitado)
    //this.loanFormGroup.controls.farmer['controls'].DepartamentoRes.setValue(this.payload.DepartamentoRes ? this.payload.DepartamentoRes: "")
    //this.loanFormGroup.controls.farmer['controls'].Vereda.setValue(this.payload.Vereda ? this.payload.Vereda: "")
    
    this.loanFormGroup.controls.farmer['controls'].role.setValue(UserRole.FARMER)
    this.loanFormGroup.controls.farmer['controls'].name.setValue('')
    //this.buildArrayBanks()
    //this.setPersonalDataVals()
    this.buildArrayPaymentPeriodicity()
    this.buildArrayProducts()
    this.buildArrayTypeInterest()
    this.generateInterestRate()
    this.buildArrayPaymentMethod()
    this.buildArrayTypeQuote()
    this.buildArrayCapitalSubscriptions()
    this.buildArrayAgreements()
    this.culculateMonthsPaymentToCapital()
    this.buildArrayOthersServices()
  }

  /**
   * Metodo que asigna los valores de agricultor si el credito se va generar desde una solicitud
   */
  setPersonalDataVals(){
    
    const form = this.loanFormGroup.controls.farmer['controls'];
    form.PrimerNombre.setValue(this.payload.PrimerNombre)
    form.PrimerApellido.setValue(this.payload.PrimerApellido)
    form.SegundoNombre.setValue(this.payload.SegundoNombre ? this.payload.SegundoNombre: "" )
    form.SegundoApellido.setValue(this.payload.SegundoApellido ? this.payload.SegundoApellido: "")
    form.cc.setValue(this.payload.cc)
    form.cellphone.setValue(this.payload.cellphone)
    form.DireccionRes.setValue(this.payload.DireccionRes ? this.payload.DireccionRes: "")
    form.DepartamentoRes.setValue(this.payload.DepartamentoRes ? this.payload.DepartamentoRes: "")
    form.Vereda.setValue(this.payload.Vereda ? this.payload.Vereda: "")
    
  }
  setValuesUser(){
    if(this.payload){
      const form = this.loanFormGroup.controls.farmer['controls'];
      form.PrimerNombre.setValue(this.payload.PrimerNombre)
      form.PrimerApellido.setValue(this.payload.PrimerApellido)
      form.SegundoNombre.setValue(this.payload.SegundoNombre ? this.payload.SegundoNombre: "" )
      form.SegundoApellido.setValue(this.payload.SegundoApellido ? this.payload.SegundoApellido: "")
      form.cc.setValue(this.payload.cc)
      form.cellphone.setValue(this.payload.cellphone)
      form.DireccionRes.setValue(this.payload.DireccionRes ? this.payload.DireccionRes: "")
      form.DepartamentoRes.setValue(this.payload.DepartamentoRes ? this.payload.DepartamentoRes: "")
      form.Vereda.setValue(this.payload.Vereda ? this.payload.Vereda: "")
      this.loanFormGroup.controls.amount.setValue(this.payload.MontoSolicitado)
    }
  }
  
  generateInterestRate() {
    this.loanFormGroup.controls.product.valueChanges.subscribe(res => {
      this.loanFormGroup.controls.interestRate.setValue(Number(res.interestRate))
    });
  }

  buildArrayBanks() {
    this.db.find<any>([], this.constants.banksCollection()).subscribe(res => {
      this.banks = res
    })
  }

  buildArrayProducts() {
    this.db.find<any>([], this.constants.loansLinesCollection()).subscribe(res => {
      this.products = res
    })
  }

  buildArrayPaymentPeriodicity() {
    this.db.find<any>(
      [
        {
          key: "id",
          relation: Database.DatabaseQueryRelation.Equal,
          value: 'paymentPeriodicity'
        }
      ], this.constants.metadata()).subscribe((res: any) => {


        this.paymentPeriodicity = res[0].options
      })
  }

  buildArrayTypeInterest() {
    this.db.find<any>([
      {
        key: "id",
        relation: Database.DatabaseQueryRelation.Equal,
        value: 'typeInterestRate'
      }
    ], this.constants.metadata()).subscribe(res => {
      this.typesInterest = res[0].options
    })

  }

  buildArrayPaymentMethod() {
    this.db.find<any>([
      {
        key: "id",
        relation: Database.DatabaseQueryRelation.Equal,
        value: 'paymentMethod'
      }
    ], this.constants.metadata()).subscribe(res => {
      this.paymentMethods = res[0].options
    })
  }

  buildArrayTypeQuote() {
    this.db.find<any>([
      {
        key: "id",
        relation: Database.DatabaseQueryRelation.Equal,
        value: 'typeQuote'
      }
    ], this.constants.metadata()).subscribe(res => {
      this.typeQuotes = res[0].options
    })
  }

  buildArrayCapitalSubscriptions() {
    this.db.find<any>([
      {
        key: "id",
        relation: Database.DatabaseQueryRelation.Equal,
        value: 'capitalSubscription'
      }
    ], this.constants.metadata()).subscribe(res => {
      this.capitalSubscriptions = res[0].options
    })
  }

  buildArrayAgreements() {
    this.db.find<any>([], this.constants.agreementCollection()).subscribe(res => {
      this.agreements = res
    })
  }

  buildArrayOthersServices() {
    this.db.find<any>([], this.constants.othersCollection()).subscribe(res => {
      this.othersServices = res
    })
  }

  culculateMonthsPaymentToCapital() {
    let currentDate = moment().add(1, 'months')
    let months = []
    this.loanFormGroup.controls.paymentPeriodicity.valueChanges.subscribe(res => {
      for (let index = 0; index < 12; index++) {
        let quoteTemp = {
          value: index,
          label: `${currentDate.locale('es').format("MMMM")}`,
        }
        months.push(quoteTemp)
        currentDate.add(1, 'months')
      }
      this.customMonths = months
    })

  }

  monthsSelected(option) {
    const i = this.customMonthsPaySelected.findIndex(el => el === option.value)
    if (i >= 0) {
      this.customMonthsPaySelected.splice(i, 1)
    } else {
      this.customMonthsPaySelected.push(option.value)
    }
  }

  othersSelected(option) {
    const i = this.othersServicesSelected.findIndex(el => el.id === option.id)
    if (i >= 0) {
      this.othersServicesSelected.splice(i, 1)
    } else {
      this.othersServicesSelected.push(option)
    }
  }



  /**
   * método para navegar en la agregación
   */
  nextStep() {
    switch (this.currentStep) {
      case 0:
        this.currentStep++;
        this.fillData = false;
        this.quotes = []
        this.loan.monthsPay = this.customMonthsPaySelected;
        this.loan.otherServices = this.othersServicesSelected;
        this.calculateQuotes();
        console.log(this.loan)
        break;
      default:
        break;
    }
  }
  /**
   * Método para navegar hacia atras en la agregación
   */
  back() {
    switch (this.currentStep) {
      case 1:
        this.currentStep--;
        this.fillData = true;
        this.quotes = []
        this.customMonthsPaySelected = []
        this.othersServicesSelected = []
        break;
      default:
        break;
    }
  }

  /**
   * Método para validar los formularios antes de avanzar
   */
  submit() {
    switch (this.currentStep) {
      case 0:
        this.nextStep()
        break;
      case 1:
        
        this.confirm({ loan: this.setAtrbutesInLoan() });
        break;
      default:
        break;
    }
  }
  /* Este metodo seta atributos al objeto del credito que se crea con el formulario */
  setAtrbutesInLoan(): any {
   /*  let billingCycle: number
    let currentDate = moment().tz('America/Bogota').valueOf();
    let currentMonthDay20 = moment(currentDate).startOf('month').add(19, 'days').valueOf();
    let currentMonthDay5 = moment(currentDate).startOf('month').add(4, 'days').valueOf()
    if (currentDate > currentMonthDay5 && currentDate < currentMonthDay20) {
      billingCycle = 1
    } else {
      billingCycle = 0
    } */
    //let farmer = {...}
    
    let temp = {
      //quoteCapitalMonth: Number(this.quotesService.quoteCapitalMonth().toFixed(2)),
      // balance: Number(this.loan.amount),
      //agreement: this.loan.agreement,
      //amount: this.loan.amount,
      ccAnalyst: this.analist.cc,
      agreement: this.loan.agreement,
      monthsPay: this.loan.monthsPay,
      othersServices: this.loan.otherServices,
      product: this.loan.product,
      amount: this.loan.amount,
      capitalSubscription: this.loan.capitalSubscription,
      interestRate: this.loan.interestRate,
      paymentMethod: this.loan.paymentMethod,
      typeInterestRate: this.loan.typeInterestRate,
      typeQuote: this.loan.typeQuote
      }
      
      //billingCycle: Number(billingCycle),
      //farmer: this.payload,
      //ccAnalyst: this.analist.cc,
      //bankInformation: JSON.parse(JSON.stringify(this.loan.bankInformation))
    

    let loan = { ...this.payload, ...temp }
        
    return loan
  }

  /**
   * Método para calcular las cuotas de un credito
   */
  calculateQuotes() {
    this.quotes = this.quotesService.calculateQuotes(this.loan)
  }


  prueba($evet) {

  }
}
