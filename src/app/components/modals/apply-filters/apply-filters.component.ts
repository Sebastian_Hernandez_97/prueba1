import { Component, OnInit, Input } from '@angular/core';
import { ModalBaseComponent } from '../modal.base.component';
import { MainService } from 'src/app/services/main.service';
import { Options, LabelType } from 'ng5-slider';

export interface FilterType {
  document?: string;
  value?: string;
  custom?: string;
  minAmount?: number;
  maxAmount?: number;
  minDate?: string;
  maxDate?: string;
  filterOptions?:Array<object>;
}

@Component({
  selector: 'app-apply-filters',
  templateUrl: './apply-filters.component.html',
  styleUrls: ['./apply-filters.component.scss']
})
export class ApplyFiltersComponent extends ModalBaseComponent implements OnInit {

  payload:FilterType;
  minValue: number = 500;
  maxValue: number = 100000000;
  options: Options = {
    floor: 500,
    ceil: 100000000,
    step: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        case LabelType.High:
          return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        default:
          return '$' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
      }
    }
  };
  filterOptions:Array<object>;

	@Input() set _payload(payload: FilterType) {
		if (Object.keys(payload).length) {
      this.payload = payload;
      this.minValue = payload.minAmount ? payload.minAmount : this.minValue;
      this.maxValue = payload.maxAmount ? payload.maxAmount : this.maxValue;;
		}
	}

  constructor(
    mainSvc: MainService
  ) {
    super(mainSvc);
  }

  ngOnInit(): void {
    
  }

  /**
   * Metodo para enviar las condiciones de los filtros al componenete donde fue invocada la modal
   */
  search(): void {
    this.payload.maxAmount = this.maxValue;
    this.payload.minAmount = this.minValue;   
    this.confirm(this.payload)
  }

  /**
   * Metodo para limpiar las condiciones de los filtros a como eran orginalmente
   */
  resetFilter(){
    this.maxValue = this.options.ceil;
    this.minValue = this.options.step;  
    this.payload = {
      filterOptions: this.payload.filterOptions,
    }
  }
}
