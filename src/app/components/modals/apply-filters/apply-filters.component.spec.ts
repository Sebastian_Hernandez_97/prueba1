import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyFiltersComponent } from './apply-filters.component';

describe('ApplyFiltersComponent', () => {
  let component: ApplyFiltersComponent;
  let fixture: ComponentFixture<ApplyFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
