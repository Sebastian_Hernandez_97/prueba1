import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DatatableOptions } from 'ngx-3a';
import * as moment from 'moment';
import { User, UserRole } from 'src/app/models/user';
import { Observable } from 'rxjs';
import { LoansService } from 'src/app/services/loans.service';
import { take } from 'rxjs/operators';
import { QuotesService } from 'src/app/services/quotes.service';
import { InvoicesPdfService } from 'src/app/services/invoces-pdf.service';
@Component({
  selector: 'app-credit-history',
  templateUrl: './credit-history.component.html',
  styleUrls: ['./credit-history.component.scss']
})
export class CreditHistoryComponent implements OnInit {

  @ViewChild('fileDownload', { static: false }) fileDownloadTemplate: TemplateRef<any>;

  user: User = { name: '', PrimerNombre: '', PrimerApellido: '' };
  role: UserRole;
  options: DatatableOptions;

  /**
   * Se guardan todos los eventos para que puedan ser filtrados y ordenados facilmente
   */
  events: [{
    amount?: any,
    date: any,
    type: string,
    obj: {},
    id?: string,
  }
  ] = [undefined];

  loans: any = [{ amount: 0 }];
  actualLoan = 0;
  quotes: any;
  userId;
  loanId;

  constructor(
    private route: ActivatedRoute,
    private userSvc: UserService,
    private loansSvc: LoansService,
    private router: Router,
    private quoteSvc: QuotesService,
    private invoicePdfSvc: InvoicesPdfService,
  ) { }
  ngOnInit() {
    this.events.pop();
    this.route.paramMap.pipe(take(1)).subscribe(params => {
      const loanId = params.get('id');
      const userId = params.get('userId');

      let user$: Observable<User>;
      if (userId) {
        user$ = this.userSvc.getUserByDocument(userId);
      } else {
        user$ = this.userSvc.getCurrentUser();
      }
      user$.subscribe(user => {
        if (user.cc) {
          this.user = user;
          this.loansSvc
            .getUserLoans(user.id)
            .pipe(take(1))
            .subscribe(loans => {
              this.loans = loans;
              for (let i = 0; i < this.loans.length; ++i) {
                if (this.loans[i].id === this.loanId) {
                  this.actualLoan = i;
                }
              }
              this.loadOutlays();
            });
          this.role = user.role;
        } else {
          this.router.navigate(['']);
        }
      });

      this.quoteSvc.getLoanQuotes(loanId).pipe(take(1)).subscribe((quotes: any) => {
        this.quotes = quotes
        this.quotes = quotes.sort((a, b) => a.date - b.date);
      })
    });
  }

  /**
   * Carga los desembolsos del crédito
   */
  loadOutlays() {
    this.loans[this.actualLoan].amount
    this.events.push({
      amount: this.loans[this.actualLoan].amount || 0,
      date: moment(this.loans[this.actualLoan].outlayDate.date).format('L'),
      type: 'outlay',
      obj: {}
    })
    this.loadBillsAndDebts()
  }

  /**
   * Carga las facturas y los pagos asociados al crédito
   */
  loadBillsAndDebts() {
    for (let index = 0; index < this.quotes.length; index++) {
      const element = this.quotes[index];
      if (element.invoice) {
        this.events.push({
          amount: element.invoice.total,
          date: moment(element.date).format('L'),
          type: 'bill',
          id: element.id,
          obj: {
            expDate: moment(element.invoice.expirationDate).format('L'),
            status: element.invoice.payed ? 'Pagada' : 'No pagada'
          }
        })
      } else {
        this.events.push({
          date: moment(element.date).format('L'),
          type: 'quote',
          obj: {
            name: element.numberQuote + 1,

          }
        })
      }
    }
  }

  /**
* Metodo que se ejecuta cuando el usuario da click en alguna de las facturas
* @param id id de la factura
*/
  openInvoice(id) {
    let quote = this.quotes.find(element => element.id = id)
    let loanAndInvoice = { ...this.loans[this.actualLoan], quote: quote, }
    this.invoicePdfSvc.generatePdf(loanAndInvoice)
  }
}
