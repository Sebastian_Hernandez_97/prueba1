import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { LoansService } from 'src/app/services/loans.service';
import { Observable, Subscription } from 'rxjs';
import { User, UserRole } from 'src/app/models/user';
import { take } from 'rxjs/internal/operators/take';
import { DatatableOptions } from 'ngx-3a';
import * as moment from 'moment';
import { MainService } from 'src/app/services/main.service';
import { Loan } from 'src/app/models/loans';
import { QuotesService } from 'src/app/services/quotes.service';
import { Quote } from 'src/app/models/quote';
import { InvoicesPdfService } from 'src/app/services/invoces-pdf.service';

//var moment = require('moment-timezone');   let dateQuote = moment().tz('America/Bogota').add(1, "months")
@Component({
  selector: 'app-loan-profile-admin',
  templateUrl: './loan-profile-admin.component.html',
  styleUrls: ['./loan-profile-admin.component.scss']
})
export class LoanProfileAdminComponent implements OnInit {

  @ViewChild('fileDownload', { static: false }) fileDownloadTemplate: TemplateRef<any>;
  @ViewChild('date', { static: false }) dateTemplate: TemplateRef<any>;
  @ViewChild('money', { static: false }) moneyTemplate: TemplateRef<any>;
  @ViewChild('payed', { static: true })
  payedTemplate: TemplateRef<any>;

  user: User = { name: '', PrimerNombre: '', PrimerApellido: '' };
  role: UserRole;
  options: DatatableOptions;
  loans: any = [];
  calendar: any;
  actualDebt: number;
  remainingOutlays: number;
  bankEntity: string;
  quotes: any;
  quotesShowAmortization: Array<any>
  remainingQuotes = 0;
  paidQuotes = 0;
  currentLoan: Loan;

  outlay: {
    amount: any,
    date: any,
    farmer: any
  }

  mora: {
    amount: any,
    date: any,
    farmer: any
  }

  //corrgir
  actualLoan: number = 0
  /**
   * Estado que determina si hay mora en el credito actual
   */
  hasDebts = false;

  /**
   * Estado que determina si se han generado facturas en el credito actual
   */
  hasInvoices = false;

  /**
   * Estado que determina si hay desembolsos en el credito actual
   */
  hasOutlays = false;

  /**
   * Arreglo con las moras del credito actual
   */
  actualLoanDebts = [];

  /**
   * Objeto que contiene el próximo desembolso del credito actual
   */
  closeOutlay: {
    amount: number,
    date: string,
    id: number
    veryClose: boolean
  };

  invoices: Array<any> = [];
  loansSubscribe$: Subscription

  constructor(
    private mainSvc: MainService,
    private route: ActivatedRoute,
    private userSvc: UserService,
    private loansSvc: LoansService,
    private router: Router,
    private quotesService: QuotesService,
    private invoicePdfSvc: InvoicesPdfService,


  ) {
  }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const loanId = params.get('id');
      const userId = params.get('userId');

      let user$: Observable<User>;
      if (userId) {
        user$ = this.userSvc.getUserByDocument(userId);
      } else {
        user$ = this.userSvc.getCurrentUser();
      }
      user$.subscribe(user => {
        if (user.cc) {
          this.user = user;
          this.loansSvc
            .getUserLoans(user.id)
            .pipe(take(1))
            .subscribe(loans => {
              this.loans = loans;
              this.currentLoan = this.loans[0]
              this.setAttributesOfVist()
              this.quotesShowAmortization = this.quotesService.calculateQuotes(this.currentLoan)
              this.getLoanQuotes();
              this.getAllQuotes();
            });
          this.role = user.role;
        } else {
          this.router.navigate(['']);
        }
      });
    });
  }

  /**
   * Metodo que setea el nombre del agricutlor para mostrarlo en el PDF
   * @param famer Datos del agricultor 
   */
  getCompleteName(farmer) {
    let name = `${farmer.PrimerNombre} ${farmer.SegundoNombre ? farmer.SegundoNombre : ''} ${farmer.PrimerApellido}`
    return name
  }

  /**
   * 
   * Este metodo setea los datos que neccita la vita apenas inicia el componente
   */
  setAttributesOfVist() {
    this.outlay = {
      amount: this.currentLoan.amount,
      date: moment(this.currentLoan.outlayDate.date).format('LL'),
      farmer: this.currentLoan.farmer.name
    }

    //CONFIGURAR MORAAAA
    this.mora = {
      amount: this.currentLoan.amount,
      date: moment(this.currentLoan.outlayDate.date).format('LL'),
      farmer: this.currentLoan.farmer.name
    }
  }

  /**
   * Metodo que setea el credito seleccionado y llama las funciones para que se actualicen
   * @param index Index del credito actual
   */
  setActualLoan(index) {
    this.currentLoan = this.loans[index];
    this.quotesShowAmortization = this.quotesService.calculateQuotes(this.currentLoan)
    this.actualLoanDebts = [];
    this.paidQuotes = 0;
    this.remainingQuotes = 0;
    this.getLoanQuotes();
    this.setAttributesOfVist()
    this.getAllQuotes();
  }

  /**
   * Metodo que trae las coutas del credito actual
   */
  getLoanQuotes() {
    this.loansSvc
      .getLoanQuotes(this.user.id, this.currentLoan.id)
      .subscribe((quotes: any) => {
        this.quotes = quotes.sort((a, b) => a.date - b.date);
        const start = moment(this.currentLoan.outlayDate.date);
        const finish = moment(quotes[quotes.length - 1].date);
        this.loadCalendarDates(start, finish);
      });
  }

  loadCalendarDates(start, finish) {
    const loan = this.currentLoan;
    const quotes: Quote[] = this.quotes
    let i = 0;
    this.calendar = [];
    let withDebt = false;
    let withInvoice = false;
    let withOutlay = false;
    while (finish.diff(start, 'months') >= 0) {
      withOutlay = true
      this.calendar[i] = {
        month: start.format('YYYY'),
        day: start.format('MMMM').toUpperCase().replace('.', ''),
        date: start,
        outlay: (i == 0) ? { amount: loan.amount } : {},
        invoice: {},
        quote: {},
        debt: {}
      };


      //calculo de cuotas 
      for (let j = quotes.length - 1; j >= 0; j--) {
        if (quotes[j]) {
          if (start.isSame(moment(quotes[j].date).toDate(), 'months')) {
            withInvoice = true;
            //RECORDAR CAMBIAR CUANDO SI EXISTA FACTURA EN LA COLECCION DE USUARIOS
            this.calendar[i].quote = (quotes[j].invoice) ? quotes[j].invoice : { number: i, date: moment(quotes[j].date).format('LL') }
          }
        }
      }
      ++i;
      start.add(1, 'months');
    }

    this.hasOutlays = withOutlay;
    this.hasDebts = withDebt;
    this.hasInvoices = withInvoice;
  }
  /**
 * Metodo que trae todas las cuotas de los creidtos
 */
  getAllQuotes() {
    this.quotesService.getLoanQuotes(this.currentLoan.id)
      .subscribe((quotes: any) => {
        this.mapQuotesASingleArray(quotes, this.currentLoan)
      });

  }

  /** 
   * Metodo que filtra las cuotas y saca solo las que ya han sido facturadas
   * @param quotes parametro que trae las coutas por cada loan
  */
  mapQuotesASingleArray(quotes, loan) {
    for (let index = 0; index < quotes.length; index++) {
      const element = quotes[index];
      if (element.invoice) {
        this.invoices.push({ quote: element, ...loan })
      }
    }
    if (this.invoices.length) {
      this.loadData()
    }
  }

  /**
 * Metodo que se ejecuta cuando el usuario da click en alguna de las facturas de la tabla
 * @param event Evento que trae los datos de la factura
 */
  openInvoice(event) {
    this.invoicePdfSvc.generatePdf(event)
  }

  /**
   * Carga los datos que se nececitaran para mostrar en la tabla
   */

  loadData() {
    const rows = [
      {
        name: '#',
        prop: 'quote.numberQuote'
      },
      {
        name: 'Fecha',
        prop: 'quote.date',
        cellTemplate: this.dateTemplate,
        flexGrow: 3
      },
      {
        name: 'Monto',
        prop: 'quote.invoice.total',
        cellTemplate: this.moneyTemplate

      },
      {
        name: 'Estado',
        prop: 'quote.invoice.payed',
        cellTemplate: this.payedTemplate
      },
      {
        name: 'Descargar',
        prop: 'amount',
        cellTemplate: this.fileDownloadTemplate
      },
    ];
    this.loadTableData(this.invoices, rows);
  }

  /**
   * Configura y carga la tabla de 3A
   * @param data Datos para cargar en la tabla de 3A
   */
  loadTableData(data, columns) {
    const options = new DatatableOptions();
    options.columns = columns;
    options.rows = data;
    this.options = options;
  }

  /**
   * Método para ver las amortizaciones de un crédito
   */
  openModalAmortization() {
    this.mainSvc.toogleModal.next(this.loansSvc.addAmortizationEvent(this.quotesShowAmortization));
  }

  navigate() {
    this.router.navigate(['./', 'creditHistory',
      (this.currentLoan ? this.currentLoan.id : '')], { relativeTo: this.route });
  }

  navigateToProfileFarmer(user){
   this.router.navigateByUrl(`/admin/profile-farmer/${user.id}`)
  }
}
