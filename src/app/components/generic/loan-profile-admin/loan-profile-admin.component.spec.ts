import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanProfileAdminComponent } from './loan-profile-admin.component';

describe('LoanProfileAdminComponent', () => {
  let component: LoanProfileAdminComponent;
  let fixture: ComponentFixture<LoanProfileAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanProfileAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanProfileAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
