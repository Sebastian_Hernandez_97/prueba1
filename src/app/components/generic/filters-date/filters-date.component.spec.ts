import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersDateComponent } from './filters-date.component';

describe('FiltersDateComponent', () => {
  let component: FiltersDateComponent;
  let fixture: ComponentFixture<FiltersDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltersDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
