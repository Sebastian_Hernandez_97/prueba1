import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-filters-date',
  templateUrl: './filters-date.component.html',
  styleUrls: ['./filters-date.component.scss']
})
export class FiltersDateComponent implements OnInit {

  @Output() dateSelect = new EventEmitter();

  constructor() { }
  currentYear:number;
  currentDate:any= moment();
  months = [
    {value:1, title:"Enero"},
    {value:2, title:"Febrero"},
    {value:3, title:"Marzo"},
    {value:4, title:"Abril"},
    {value:5, title:"Mayo"},
    {value:6, title:"Junio"},
    {value:7, title:"Julio"},
    {value:8, title:"Agosto"},
    {value:9, title:"Septie"},
    {value:10, title:"Octubre"},
    {value:11, title:"Noviem"},
    {value:12, title:"Diciem"},
  ]

  ngOnInit() {
    this.currentYear = moment(this.currentDate).year()    
  }

  monthSelected(month){
    let date = moment(`${this.currentYear} ${month} 01`, "YYYY MM DD").valueOf()
    this.dateSelect.emit(date)
  }

  nextYear(){
   this.currentDate = moment(this.currentDate).add(1, 'year')
   this.currentYear = moment(this.currentDate).year()
  }

  backYear(){
    this.currentDate = moment(this.currentDate).subtract(1, 'year')
    this.currentYear = moment(this.currentDate).year()
   }
}
