// Modulos
import { NgModule } from "@angular/core";
import { FormsModule, UtilitiesModule, DashboardModule } from "ngx-3a";
import { CommonModule } from "@angular/common";
import { FormlyModule } from "@ngx-formly/core";
import { FormsModule as f, ReactiveFormsModule } from "@angular/forms";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { RxReactiveFormsModule } from "@rxweb/reactive-form-validators";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { Ng5SliderModule } from "ng5-slider";
// Componentes
import { TextComponent } from "./components/fields/text/text.field.component";
import { PasswordComponent } from "./components/fields/password/password.field.component";
import { ModalComponent } from "./components/modals/modal.component";
import { AddUserModalComponent } from "./components/modals/add-user/add-user.component";
import { AddLoanModalComponent } from "./components/modals/add-loan/add-loan.component";
import { MatDatepickerModule, MatInputModule, MatFormFieldModule } from "@angular/material";
import { MatNativeDateModule } from "@angular/material/core";
import { ModalOutlayConfigComponent } from "./components/modals/outlay-config/outlay-config.component";
import { LoanProfileAdminComponent } from "./components/generic/loan-profile-admin/loan-profile-admin.component";
import { CreditHistoryComponent } from "./components/generic/credit-history/credit-history.component";
import { AmortizationModalComponent } from "./components/modals/amortization/amortization.component";
import { AddConciliationModalComponent } from "./components/modals/add-conciliation/add-conciliation.component";
import { DragAndDropFileDirective } from "./directives/dragAndDropFile.directive";
import { ApplyFiltersComponent } from "./components/modals/apply-filters/apply-filters.component";
import { AlertMessageComponent } from "./components/modals/alert-message/alert-message.component";
import { FiltersDateComponent } from "./components/generic/filters-date/filters-date.component";
import { AddFileAttachmentComponent } from './components/modals/add-file-attachment/add-file-attachment.component';
import { CommentsComponent } from './components/modals/comments/comments.component';
import { AddLoanConditionsModalComponent} from './components/modals/add-loan-conditions/add-loan-conditions.component';
import { AddBankInfoModalComponent} from './components/modals/add-bankInfo/add-bankInfo.component';
import { ReturnElectionModalComponent} from './components/modals/return-election/return-election.component';
@NgModule({
  declarations: [
    TextComponent,
    PasswordComponent,
    ModalComponent,
    AddUserModalComponent,
    AddLoanModalComponent,
    ApplyFiltersComponent,
    ModalOutlayConfigComponent,
    AmortizationModalComponent,
    AddConciliationModalComponent,
    LoanProfileAdminComponent,
    CreditHistoryComponent,
    AlertMessageComponent,
    DragAndDropFileDirective,
    FiltersDateComponent,
    AddFileAttachmentComponent,
    CommentsComponent,
    AddLoanConditionsModalComponent,
    AddBankInfoModalComponent,
    ReturnElectionModalComponent
  ],
  imports: [
    CommonModule,
    UtilitiesModule,
    FormsModule,
    f,
    DashboardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatFormFieldModule,
    ScrollingModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    NgxExtendedPdfViewerModule,
    Ng5SliderModule,
  ],
  providers: [],
  exports: [FormsModule, DashboardModule, FiltersDateComponent,DragAndDropFileDirective, ModalComponent, f, MatDatepickerModule, MatInputModule, MatFormFieldModule, NgxExtendedPdfViewerModule],
  bootstrap: [],
})
export class SharedModule {}
