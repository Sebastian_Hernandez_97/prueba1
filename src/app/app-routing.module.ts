import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./services/guards.service";
import { NotPage404Component } from "./components/not-page404/not-page404.component";

const routes: Routes = [
  {
    path: "auth",
    loadChildren: () => import("./submodules/auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "user",
    loadChildren: () => import("./submodules/userprofile/user-profile.module").then((m) => m.AuthModule),
  },
  {
    path: "admin",
    loadChildren: () => import("./submodules/admin/admin.module").then((m) => m.AdminModule),
    canActivate: [AuthGuard],
  },

  { path: "404", component: NotPage404Component },
  { path: "**", redirectTo: "auth" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
