// modulos
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "./shared.module";
import { AngularFireModule } from "@angular/fire";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireFunctionsModule } from "@angular/fire/functions";
import { FirebaseModule } from "ngx-3a-firebase";
import { registerLocaleData } from '@angular/common';
import localeCo from '@angular/common/locales/es-CO';
registerLocaleData(localeCo, 'CO');
// componentes
import { AppComponent } from "./app.component";
import { environment } from "src/environments/environment";
import {
  AuthenticationAlternative,
  DatabaseAlternative,
  ROLE_COMPARATOR
} from "ngx-3a";

// Servicios generales
import { UserService } from "./services/user.service";
import { BankTypeFormatterService } from "./services/bank-type-formatter.service";
import { CurrencyPipe } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { User, UserRole } from "./models/user";
import { of } from "rxjs";
import { NotPage404Component } from './components/not-page404/not-page404.component';
@NgModule({
  declarations: [AppComponent, NotPage404Component],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    FirebaseModule.forRoot({
      auth: {
        provider: AuthenticationAlternative.Firebase
      },
      database: {
        provider: DatabaseAlternative.Firebase
      }
    })
  ],
  providers: [
    UserService,
    BankTypeFormatterService,
    CurrencyPipe,
    {
      provide: ROLE_COMPARATOR,
      useValue: (user: User, role: UserRole) => {
        return of(user.role === role);
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
