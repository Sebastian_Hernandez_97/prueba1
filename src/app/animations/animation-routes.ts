import {
    transition,
    trigger,
    query,
    style,
    animate,
    group
} from '@angular/animations'

export const animationRoutes =
    trigger('animationRoutes', [
        transition('-1 => *', [
            query(':enter', [
                style({
                    position: 'absolute',
                    transform: 'translateX(-100%)',
                }),
                animate(
                    '500ms ease',
                    style({
                        opacity: 1,
                        transform: 'translateX(0%)',
                    }),
                ),
            ]),
        ]),
        transition(':decrement', [
            query(
                ':enter',
                style({
                    position: 'absolute',
                    transform: 'translateX(-100%)',
                }),
            ),
            group([
                query(
                    ':leave',
                    animate(
                        '500ms ease',
                        style({
                            position: 'absolute',
                            transform: 'translateX(110%)',
                        }),
                    ),
                ),
                query(
                    ':enter',
                    animate(
                        '500ms ease',
                        style({
                            opacity: 1,
                            transform: 'translateX(0%)',
                        }),
                    ),
                ),
            ]),
        ]),
        transition(':increment', [
            query(
                ':enter',
                style({
                    position: 'absolute',
                    transform: 'translateX(100%)',
                }),
            ),

            group([
                query(
                    ':leave',
                    animate(
                        '500ms ease',
                        style({
                            position: 'absolute',
                            transform: 'translateX(-110%)',
                        }),
                    ),
                ),
                query(
                    ':enter',
                    animate(
                        '500ms ease',
                        style({
                            opacity: 1,
                            transform: 'translateX(0%)',
                        }),
                    ),
                ),
            ]),
        ]),
    ])
