import { Invoice } from './invoice';

export interface Quote {
    id?: string;
    date?: number;
    amount?: number;
    taxAmount?: number;
    status?: QuoteStatus;
    invoice?: Invoice;
    paymentCapital?: number;
    othersServices?: number;
    methodPayment?: string;
    total?: number;
    paymentCapitalInThisQuote?: boolean;
    valuePaidToCapital?: number
}

export enum QuoteStatus {
    PAID,
    PENDING,
    OTHER
}
