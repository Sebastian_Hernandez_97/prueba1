import { alpha, required, numeric, maxLength } from '@rxweb/reactive-form-validators';

export interface BankInformation {
    id?: string;
    bank?: Bank;
    accountType?: BankAccountType;
    accountNumber?: string;
    paymentDestination?: DestinationPayment;
    paymentType?: PaymentType;

}

export interface Bank {
    id?: string;
    name: string;
}

export enum BankAccountType {
    SAVING,
    CHECKING
}

export enum DestinationPayment {
    CUSTOMER_PAYMENT,
    THIRD_PAYMENT
}

export enum PaymentType {
    CHECK,
    BANK_TRANSFER,
    THIRD_PAYMENT
}

export class BankInformationModel implements BankInformation {
    @required({ message: "Este campo es obligatorio" })
    bank?: Bank;


    @required({ message: "Este campo es obligatorio" })
    paymentType?: PaymentType


    @required({ message: "Este campo es requerido", conditionalExpression: (x, y) => x.paymentType == 1 })
    accountType?: BankAccountType

    @numeric({ message: "Este campo solo permite numeros" })
    @required({ message: "Este campo es requerido", conditionalExpression: (x, y) => x.paymentType == 1 })
    @maxLength({ message: "Este campo no permite mas de 11 digitos", value: 11 })
    accountNumber?: string;

    @required({ message: "Este campo es obligatorio" })
    paymentDestination?: DestinationPayment
}

