import { Auth, Timestamp, Database } from "3a-common";
import { alpha, required, numeric, maxLength, async, ValidationAlphabetLocale, minDate, email, maxDate, propObject } from "@rxweb/reactive-form-validators";
import { FormControl } from "@angular/forms";
import * as moment from "moment";

export interface User extends Auth.User {
  cc?: string;
  role?: UserRole;
  address?: string;
  PrimerNombre?: string;
  SegundoNombre?: string;
  PrimerApellido?: string;
  SegundoApellido?: string;
  idPicture?: string;
  status?: UserStatus;
  birth?: Timestamp;
  bankInformation?: any[];
  cellphone?: string;
  phone?: string;
}

export enum UserRole {
  ANALISTA,
  FARMER,
  COMERCIAL,
  DESEMBOLSO,
  REFERENCIACION,
  ADMIN,
  TIENDA,
  COMITE
}



export enum UserStatus {
  UNACTIVE,
  ACTIVE,
}

export interface Invitation {
  id?: string;
  email: string;
  name?: string;
  role: UserRole;
}

export class RefereceModel{
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  nombre?: string;
  
  @numeric({ message: "Este campo solo permite números" })
  telefono?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  parentesco?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  direccion?: string;
  
}

export class ReferecePersonalModel{
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  nombre?: string;
  
  @numeric({ message: "Este campo solo permite números" })
  telefono?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  quien_es?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  direccion?: string;
  
}

export class UserModel implements User {
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  @required({ message: "Este campo es obligatorio" })
  PrimerNombre?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  SegundoNombre?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  @required({ message: "Este campo es obligatorio" })
  PrimerApellido?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  SegundoApellido?: string;

  @numeric({ message: "Este campo solo permite números" })
  @required({ message: "Este campo es obligatorio" })
  @maxLength({ message: "Este campo no permite mas de 10 digitos", value: 10 })
  cc?: string;

  @numeric({ message: "Este campo solo permite números" })
  @required({ message: "Este campo es obligatorio" })
  cellphone?: string;

  @numeric({ message: "Este campo solo permite números" })
  phone?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  //@required({ message: "Este campo es obligatorio" })
  DireccionRes?: string;
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  //@required({ message: "Este campo es obligatorio" })
  DepartamentoRes?: string;
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  //@required({ message: "Este campo es obligatorio" })
  MunicipioRes?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  Vereda?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  predioNombre?: string;

  @alpha()
  name: string = "";

  @numeric()
  role?: UserRole;
}

export class PersonalDataModel {
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  @required({ message: "Este campo es obligatorio" })
  PrimerNombre?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  SegundoNombre?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  @required({ message: "Este campo es obligatorio" })
  PrimerApellido?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  SegundoApellido?: string;

  @required({ message: "Este campo es obligatorio" })
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  TipoDocumento?: string;

  @required({ message: "Este campo es obligatorio" })
  @maxDate({ message: "La fecha no puede ser mayor a a la actual", value: moment().format("M/D/YYYY") })
  FechaExpedicion?: any;

  @required({ message: "Este campo es obligatorio" })
  @maxDate({ message: "El agricultor no puede ser menor de edad", value: moment().subtract(18, "years").format("M/D/YYYY") })
  FechaNacimiento?: any;

  @numeric({ message: "Este campo solo permite números" })
  @required({ message: "Este campo es obligatorio" })
  @maxLength({ message: "Este campo no permite mas de 10 digitos", value: 10 })
  cc?: string;

  @required({ message: "Este campo es obligatorio" })
  Genero?: string;

  @required({ message: "Este campo es obligatorio" })
  EstadoCivil?: string;

  @required({ message: "Este campo es obligatorio" })
  NivelEstudios?: string;

  @required({ message: "Este campo es obligatorio" })
  PersonasCargo?: string;

  @required({ message: "Este campo es obligatorio" })
  ZonaResidencia?: string;

  @required({ message: "Este campo es obligatorio" })
  EstadoVias?: string;

  @required({ message: "Este campo es obligatorio" })
  DepartamentoRes?: string;

  @required({ message: "Este campo es obligatorio" })
  MunicipioRes?: string;
  
  @required({ message: "Este campo es obligatorio" })
  DireccionRes?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  Vereda?: string;

  @required({ message: "Este campo es obligatorio" })
  TipoVivienda?: string;

  @numeric({ message: "Este campo solo permite números" })
  @required({ message: "Este campo es obligatorio" })
  cellphone?: string;

  @numeric({ message: "Este campo solo permite números" })
  cellphone2?: string;

  @numeric({ message: "Este campo solo permite números" })
  @required({ message: "Este campo es obligatorio" })
  TiempoResidencia?: number;

  @required({ message: "Este campo es obligatorio" })
  Internet?: string;

  @required({ message: "Este campo es obligatorio" })
  WhatsApp?: string;

  @required({ message: "Este campo es obligatorio" })
  RedesSociales?: string;

  @required({ message: "Este campo es obligatorio" })
  @email({ message: "Este campo debe ser de tipo correo" })
  CorreoElectronico?: string;

  @required({ message: "Este campo es obligatorio" })
  EnvioCorrespondencia?: string;

  @required({ message: "Este campo es obligatorio" })
  ActividadEconomica1?: string;

  @required({ message: "Este campo es obligatorio" })
  ActividadEconomica2?: string;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  predioName?: string;
}

export class financialDataModel {
  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  propiedades?: number;

  @required({ message: "Este campo es obligatorio" })
  OrigenIngresos?: string;

  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  IngresoPromedio?: number;

  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  OtrosIngresos?: number;

  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  DetalleOtrosIngresos?: string;

  @required({ message: "Este campo es obligatorio" })
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  DireccionActividad?: string;

  @required({ message: "Este campo es obligatorio" })
  DepartamentoActividad?:string;

  @required({ message: "Este campo es obligatorio" })
  MunicipioActividad?:string;

  @required({ message: "Este campo es obligatorio" })
  @alpha({ locale: ValidationAlphabetLocale.spanish, allowWhiteSpace: true, message: "Este campo solo permite letras" })
  VeredaActividad?: string;

  @required({ message: "Este campo es obligatorio" })
  Tenencia?: string;

  @required({ message: "Este campo es obligatorio" })
  Experiencia?: string;

  @required({ message: "Este campo es obligatorio" })
  ExperienciaCultivo?: string;


  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  ServiciosPublicos?: string;

  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  GastosFamiliares?: string;


  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  GastosCelular?: string;

  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  GastosArriendo?: string;

  @required({ message: "Este campo es obligatorio" })
  @numeric({ message: "Este campo solo permite números" })
  CuotasCreditos?: string;
  
  @required({ message: "Este campo es obligatorio" })
  Propietario?:string;

  @required({ message: "Este campo es obligatorio" })
  PagoAlDia?: string;
}

export class ReferecesDataModel {
  @propObject(RefereceModel)
  familiar_reference_1:RefereceModel;

  @propObject(RefereceModel)
  familiar_reference_2:RefereceModel;

  @propObject(ReferecePersonalModel)
  personal_reference_1:ReferecePersonalModel;

  @propObject(ReferecePersonalModel)
  personal_reference_2:ReferecePersonalModel;
  
}


