
export interface Invoice {
    invoiceReecord?: number;
    amount?: number;
    date?: Date;
    expirationDate?: Date;
    pdf?: string;
    paid?: boolean;
    othersServices?: number;
    total?: number;
}
