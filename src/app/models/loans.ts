import { BankInformation, BankInformationModel } from './bank';
import { User, UserModel } from './user';
import { Agreement } from './agreement';
import { Outlay } from './outlay';
import * as moment from 'moment-timezone';
import { propObject, numeric, minLength, minNumber, required, toFloat, minDate, disable, trim, rtrim, maxDate, maxNumber } from '@rxweb/reactive-form-validators';

export interface Loan {
    id?: string;
    farmer?: UserModel;
    bankInformation?: BankInformationModel;
    amount?: number;
    outlayed?: number;
    debtStatus?: DebtStatus;
    loanCategory?: LoanCategory;
    interestRate?: number;
    interestOption?: string;
    PmryNoteNo?: string;
    agreement?: Agreement;
    paymentPeriodicity?: number;
    ccAnalyst?: string;
    outlay?: Outlay[];
    userId?: string;
    product?: Product;
    capitalSubscription?: number;
    firstCapitalSubscription?: Date;
    otherServices?: string[];
    monthsPay?: number[];
    graceMonths?: number;
    typeQuote?: string;
    paymentMethod?: string;
    typeInterestRate?: string;
    outlayDate?: any;
    firstInterestPayment?: Date;
    billingCycle?: string;
    state: boolean;
    balance?: any;
    quoteCapitalMonth?: number;
    stateBilling?: StateBilling;
    Estado?: Estado;
}

export enum Estado{
    INICIAL,
    ANALISIS,
    APROBADO,
    NEGADO, 
    DEVUELTO,
    COMITE
}

export enum EstadoRef{
    ESPERA,
    Exitosa,
    No_Exitosa,
    No_Contactado
}

export enum DebtStatus {
    NODEBT,
    T1,
    T2,
    T3
}

export enum LoanCategory {
    EXPRESS,
    LOW,
    MEDIUM,
    HIGH
}
export interface Product {
    id?: string;
    name?: string;
    outlaying?: boolean;
    interestRate?: number;
}

export interface StateBilling {
    moraQuotes: number;
    payedQuotes: number;
    pendingQuotes: number;
}


export class LoanModel implements Loan {
    state: boolean;

    @propObject(UserModel)
    farmer: UserModel

    @propObject(BankInformationModel)
    bankInformation: BankInformationModel

    @required({ message: "Este campo es obligatorio" })
    product?: Product;

    @numeric({ message: "Este campo solo permite numeros", isFormat: true })
    @minNumber({ message: "Este campo no permite numeros menores a 1", value: 1 })
    @required({ message: "Este campo es obligatorio" })
    amount?: number;


    @required({ message: "Este campo es obligatorio" })
    paymentPeriodicity?: number;

    @required({ message: "Este campo es obligatorio" })
    typeInterestRate?: string;

    @required({ message: "Este campo es obligatorio" })
    //@numeric({ message: "Este campo solo permite numeros", allowDecimal: true })
    @minNumber({ message: "Este campo no permite numeros menores a 1", value: 0 })
    @maxNumber({ message: "Este campo no permite numeros mayores a 3", value: 3 })
    @toFloat()
    interestRate?: number;

    @required({ message: "Este campo es obligatorio" })
    paymentMethod?: string;

    @required({ message: "Este campo es obligatorio" })
    typeQuote?: string;

    @required({ message: "Este campo es obligatorio" })
    capitalSubscription?: number;

    @required({ message: "Este campo es obligatorio" })
    agreement?: Agreement;

    @required({ message: "Este campo es obligatorio" })
    @minDate({ message: "La fecha no puede ser inferior a la actual", value: moment().format("M/D/YYYY") })
    @maxDate({ message: `la fecha no puede ser superior ${moment().add(1, 'months').format("M/D/YYYY")}`, value: `${moment().add(1, 'months').format("M/D/YYYY")}` })
    outlayDate?: any;

    @numeric({ message: "Este campo solo permite numeros" })
    @minNumber({ message: "Este campo no permite numeros menores a 1", value: 1 })
    @maxNumber({ message: "Este campo no permite numeros mayores a 3", value: 3 })
    graceMonths?: number;

    monthsPay?: number[] = [];

    otherServices?: string[] = [];

    outlayed?: number;
    debtStatus?: DebtStatus;
    loanCategory?: LoanCategory;
    interestOption?: string;
    PmryNoteNo?: string;
    ccAnalyst?: string;
    outlay?: Outlay[];
    userId?: string;
    firstCapitalSubscription?: Date;
    firstInterestPayment?: Date;
    billingCycle?: string;
    balance?: any;
    quoteCapitalMonth?: number;
    stateBilling?: StateBilling;

}

export class AnalistLoanModel implements Loan {
    state: boolean;

    @propObject(UserModel)
    farmer: UserModel

    //@propObject(BankInformationModel)
    //bankInformation: BankInformationModel

    @required({ message: "Este campo es obligatorio" })
    product?: Product;

    @numeric({ message: "Este campo solo permite numeros", isFormat: true })
    @minNumber({ message: "Este campo no permite numeros menores a 0", value:0 })
    @required({ message: "Este campo es obligatorio" })
    amount?: number;


    @required({ message: "Este campo es obligatorio" })
    paymentPeriodicity?: number;

    @required({ message: "Este campo es obligatorio" })
    typeInterestRate?: string;

    @required({ message: "Este campo es obligatorio" })
    //@numeric({ message: "Este campo solo permite numeros", allowDecimal: true })
    @minNumber({ message: "Este campo no permite numeros menores a 0", value: 0 })
    @maxNumber({ message: "Este campo no permite numeros mayores a 1", value: 1 })
    @toFloat()
    interestRate?: number;

    @required({ message: "Este campo es obligatorio" })
    paymentMethod?: string;

    @required({ message: "Este campo es obligatorio" })
    typeQuote?: string;

    @required({ message: "Este campo es obligatorio" })
    capitalSubscription?: number;

    @required({ message: "Este campo es obligatorio" })
    agreement?: Agreement;

    @minDate({ message: "La fecha no puede ser inferior a la actual", value: moment().format("M/D/YYYY") })
    @maxDate({ message: `la fecha no puede ser superior ${moment().add(1, 'months').format("M/D/YYYY")}`, value: `${moment().add(1, 'months').format("M/D/YYYY")}` })
    outlayDate?: any;
    
    @numeric({ message: "Este campo solo permite numeros" })
    @minNumber({ message: "Este campo no permite numeros menores a 0", value: 0 })
    @maxNumber({ message: "Este campo no permite numeros mayores a 3", value: 3 })
    graceMonths?: number;

    monthsPay?: number[] = [];

    otherServices?: string[] = [];

    outlayed?: number;
    debtStatus?: DebtStatus;
    loanCategory?: LoanCategory;
    interestOption?: string;
    PmryNoteNo?: string;
    ccAnalyst?: string;
    outlay?: Outlay[];
    userId?: string;
    firstCapitalSubscription?: Date;
    firstInterestPayment?: Date;
    billingCycle?: string;
    balance?: any;
    quoteCapitalMonth?: number;
    stateBilling?: StateBilling;

}




