

export interface Outlay {
    id?: string;
    date?: Date;
    amount?: number;
    delivered?: boolean;
}
