export interface EmailData {
  to: string;
  subject: string;
  titleRegular: string;
  titleBold: string;
  description: string;
  buttonTitle: string;
  actionLink: string;
}
