import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalEvent, AlertEvent } from '../models/navigation';

@Injectable({
  providedIn: 'root',
})
export class MainService {
  public toogleModal: Subject<ModalEvent> = new Subject();
  public toogleFullModal: Subject<ModalEvent> = new Subject();
  //   public toogleRBar: Subject<RBarEvent> = new Subject();
  public alert: Subject<AlertEvent> = new Subject();
  constructor() { }

}
