import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CustomerIdFormatterService {
  public static parseid(document: string) {
    let numRegExP = /\D/g;
    return document ? document.replace(numRegExP, "") : document;
  }
}
