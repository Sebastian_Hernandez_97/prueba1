import { Injectable } from '@angular/core';
import { ModalEvent, Modals } from '../models/navigation';
import { Invoice } from '../models/invoice';
import { FilterType } from '../components/modals/apply-filters/apply-filters.component';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor() { }

  /**
      * Crea el evento necesario para que se dispare el modal de aplicar filtros
      */
     applyFilters(filters:FilterType, callback?: (_:any) => void): ModalEvent {       
      return {
          type: Modals.FILTERS,
          payload: filters,
          confirmation: {
              text: '',
              callback: $payload => {
                callback($payload)
                  return true;
              },
          },
      };
  }
}
