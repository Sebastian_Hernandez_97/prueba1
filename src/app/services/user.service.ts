import { Injectable, Inject, forwardRef } from "@angular/core";
import { Database } from "3a-common";
import { tap, pluck, combineLatest, take, map } from "rxjs/operators";
import { concat, merge, Subscription } from "rxjs";

import { BehaviorSubject, Observable } from "rxjs";
import { AuthenticationService, FormService } from "ngx-3a";
import { AppConstantsService } from "./app-constants.service";
import { User, Invitation } from "../models/user";
import { ModalEvent, Modals } from "../models/navigation";
import { EmailService } from "./email.service";
import { environment } from "src/environments/environment";
import { SecurityService } from "./security.service";
import { FormlyFieldConfig } from "@ngx-formly/core";
import { DatabaseQueryRelation } from "3a-common/dist/database";
import { AngularFireAuth } from "@angular/fire/auth";

@Injectable({
  providedIn: "root",
})
export class UserService {
  private user: BehaviorSubject<User> = new BehaviorSubject({
    cc: "",
    name: "",
    last_name: "",
    role: 0,
    PrimerNombre: "",
    PrimerApellido: "",
  });

  $userLoggerSubcription: Subscription;
  constructor(
    private constants: AppConstantsService,
    @Inject(forwardRef(() => Database.DatabaseService))
    private db: Database.DatabaseService,
    @Inject(forwardRef(() => AuthenticationService))
    private authSvc: AuthenticationService,
    @Inject(forwardRef(() => EmailService))
    private emailSvc: EmailService,
    @Inject(forwardRef(() => SecurityService))
    private securitySvc: SecurityService,
    private formSvc: FormService,
    private afAuth: AngularFireAuth
  ) {
    // this.db.find<any>([], { name: 'quotes' }).pipe(take(1)).subscribe(users => {
    // 	for (const user of users) {
    //     this.db.find([], { name: `users/${user.id}/loans`}).subscribe()
    // 		this.db.save(user, { name: 'Quotes' }).subscribe();
    // 	}
    // });
  }
  signInUser(user: string) {
    return this.db
      .find<User>(
        [
          {
            key: "cc",
            relation: Database.DatabaseQueryRelation.Equal,
            value: user,
          },
        ],
        this.constants.usersCollection()
      )
      .pipe(
        tap((users) => {
          this.user.next(users[0]);
        })
      );
  }

  signInEmployee(email: string, password: string) {
    this.authSvc.signIn(email, password);
    this.user = this.authSvc.user;
    return this.authSvc.user;
  }

  /**
   * Metodo para cerrar sesión
   */
  logOut(): void {
    this.$userLoggerSubcription.unsubscribe();
    this.authSvc.signOut();
  }

  getCurrentUser() {
    return this.user;
  }

  authUserLogged(): Promise<any> {
    return new Promise((resolve) => {
      this.$userLoggerSubcription = this.afAuth.authState.pipe(map((user: any) => user.uid)).subscribe(async (id) => {
        this.getUser(id).subscribe((currentUser) => {
          resolve(currentUser);
        });
      });
    });
  }

  /**
   * Metodo para obtener el usuario por id
   * @param id Id del usuario
   */
  getUser(id: string): Observable<User> {
    return this.db.get<User>(id, { name: "Users" });
  }

  /**
   * Carga un usuario específico o el usuario que se encuentra loggeado
   * @param cc El identificador del usuario que se desea cargar
   */
  getUserByDocument(cc: string): Observable<User> {
    return this.db
      .find<User>(
        [
          {
            key: "cc",
            relation: Database.DatabaseQueryRelation.Equal,
            value: cc,
          },
        ],
        this.constants.usersCollection()
      )
      .pipe(pluck("0"));
  }

  /**
   * Crea el evento necesario para que se dispare el modal de agregar usuarios
   */
  addOrganizaitonClientEvent(callback?: (_: User[]) => void): ModalEvent {
    return {
      type: Modals.USER_ADD,
      payload: {},
      confirmation: {
        text: "",
        callback: ($payload) => {
          this.addUsersAndInvitations($payload as User[]);
          return true;
        },
      },
    };
  }
  /**
   * Método que agrega usuarios a la aplicación
   * @param users usuarios que se agregaran a la aplicación
   */
  addUsersAndInvitations(users: User[]) {
    combineLatest(users.map((u) => this.iviteUser(u)));
  }

  /**
   * Método que crea la invitación del usuario utilizando sendGrid
   * @param user  usuario que se agregara a la aplicación
   */
  iviteUser(user: User) {
    if (!user.id) {
      const invitation: Invitation = {
        email: user.email,
        name: user.name,
        role: user.role,
      };
      this.db.save(invitation, this.constants.invitationsCollection()).subscribe((invitation$) => {
        // Invite user
        this.emailSvc.sendEmail({
          to: user.email,
          actionLink: `${environment.pageURL}/auth/signup/${encodeURIComponent(this.securitySvc.encrypt(invitation$.id))}`,
          buttonTitle: "Completar registro",
          description: `Has sido invitado a unirte a
                        Agricapital.
                        Para completar tu registro debes hacer click abajo.`,
          subject: "¡Bienvenido a Agricapital!",
          titleBold: "Agricapital!",
          titleRegular: "¡Bienvenido a ",
        });
      });
    }
  }

  /**
   * Método encargado de obtener los usuarios de agricapital
   */
  getUsers(query: Database.DatabaseQuery[]) {
    return this.db.find(query, this.constants.usersCollection());
  }
  /**
   * Método encargado de guardar un usuario ene la base de datos
   * @param user usuario que sera guardado en la base de datos
   */
  saveUser(user: User) {
    return this.db.save(user, this.constants.usersCollection());
  }

  /**
   * Trae una invitación de un usuario que fue invitado pero no se ha registrado
   * @param id El id de la invitación pendiente
   */
  getInvitation(id: string): Observable<Invitation> {
    return this.db.get(this.securitySvc.decrypt(id), this.constants.invitationsCollection());
  }

  /**
   * Retorna los campos necesarios para crear un usuario
   */
  addUserRegisterFields(): FormlyFieldConfig[] {
    return [
      this.formSvc.textInput("name", "Nombre", "", true),
      this.formSvc.textInput("email", "Correo", "", true),
      this.formSvc.passwordInput("password", "Contraseña", true),
    ];
  }

  getForms() {
    const financialdata$ = this.db.find([{ key: "id", relation: DatabaseQueryRelation.Equal, value: "financialdata" }], { name: "FormSteps" });
    const personalData$ = this.db.find([{ key: "id", relation: DatabaseQueryRelation.Equal, value: "personalData" }], { name: "FormSteps" });
    const references$ = this.db.find([{ key: "id", relation: DatabaseQueryRelation.Equal, value: "references" }], { name: "FormSteps" });
    let finalObserbable = merge(financialdata$, personalData$, references$);
    return finalObserbable;
  }
}
