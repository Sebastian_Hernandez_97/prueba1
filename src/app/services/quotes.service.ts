import { Injectable } from '@angular/core';
import { Database } from '3a-common';
import { FormService } from 'ngx-3a';
import { AppConstantsService } from './app-constants.service';
import { UserService } from './user.service';
import * as moment from 'moment';
import { QuoteStatus, Quote } from '../models/quote';
import { LoansService } from './loans.service';

interface monthsPay {
  quotes: Array<number>
  numberQuotesCapital: number
}

@Injectable({
  providedIn: 'root'
})
export class QuotesService {
  constructor(
    private constants: AppConstantsService,
    private db: Database.DatabaseService,

  ) { }

  quotes: Quote[] = [];
  loan: any



  /**
   * Carga las cuotas de un crédito
   * @param loan El crédito a cargar
   */
  getLoanQuotes(loanId: string) {
    return this.db.find(
      [],
      this.constants.loansQuotesCollection(loanId)
    );
  }



  /**
 * Método para calcular las cuotas de un credito
 */
calculateQuotes(loan) {
  this.quotes = []
  this.loan = loan
  const termToPay = Number(loan.paymentPeriodicity)
  const monthsPay = this.monthsPay()
  const quoteCapitalMonth = Number(this.quoteCapitalMonth().toFixed(2))
  const othersServices = this.othersServices()
  let interestRate = Number(loan.interestRate)
  let amount = Number(loan.amount)
  let interest: number
  let paymentCapital: number
  let total: number
  let dateQuote = moment().add(1, "months")
  let TotalAmount = amount
  if (Number(loan.billingCycle) === 0) {
    dateQuote = dateQuote.startOf('month').add(4, "days")
  } else {
    dateQuote = dateQuote.startOf('month').add(19, "days")
  }
  //for que calcula los valores a pagar de cada cuota
  for (let index = -1; index < termToPay; index++) {
    if (index == -1){
      interest = 0
      paymentCapital = 0
      amount = amount
      total = 0
    } else {
      //Si la cuota es fija: Toca mirar el tipo de interes
      if (loan.typeQuote === "cuota-fija") {
        interestRate = Number(loan.interestRate)        
        interest = (monthsPay['quotes'][index] === 2) ? interestRate * amount : 0
        if (loan.paymentMethod == "vencido"){
          paymentCapital = (monthsPay['quotes'][index] === 2) ? (interestRate*Math.pow(1+interestRate,monthsPay['numberQuotesCapital']+1))*this.loan.amount/(Math.pow(1+interestRate,monthsPay['numberQuotesCapital']+1)-1)-interest : 0 
        } else if (loan.paymentMethod == "anticipado"){
          paymentCapital = (monthsPay['quotes'][index] === 2) ? (interestRate*Math.pow(1+interestRate,(monthsPay['numberQuotesCapital'])))*this.loan.amount/(Math.pow(1+interestRate,(monthsPay['numberQuotesCapital']))-1)-interest : 0 
        }        
        amount = amount - ((monthsPay['quotes'][index] === 2) ? paymentCapital : 0)
        total = (monthsPay['quotes'][index] === 2) ? (paymentCapital + interest + othersServices) : (monthsPay['quotes'][index] === 1) ? othersServices : 0
      } else {
        if (loan.typeInterestRate == "DIF"){ //Si es DTF
          const DTF = 0.0449
          const DTF_Spreads = 0.14
          let DTFea :number
          DTFea = Math.pow(1+((4*(Math.pow(1+DTF,0.25)-1)/(1+(Math.pow(1+DTF,0.25)-1)) + DTF_Spreads)/4)/(1-((4*(Math.pow(1+DTF,0.25)-1)/(1+(Math.pow(1+DTF,0.25)-1)) + DTF_Spreads)/4)),4)-1
          if (loan.paymentMethod == "vencido"){          
            interestRate = Math.pow(1+DTFea,1/(12/loan.capitalSubscription))-1
          } else if (loan.paymentMethod == "anticipado"){
            interestRate = (Math.pow(1+DTFea,1/(12/loan.capitalSubscription))-1)/(1+(Math.pow(1+DTFea,1/(12/loan.capitalSubscription))-1))
          }
        } else if (loan.typeInterestRate == "IBR"){ //Si es IBR
          const IBR = 0.04113
          const IBR_Spreads = 0
          let IBRea :number
          IBRea = Math.pow(1+(IBR + IBR_Spreads)/12,12)-1
          if (loan.paymentMethod == "vencido"){          
            interestRate = (monthsPay['quotes'][index] === 2) ? Math.pow(1+IBRea,1/(12/loan.capitalSubscription))-1 : 0
          } else if (loan.paymentMethod == "anticipado"){
            interestRate = (monthsPay['quotes'][index] === 2) ? (Math.pow(1+IBRea,1/(12/loan.capitalSubscription))-1)/(1+(Math.pow(1+IBRea,1/(12/loan.capitalSubscription))-1)) : 0
          } 
        }
        if (loan.paymentMethod == "vencido"){
          paymentCapital = (monthsPay['quotes'][index] === 2) ? TotalAmount/(monthsPay['numberQuotesCapital']) : 0
        } else if (loan.paymentMethod == "anticipado"){
          paymentCapital = (monthsPay['quotes'][index] === 2) ? TotalAmount/(monthsPay['numberQuotesCapital']-1) : 0
        } 
        if (index == termToPay-1 && loan.paymentMethod == "anticipado"){
          paymentCapital = 0
        }
        interest = (monthsPay['quotes'][index] === 2) ? (amount * interestRate) : 0
        amount = amount - ((monthsPay['quotes'][index] === 2) ? paymentCapital : 0)
        total = (monthsPay['quotes'][index] != 0) ? (paymentCapital + othersServices + interest) : 0
      }
    }
    this.addToList(amount, paymentCapital, interest, dateQuote, othersServices, loan.paymentMethod, total);
    dateQuote.add(1, "months")
  }
  return this.quotes
}


  /**
   * Método para agregar una cuota
   * @param amount monto actual de la cuota
   * @param paymentCapital cantidad de abono al capital
   * @param interestQuote cantidad de pago de intereses en la couta
   * @param quoteDate fecha en la cual queda el pago de la cuota
   * @param othersServices saldo que debe cancelar de otros servicios que ofrece el banco
   * @param total todal a cancelar de la cuota
   **/
  addToList(amount: number, paymentCapital: number, interestQuote: number, quoteDate: any, othersServices: number, methodPayment: string, total: number) {
    this.quotes.push({
      amount: amount,
      paymentCapital: paymentCapital,
      methodPayment: methodPayment,
      date: quoteDate.valueOf(),
      status: QuoteStatus.PENDING,
      taxAmount: interestQuote,
      othersServices: othersServices,
      total: total
    });
  }


  /**
 * Metodo que retorna los meses en los cuales se debe hacer abono al capital  
 **/
  monthsPay(): object {
    let months: monthsPay = {
      quotes: [],
      numberQuotesCapital: 0
    }
    let temp: number = 0
    let count: number = 0
    let monthsPay = (this.loan.monthsPay && this.loan.monthsPay.length != 0) ? this.loan.monthsPay : []
    let periodicity: number = this.loan.capitalSubscription
    let graceMonth: number


    if (monthsPay.length) {
      var orderedMonths = monthsPay.sort((a, b) => a - b);
      for (let index = 0; index < this.loan.paymentPeriodicity; index++) {
        if (index === periodicity - 1 && orderedMonths[count] === index) {
          count++
          temp = 2
          periodicity = periodicity + Number(this.loan.capitalSubscription)
        } else {
          if (index === periodicity - 1) {
            periodicity = periodicity + Number(this.loan.capitalSubscription)
            if (orderedMonths[count] === index) {
              temp = 2
              count++
            } else {
              temp = 1
            }
          } else {
            (orderedMonths[count] === index) ? (temp = 2, count++) : temp = 0
          }
        }

        months.quotes.push(temp)
      }
      months.numberQuotesCapital = count

    } else {
      graceMonth = this.loan.graceMonths ? this.loan.graceMonths : 0
      for (let i = 0; i < this.loan.paymentPeriodicity; i++) {
        if (i === periodicity - 1) {
          (i > graceMonth - 1) ? (temp = 2, count++) : 0
          periodicity = periodicity + Number(this.loan.capitalSubscription)
        } else {
          temp = 0
        }
        months.quotes.push(temp)
      }
      months.numberQuotesCapital = count
    }
    return months
  }
  /** 
   * Meotodo que retorna el valor del abono a capial 
   **/
  quoteCapitalMonth(loan?): number {
    if (loan) this.loan = loan
    const monthsPay = this.monthsPay()
    if (this.loan.typeQuote === "cuota-fija") {
      return Number(this.loan.amount) / ((1 - Math.pow(this.loan.interestRate + 1, -monthsPay['numberQuotesCapital'])) / this.loan.interestRate);
    } else {
      return Number(this.loan.amount) / monthsPay['numberQuotesCapital']
    }
  }

  /**
   * Metodo que retorna el valor de los servicios adicionales que se contrata
   **/
  othersServices(): number {
    let total: number = 0
    if (this.loan.otherServices) {
      for (let index = 0; index < this.loan.otherServices.length; index++) {
        const element = this.loan.otherServices[index]['price'];
        total = total + element
      }
    }
    return total
  }


}
