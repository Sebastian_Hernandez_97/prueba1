import { Injectable } from '@angular/core';
import { EmailData } from '../models/email';
import { AngularFireFunctions } from '@angular/fire/functions';

@Injectable({ providedIn: 'root' })
export class EmailService {
  constructor(private fun: AngularFireFunctions) { }

  sendEmail(emailData: EmailData) {
    const callable = this.fun.httpsCallable('genericEmail');
    callable(emailData).subscribe();
  }
}
