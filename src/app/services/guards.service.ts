import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { Observable } from "rxjs";
import { take, map, tap } from "rxjs/operators";
import { UserService } from "./user.service";
import { UserRole } from "../models/user";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private afAuth: AngularFireAuth, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.afAuth.authState.pipe(
      take(1),
      map((user) => {
        return !!user;
      }),
      tap((loggedIn) => {
        if (!loggedIn) {
          this.router.navigate(["/auth/admin"]);
        }
      })
    );
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }
}

@Injectable({
  providedIn: "root",
})
export class RoleGuard implements CanActivate {
  constructor(private afAuth: AngularFireAuth, private router: Router, private userSvc: UserService) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.userSvc.authUserLogged().then((currentUser) => {
          if (next.data.roles && next.data.roles.indexOf(currentUser.role) === -1) {
            switch (currentUser.role) {
              case UserRole.ANALISTA:
                this.router.navigateByUrl("/admin/applications-loans");
                break;
              case UserRole.COMERCIAL:
                this.router.navigateByUrl("/admin/applications-loans");
                break;

              case UserRole.DESEMBOLSO:
                this.router.navigateByUrl("/admin/payout");
                break;

              case UserRole.ADMIN:
                this.router.navigateByUrl("/admin/loans");
                break;

              case UserRole.REFERENCIACION:
                this.router.navigateByUrl("/admin/applications-loans");
                break;

              case UserRole.COMITE:
                this.router.navigateByUrl("/admin/applications-loans");
                break;

              default:
                this.router.navigateByUrl("/404");
                break;
            }
            resolve(false);
          } else {
            resolve(true);
          }
        });
    });
  }
}
