import { Injectable } from '@angular/core';
import { ModalEvent, Modals } from '../models/navigation';


@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor() { }

  /**
      * Crea el evento necesario para que se dispare el modal de comentarios
      */
     showModalComments(atributes?:object, callback?: (_:any) => void): ModalEvent {       
      return {
          type: Modals.COMMENTS,
          payload: atributes,
          confirmation: {
              text: '',
              callback: $payload => {
                callback($payload)
                  return true;
              },
          },
      };
  }
}
