import { Injectable } from "@angular/core";
import { AppConstantsService } from "./app-constants.service";
import { Database } from "3a-common";
import { Observable } from 'rxjs';
import { Estado } from '../models/loans';
import { Modals, ModalEvent } from '../models/navigation';

@Injectable({
  providedIn: "root",
})
export class LoansApplicationsService {
  constructor(
    private constants: AppConstantsService,
    private db: Database.DatabaseService
  ) {}

/**
 * Metodo que trae todas la solicitudes de credito
 */
  getApplications(): Observable<any> {
    return this.db.find<any>([], this.constants.loansApplicationsCollection());
  }

  /**
   * Metodo que trae una solicitud en especifico
   * @param id id de la solicitud
   */
  getApplication(id):Observable<any>{
      return this.db.get(id,this.constants.loansApplicationsCollection());
  }

  /**
   * 
   * @param applicationLoan guarda la solicitud
   */
  saveApplication(applicationLoan: any) {
    return this.db.save(applicationLoan, this.constants.loansApplicationsCollection());
  }

  changeStateApplicationLoan(data, state){
    let stateObj = { Estado: state };
    let dataSave = Object.assign(data, stateObj);
    this.saveApplication(dataSave);
  }

  addLoanConditionEvent(data?: any, callback?: (_: any) => void): ModalEvent {
    return {
      type: Modals.LOAN_CONDITIONS,
      payload: {
        data:data,
      },
      confirmation: {
        text: "",
        callback: ($payload) => {
          console.log("Debug Acá")
          console.log($payload.loan)
          this.saveApplication($payload.loan);
          return true;
        },
      },
    };
  }

  ReturnElectionEvent(data?: any, callback?: (_:any) => void): ModalEvent {
    return{
      type:Modals.RETURN,
      payload: {
        data:data
      },
      confirmation: {
        text: "",
        callback: ($payload) =>{
          return true;
        }

      }
    }
  }

  addbankInfoEvent(data?: any, callback?: (_: any) => void): ModalEvent {
    return {
      type: Modals.BANK_INFO,
      payload: {
        data,
      },
      confirmation: {
        text: "",
        callback: ($payload) => {
          /*console.log("Debug Acá")
          console.log($payload.loan)
          this.saveApplication($payload.loan);*/
          return true;
        },
      },
    };
  }



}
