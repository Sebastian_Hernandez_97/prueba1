import { Injectable } from '@angular/core';
import { ModalEvent, Modals } from '../models/navigation';


@Injectable({
  providedIn: 'root'
})
export class AddFileAttachmentService {

  constructor() { }

  /**
      * Crea el evento necesario para que se dispare el modal de alertas
      */
     showModalAddFile(atributes?:object, callback?: (_:any) => void): ModalEvent {       
      return {
          type: Modals.ADD_FILES_ATTACHMETN,
          payload: atributes,
          confirmation: {
              text: '',
              callback: $payload => {
                callback($payload)
                  return true;
              },
          },
      };
  }
}
