import { Injectable } from '@angular/core';
import { ModalEvent, Modals } from '../models/navigation';


@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  /**
      * Crea el evento necesario para que se dispare el modal de alertas
      */
     showAlert(alertAtributes:object, callback?: (_:any) => void): ModalEvent {       
      return {
          type: Modals.ALERTS,
          payload: alertAtributes,
          confirmation: {
              text: '',
              callback: $payload => {
                callback($payload)
                  return true;
              },
          },
      };
  }
}
