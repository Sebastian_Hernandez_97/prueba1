import { Injectable } from "@angular/core";
import { Database } from "3a-common";

@Injectable({
  providedIn: "root",
})
export class AppConstantsService {
  constructor() {}

  /**
   * La colección de los usuarios
   */
  usersCollection(): Database.Collection {
    return { name: "Users" };
  }

  /**
   * La colección de los solicitudes
   */
  loansApplicationsCollection(): Database.Collection {
    return { name: "ApplicationsLoans" };
  }
  /**
   * La colección de los creditos
   */
  loansCollection(): Database.Collection {
    return { name: "Loans" };
  }

  /**
   * La colección de los cuotas
   */
  quotesCollection(): Database.Collection {
    return { name: "Quotes" };
  }

  /**
   * La colección de los usuarios
   */
  invitationsCollection(): Database.Collection {
    return { name: "Invitations" };
  }

  /**
   * Trae la colección de los préstamos de un usuario específico
   * @param id El usuario específico
   */
  userLoansCollection(id: string): Database.Collection {
    return { name: `Users/${id}/Loans` };
  }

  /**
   * Trae la colección de cuotas de un prestamo especifico
   * @param id El credito especifico
   */
  loansQuotesCollection(id: string): Database.Collection {
    return { name: `Loans/${id}/Quotes` };
  }
  /**
   * Trae la colección de cuotas de un usuario y un préstamo específicos
   * @param userId El usuario específico
   * @param loanId El préstamo específico
   */
  userLoanQuotesCollection(userId: string, loanId: string): Database.Collection {
    return {
      name: `${this.userLoansCollection(userId).name}/${loanId}/Quotes`,
    };
  }
  /**
   * La colección de los bancos
   */
  banksCollection(): Database.Collection {
    return { name: "/Metadata/info/Banks" };
  }

  /**
   * La colección de los bancos
   */
  agreementCollection(): Database.Collection {
    return { name: "/Metadata/info/Agreements" };
  }

  /**
   * La colección de los otros servicios que presta el banco
   */
  othersCollection(): Database.Collection {
    return { name: "/Metadata/info/Others" };
  }

  /**
   * La colección de los ciclos de facturación
   */

  billingCycleCollettion(): Database.Collection {
    return { name: "/Metadata/info/BillingCycle" };
  }
  /**
   * La colección de las lineas de crédito
   */
  loansLinesCollection(): Database.Collection {
    return { name: "/Metadata/info/Products" };
  }

  /**
   * La colección de los plazos para un credito
   */
  metadata() {
    return { name: `/Metadata/` };
  }
}
