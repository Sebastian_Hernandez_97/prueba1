import { Injectable } from "@angular/core";
import { User } from "3a-common/dist/authentication";
import { ModalEvent, Modals } from "../models/navigation";
import { AppConstantsService } from "./app-constants.service";
import { Database } from "3a-common";

@Injectable({
  providedIn: "root",
})
export class ConciliationService {
  constructor(private constants: AppConstantsService, private db: Database.DatabaseService) {}

  /**
   * Crea el evento necesario para que se dispare el modal de agregar usuarios
   */
  addOrganizaitonClientEvent(invoices, callback?: (_: User[]) => void): ModalEvent {
    return {
      type: Modals.CONCILIATION,
      payload: {
        invoices: invoices,
      },
      confirmation: {
        text: "",
        callback: ($payload) => {
          return true;
        },
      },
    };
  }



  /**
   * Metodo para agregar el recibo de conciliacion
   */

  addReceiptOfConciliationInTheInvoice(data, quote) {
    this.db.save(quote, { name: `Loans/${data.id}/Quotes` }, true);
    this.db.save(quote, { name: `Users/${data.quote.userId}/Loans/${data.id}/Quotes` }, true);
  }
}
