import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { environment } from 'src/environments/environment';
import { Logger } from 'ngx-3a';

@Injectable({
  providedIn: 'root',
})
export class SecurityService {
  logger = new Logger('Security', environment.production);
  constructor() { }

  encrypt(data: string) {
    try {
      return CryptoJS.AES.encrypt(data, environment.security.key).toString();
    } catch (e) {
      this.logger.error(e);
    }
  }

  decrypt(data: string) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, environment.security.key);
      if (bytes.toString()) {
        return bytes.toString(CryptoJS.enc.Utf8);
      }
      return data;
    } catch (e) {
      this.logger.error(e);
    }
  }
}
