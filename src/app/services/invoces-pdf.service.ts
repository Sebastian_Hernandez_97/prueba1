import { Injectable } from '@angular/core';
import { Database } from '3a-common';
import pdfMake from 'pdfmake/build/pdfmake';
import * as moment from 'moment';
import pdfFonts from './fonts';

moment.locale('es');

pdfMake.fonts = {
  Montserrat: {
    normal: 'Montserrat-Medium.ttf',
    bold: 'Montserrat-Bold.ttf',
    italics: 'Montserrat-Italic.ttf',
    bolditalics: 'Montserrat-BoldItalic.ttf'
  }
}
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root',
})
export class InvoicesPdfService {
  constructor() { }

  /**
   * Metodo que realiza la creacion del PDF con la factura
   * @param data Datos del credito y la factura que se va genera
   */
  generatePdf(data) {
    var dd = {
      defaultStyle: {
        font: 'Montserrat'
      },
      pageSize: 'A3',
      pageMargins: [10, 20, 20, 20],
      content: [
        /* CABECERA */
        {
          style: 'table',
          table: {
            widths: ['*', '*'],
            body: [
              [
                [
                  {
                    table: {
                      widths: ['*', '*'],
                      body: [
                        [
                          { text: '\n\nMes:', fontSize: 12, border: [false, false, false, false] },
                          { text: `\n\n${data.quote.numberQuote + 1}`, fontSize: 12, fillColor: "#d6d6d6", alignment: 'right', border: [false, false, false, false] }
                        ],
                        [{ text: '', border: [false, false, false, false], margin: [0, 10, 0, 0] }, { text: '', border: [false, false, false, false] }],
                        [
                          {
                            colSpan: 2,
                            border: [false, true, false, true],
                            margin: [0, 5, 0, 0],
                            columns: [
                              {
                                alignment: 'left',
                                lineHeight: 1.3,
                                text: ['Factura de pago No:\n', 'Pagaré No:\n', 'Fecha desembolso:'],

                              },
                              {
                                alignment: 'right',
                                lineHeight: 1.3,
                                text: ['1219150\n', '150\n', `${moment(data.outlayDate.date).format('L')}`]
                              }
                            ]
                          },
                        ]
                      ]
                    },
                    layout: {
                      hLineWidth: function (i, node) {
                        return (i === 2 || i === node.table.body.length) ? 2 : 1;
                      }
                    }
                  }
                ]
                ,
                {
                  image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABHCAMAAAB1YPihAAAACXBIWXMAAAsSAAALEgHS3X78AAAAY1BMVEVKRkZKRkZKRkZKRkZKRkZMSUZKRkZKRkZKRkZHcExKRkZKRkZKRkZKRkZKRkZKRkaZuCSfvjC/1WakwDe0zVK+1GS/1WaevC6uyEq/1Wa/1WalwTm/1WaRsRZKRka/1Wasx0YwT0APAAAAHXRSTlOBIDChYBDxQMEA0eGRsVBw+MKBlB9BYOA3qNRbwSegv5EAAAgYSURBVGje3ZqHduMqEEDpHeRUJ5usdv//K98AKiDhJnsT55FjO1aBucBUGekvbW8vr4+Pj++fn/D++PLCb9cz+jqG1/dff1bt8/H17QeBPL++rxD+ju3P3/fX558Awl9/LRF2v+u2+7t/er5zkJfFWqwgRpbf+6c7BlksxiGKsT283SfIEuP36bZ/uz+Ql3Mw9rsHaPv9bnclyj8DeX7/9f74+vISHcdnE2P3UCv4x9PDPqM83xNIYVHB7b09rCjaE//xFGEe7tchcv20LzbUx7FrP552u4/79eyaP51vm56f7hgExNvHPfX8T/quQRhCbEMnTilV3Ic7r/reKBfWi3LGTBMVGzlnYInQKHEFwntoGyJSELufOpLx29DslmmR6VZ51jLEK9UapIvHu+tAXF81xb8DxMTj5hoQ7vtFM3w7SNyyil0OgvPQ5GIQUC2UZ5D2/dUkM0japOhykGFXuO2mo8s9CASTgZnN3y41jLOybwWBgQW87HZPIWq9GNYHb+1vI0iAgzTOadg6MFvuJXrdEm8EiaOSqCd068B+qWN5iezXgsRBbbJcYmtxQ6ysnttoPq4BYXkTxL21sHhEQmuqJRwvhVzrdlbcdDPzWfmF73gREYBWSy/S8XHYdI/GUiZ3QMfRsRtcrXLyGIjKaomnU8NC0azBPeV6PKXi53jCdqk/OdrNtt2XtjTJqJjxMJtsg+f5yEKODeSoPO0QwbRA8LgrfGVniJhuFqQEKU6oCkQeNgOFc5lBRHE4b+oWCDd1B4IcAunG6ISVYQquhilAbHGCngTBKzfpJpA6ojkEsvK0ibkFYseF4KWdSUNRoOeDe1OzACaaaThs+JkrYhCTzNnZuYwg1iEv5rAiiw6Rrc3rDS2viKBIBuTnqWiAkFk1/GxnSLHp8+LOIHTUIa5Pg2gmpmXOk9vNIKyI0twEsrBaMPzooMhk1BsgdJaYzT7MlV6FVCB2YQVPgGjCyggid6tKzed2FK8JojnitcbhJogoMhH4X8zhMK7s2gTCLgRpWX81K/gk3kEQvbTzsgUSwxNfuvgw3mBqASYQvh2kq0B8bRHkdSC+DLEmqkW8IksQvR1EViBoJd4VINFSiTrU4MOIqA2iLgbBDNHks5U5AGJOgIAdyx2ogyBdPfVu0IEFiL4CBPuGx8ievQ6tDoIgseihBWJqAcigG7cDkaI/BCLPAuFr59kASWqGijZYt00grWwGi76/bkVcfw5I46rUwQKEnABpRL84bekhLqTkuLIf0ZFs0Qzjx5XdtkBsltydr+w5H1HrJD674u4Kq5UktvyE1ZJ9s8l5L80e6yiIWqXoaYpEmk9xwCGqc/yIrytuB0Do2loPsYmtPB89BZLn3y98OKRG9UrVINMA3RHPXpsF3gZJifWi0kHyFNIyFuHiFMhQRaGL6hAmlcB5mVQ71jIrELdeEdYGYa3yokkIIW2MsrRwFGTMIWxSyjBYTDqIRqvodwbJAg6ZUxX9un4cH83J1GTLZY5BhlAdZQG7Vjzk8+QJWXi04yB6yuPEZECiiuZ7DZKQTth+CdJbisZ/SQmSZ17ExD7/R5mUnZ91eEjYYmEV5S+8mdPxAdkOYcVpkGVGOkxjw5yoZobodQkym9PGpYV5SV5wnPtWKaark212GmRVxTbkgD+bQEp0y2uQaQLK2sGY58vS0wKIbVc+Rs0J46zYoM8AKW5IRWBe2a+MEOoQxS2fQcwgOoixikJmYMFmI0bGAjNKvrdRkePxeNLzEGNOFIoQxSlVlUFZ/YQp0DSo9azod6hKxepVKna52ahKapOiyKquNYjB6PhtKIwZh8eCWB4tP3y4qFTe3oTbmzr7ic5tH4aSDc8I7hIk/F9A6O2G/VYQXOfDPwyE0tHyJF+HfihIFN6E2T0I/kNBwhTsiKue2Hz/1qqKBkLqHwui+VSFEZTfmCM9n0f4a0D08MsVJJfP4sjBSlwpGgF6cmE4wOWqm6VI1/mReUukH+WoQ/NchqAO1Mpc+NMamUpECB1xBOxGIBddjS/9mY4U0ToeAUHK3xKEaYbjj6U4g9BYStbFkh4ckFLjuPV5hwhcAseIxiy+x5vSJ0EQ3yOMJFwTt0l8bBu3a+pDS+V8BMExB5Rxa+MoJsNDH9riSBri4dwJhNnbQNITY6WVl45ig4KRSIRgeRCIGoQ6xTqrvWKqU5CaMsNk38mUY/VOGowUpAa6p8FbFCzJsw+7NfaRQDgkP6CWsUaIcO98DP65SD2l89p1mlFJ+9gJtR2yG0FSigB/MH/pyRUIAfkFqM28uQ3OhRCQG7QdxoY0RafLZdpqIEO2gqibQODdsSSoFHwEiecp8iA49CTjnqKQ0hgNRpSnTmK/ilyxtVT6VJwaZVEWJSU7IIExSsicTMoYnvXxSzqQPplRCmSIibGFe0uQ+IoXOj+CeBPzJsF96im9gpQCizwbw0x014MwPwsxrEjksUTMK6ImEMiepYs7PMsA6+jgzm4Fwq1BkkZFyL9ZpLBXDU+WnMHmRT6qEU+dBOjE32BFpOfSDEJ0RjKYSh9w12sDmxh0BDlCuwmkZ9wTI7FLMjDPQ1wRbjG2FQjYYKThOsAxOHhMhNMu9gRCx9nCFpJblJfVyrBRR9zw7tI78xSx6DzghZTDjEF+3lHNqWLgRzioPidwKUlpOhiAmL+zPA3MuzTjMn4OfeQLdbR/lML3oGIZ30czqGIVIE8jhU0X+vQFLiNf+7vfi13QqRDAXulH7gKko7CM3wfCbtdVQGXI8B+TkbIhkOSDFAAAAABJRU5ErkJggg==',
                  width: 180,
                  alignment: 'right',
                  margin: [0, 60, 15, 0]
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
        /* DETALLES DEL TITULAR */
        {
          style: 'table',
          table: {
            widths: ['*'],
            body: [
              [{ text: 'Detalle del titular', color: '#fff', bold: true, fillColor: '#383838', margin: [15, 2, 0, 2] }],
              [
                {
                  border: [false, true, false, true],
                  margin: [40, 0, 0, 0],
                  columns: [
                    {
                      width: 150,
                      alignment: 'left',
                      lineHeight: 1.3,
                      text: ['Nombre del titular:\n', 'Cédula:\n', 'Celular:\n', 'Dirección:\n', 'Email:\n'],

                    },
                    {
                      alignment: 'left',
                      lineHeight: 1.3,
                      text: [`${this.getCompleteName(data.farmer)}\n`, `${data.farmer.cc}\n`, `${data.farmer.cellphone}\n`, `${data.farmer.address}\n`, 'guarin@gmail.com\n',]
                    }
                  ]
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
        /* DETALLES DEL CREDITO */
        {
          style: 'table',
          table: {
            widths: [160, 100, '*', 50, 250, 50],
            body: [
              [{ colSpan: 3, text: `Detalle del crédito - Producto - ${data.product.name}`, color: '#fff', bold: true, fillColor: '#383838', margin: [15, 2, 0, 2] }, '', '', { colSpan: 3, text: 'Asesor: Cristian González', color: '#fff', bold: true, fillColor: '#383838', margin: [15, 2, 0, 2] }, '', ''],
              [{ text: 'Fecha de facturación:', margin: [40, 0, 0, 2] }, `${moment(data.quote.invoice.expirationDate).subtract(15, 'days').format('L')}`, 'Cuotas pagadas:', `${data.stateBilling.payedQuotes}`, 'DTF e.a. vigente a la fecha de facturación:', '0,00%'],
              [{ text: 'Valor del crédito:', margin: [40, 0, 0, 2] }, `$ ${data.amount}`, 'Cuotas pendientes:', `${data.stateBilling.pendingQuotes}`, 'Tasa de interés pactada DTF + %:', '0,00%'],
              [{ text: 'Plazo pactado (m):', margin: [40, 0, 0, 2] }, `${data.paymentPeriodicity}`, 'Cuotas en mora:', `${data.stateBilling.moraQuotes}`, 'Tasa de interes facturada (n.t):', `${data.interestRate}%`],
            ]
          },
          layout: 'noBorders'
        },
        /* SALDO DEL CREDITO A LA FECHA */
        {
          style: 'table',
          table: {
            widths: ['*'],
            body: [
              [{ text: 'Saldo del crédito a la fecha de corte', color: '#fff', bold: true, fillColor: '#383838', margin: [15, 2, 0, 2] },],
              [
                {
                  border: [false, true, false, true],
                  margin: [40, 0, 0, 0],
                  columns: [
                    {
                      width: 150,
                      alignment: 'left',
                      lineHeight: 1.3,
                      text: ['Saldo total:\n'],

                    },
                    {
                      alignment: 'left',
                      lineHeight: 1.3,
                      text: [`${data.balance - data.quote.invoice.total.toFixed(2)}\n`]
                    }
                  ]
                },
              ]
            ]
          },
          layout: 'noBorders'
        },
        /* MOVIMIENTOS REGISTRADOS DURANTE EL PERIODO */
        {
          style: 'table',
          table: {
            widths: [160, 100, '*', 100, 200, 50],
            body: [
              [{ colSpan: 6, text: 'Movimientos registrados durante el período', color: '#fff', bold: true, fillColor: '#383838', margin: [15, 2, 0, 2] }, '', '', '', '', ''],
              [{ text: 'Desde:', margin: [40, 0, 0, 2] }, `${moment(data.quote.invoice.expirationDate).subtract(15, 'days').format('L')}`, 'Hasta:', { text: `${moment(data.quote.invoice.expirationDate).format('L')}`, colSpan: 2 }, '', ''],
              [{ text: 'TOTAL PAGADO:', margin: [40, 0, 0, 2] }, { text: '$ 0*', margin: [0, 5, 0, 2] }, '', '', '', ''],
              [{ text: '(+) Saldo anterior:', margin: [40, 0, 0, 2] }, `${data.balance - data.quote.invoice.total.toFixed(2)}\n`, '(-) Abono a capital:', `${data.quote.invoice.paymentCapital.toFixed(2)}`, '(-) Honorarios:', '$ 0'],
              [{ text: '(-) Interés corriente:', margin: [40, 0, 0, 2] }, `$ ${data.quote.invoice.interest.toFixed(2)}`, '(-) Seguro de vida:', '$ 3.000*', '(-) Registro Garantía mobiliaria:', '$ 0'],
              [{ text: '(-) Interés de mora:', margin: [40, 0, 0, 2] }, '$ 0', '(-) Seguro cultivo:', '$ 0*', '(-) Otros egresos:', '$ 0'],
              [{ text: 'SALDO PERIODO:', margin: [40, 5, 0, 2] }, { text: '$ 0*', margin: [0, 5, 0, 2] }, '', '', '', ''],
            ]
          },
          layout: 'noBorders'
        },
        /*DETALLE DEL PROXIMO PAGO*/
        {
          style: 'table',
          table: {
            widths: [100, '*', 100, '*', 100, 100, '*'],
            body: [
              [{ colSpan: 7, text: 'Detalle próximo pago', color: '#fff', bold: true, border: [false, false, false, false], fillColor: '#383838', margin: [15, 2, 0, 2] }, '', '', '', '', '', ''],
              [{ text: 'No. Cuota', margin: [40, 0, 0, 2], border: [false, false, false, false] }, { text: 'Fecha de vencimiento', border: [false, false, false, false] }, { text: 'Capital', border: [false, false, false, false] }, { text: 'Intereses', border: [false, false, false, false] }, { text: 'Otros', border: [false, false, false, false] }, { text: 'Saldos', border: [false, false, false, false] }, { text: 'Total', border: [false, false, false, false], alignment: 'right' }],
              [{ text: `${data.quote.numberQuote + 1}`, margin: [40, 0, 0, 2], border: [false, false, false, false] }, { text: `${moment(data.quote.invoice.expirationDate).format('L')}`, border: [false, false, false, false] }, { text: `$ ${data.quote.invoice.paymentCapital.toFixed(2)}`, border: [false, false, false, false] }, { text: `$ ${data.quote.invoice.interest.toFixed(2)}`, border: [false, false, false, false] }, { text: `$ ${data.quote.invoice.others.toFixed(2)}`, border: [false, false, false, false] }, { text: '$ 0', border: [false, false, false, false] }, { text: `$ ${data.quote.invoice.total.toFixed(2)}`, border: [false, false, false, false], alignment: 'right' }],
              [{ text: '', margin: [40, 0, 0, 2], border: [false, false, false, false] }, { text: '', border: [false, false, false, false] }, { text: '', border: [false, false, false, false] }, { text: 'PAGUESE ANTES DE:', border: [true, true, false, true], bold: true, fillColor: '#d6d6d6', fontSize: 10 }, { text: `${moment(data.quote.invoice.expirationDate).format('L')}`, border: [false, true, false, true], bold: true, fillColor: '#d6d6d6' }, { text: '$ 0', border: [false, true, false, true], bold: true, fillColor: '#d6d6d6' }, { text: `$ ${data.quote.invoice.total.toFixed(2)}`, border: [false, true, true, true], bold: true, fillColor: '#d6d6d6', alignment: 'right' }],

            ]
          },

        },
        /* INSTRUCCCIONES */
        {
          style: 'table',
          table: {
            widths: ['*'],
            body: [
              [{ text: 'Instrucciones', color: '#fff', bold: true, fillColor: '#383838', margin: [15, 2, 0, 2] },],
              [
                {
                  border: [false, true, false, true],
                  margin: [40, 5, 0, 0],
                  columns: [
                    {
                      width: 150,
                      alignment: 'left',
                      lineHeight: 1.3,
                      text: ['Codigo de Convenio:\n', 'Referencia:\n', 'Nombre Empresa:\n', 'Nombre de Cuenta:\n'],

                    },
                    {
                      alignment: 'left',
                      lineHeight: 1.3,
                      text: ['78036\n', 'Por favor utilice su número de Cédula\n', 'AGRICAPITAL SAS\n', 'AGRICAPITAL SAS\n']
                    }
                  ]
                },
              ]
            ]
          },
          layout: 'noBorders'
        },

        /*  */
        {
          margin: [40, 20, 0, 0],
          text: [
            { text: 'Luego de realizada la consignación, por favor mandar comprobante al número' },
            { text: " 305 3213883", bold: true },
          ]
        },
        {
          margin: [40, 20, 0, 0],
          text: [
            { text: 'En caso de abonos extraordinarios, los mismos reducirán el plazo del crédito por defecto a menos que el titular del crédito solicite reducción de cuota. El pago es valido sólo con el comprobante enviado al número' },
            { text: " 305 3213883", bold: true },
          ]
        },
        {
          margin: [40, 20, 0, 0],
          text: [
            { text: 'Agricapital reporta períodicamente el comportamiento de los créditos a centrales de riesgo. Recuerda que si no recibes oportunamente la factura con el cobro de la cuota debes comunicarte con la oficina de Agricapital.' },
          ]
        },
        {
          margin: [40, 20, 0, 0],
          text: [
            { text: 'Recuerda actualizar tus datos personales en caso de algún cambio. Recuerda que de incurrir en mora, se dará inicio a la gestión de cobranza lo que causará los gastos correspondientes.' },
          ]
        },
        {
          margin: [40, 20, 0, 15],
          table: {
            widths: ['*'],
            body: [
              [{ text: 'Observaciones:', border: [false, false, false, false], bold: true }],
              [{ text: '', margin: [10, 10,] }],
            ]
          },
          layout: {
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 2 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 1 || i === node.table.widths.length) ? 2 : 1;
            }

          }
        },
      ],
      styles: {
        table: {
          margin: [0, 5, 0, 15],
          fontSize: 11
        },
      }

    }
    pdfMake.createPdf(dd).open();

  }

  /**
   * Metodo que setea el nombre del agricutlor para mostrarlo en el PDF
   * @param famer Datos del agricultor 
   */
  getCompleteName(farmer) {
    let name = `${farmer.PrimerNombre} ${farmer.SegundoNombre ? farmer.SegundoNombre : ''} ${farmer.PrimerApellido}`
    return name
  }

}
